<!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('{{ asset('front/img/home-bg.jpg') }}')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>{{ $pageTitle }}</h1>
                        <hr class="small">
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                @foreach($entries as $entry)
                <div class="post-preview">
                    <a href="{{ route('alpha_front_entry_'.$entry->entry_type.'_detail',$entry->slug) }}">
                        <h2 class="post-title">
                           {{parseMultiLang($entry->title)}}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ str_limit(strip_tags(parseMultiLang($entry->content),50)) }}
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="#">{{ $entry->user->username }}</a> on {{ date('F,d Y',strtotime($entry->published_at)) }}</p>
                </div>
                <hr>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">{!! $entries->render() !!}</div>
        </div>
    </div>

    <hr>