<!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('{{ asset('front/img/post-bg.jpg') }}')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="post-heading">
                        <h1>{{ parseMultiLang($entry->title) }}</h1>
                        <h2 class="subheading">Detail slug</h2>
                        <span class="meta">Posted by <a href="#">{{ $entry->user->username }}</a> on {{ date('F d, Y',strtotime($entry->published_at)) }}</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    {!! parseMultiLang($entry->content) !!}
                </div>
            </div>
        </div>
    </article>

    <hr>