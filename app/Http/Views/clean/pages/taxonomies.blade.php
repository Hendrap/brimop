<!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('{{ asset('front/img/home-bg.jpg') }}')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>{{ $pageTitle }} - Taxonomy</h1>
                        <hr class="small">
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                @foreach($taxonomies as $taxonomy)
                <div class="post-preview">
                    <a href="{{ "#" }}">
                        <h2 class="post-title">
                           {{$taxonomy->name}}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ str_limit(strip_tags(parseMultiLang($taxonomy->description),50)) }}
                        </h3>
                    </a>
                    <p class="post-meta">Total Entry {{ (int)$taxonomy->count }}</p>
                </div>
                <hr>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">{!! $taxonomies->render() !!}</div>
        </div>
    </div>

    <hr>