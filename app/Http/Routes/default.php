<?php 
	//entries
	if(!empty(app('AlphaSetting')->entries)){
		foreach (app('AlphaSetting')->entries as $key => $value) {
			
			//index
			Route::get('/'.$value['slug'],['as'=>'alpha_front_entry_'.$value['slug'],'uses'=>'FrontController@entries']);

			//detail
			Route::get('/'.$value['slug'].'/{slug}',['as'=>'alpha_front_entry_'.$value['slug'].'_detail','uses'=>'FrontController@entriesDetail']);

		}
	}

	//taxonomies
	if(!empty(app('AlphaSetting')->taxonomies)){
		foreach (app('AlphaSetting')->taxonomies as $key => $value) {

			//taxonomies
			Route::get('/categories/'.$value['slug'],['as'=>'alpha_front_taxonomy_'.$value['slug'],'uses'=>'FrontController@taxonomies']);

			//taxonomies - entries
			Route::get('/categories/'.$value['slug'].'/{slug}',['as'=>'alpha_front_taxonomy_'.$value['slug'].'_entries','uses'=>'FrontController@taxonomyEntries']);

		}
	}

	Route::get('/{page}',['as'=>'alpha_front_page_default','uses'=>'FrontController@defaultPage']);