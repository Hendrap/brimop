<?php 

if(count(\Config::get('alpha.application.locales')) > 1){
		if(!isset($_SESSION['lang'])){
			$_SESSION['lang'] = \Config::get('alpha.application.default_locale');
		}
		app('config')->set('alpha.application.default_locale',$_SESSION['lang']);
}
