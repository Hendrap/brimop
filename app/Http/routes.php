<?php

Route::group(['middleware' => ['web','multilang'],'namespace'=>'App\Http\Controllers'], function () {
	//multi-language config
	include base_path('app/Http/Routes/multilang.php');
	//default routing
	include base_path('app/Http/Routes/default.php');
	//index
	Route::get('/','FrontController@index');
});

