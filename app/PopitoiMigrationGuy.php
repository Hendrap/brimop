<?php 
namespace App;


use Entry;
use Entrymeta;
use DB;
use Taxonomy;
use Config;

class PopitoiMigrationGuy 
{
	/*
		-- How to use --

		1. Export(bikin sql dump) dari database lama
		2. Table yg perlu diexport adalah,entry,entrymetas,taxonomies,medias,entry_media,entry_taxonomy
		3. Step Import dari function dari class ini , dari atas ke bawah 
		4. Lihat comment tiap function

	*/

	public function migrateProductsAndUpdateBrandId()
	{

		/*
			1. Buat dump/backup dari database lama,semua yg berhunugan sama entry,samakan strukturnya
			2. Import ke database yg baru
			3. Run this function
		*/

		Config::set('database.default','old');

		$brands = Entry::whereEntryType('brand')->get();
		foreach($brands as $brand){
			Config::set('database.default','old');
			$entries = Entrymeta::whereMetaKey('brand_id')->where(function($q) use($brand){
				$q->where('meta_value_int',$brand->id);
				$q->orWhere('meta_value_text',$brand->id);
			})->lists('entry_id');
			
			Config::set('database.default','mysql');
			Entry::whereIn('id',$entries)->update(['brand_id'=>$brand->id]);	
		}

	}

	public function updateCategoryIdOfEntries()
	{

		//Lanjutan dari function migrateProductsAndUpdateBrandId();

		Config::set('database.default','old');

		$categories = Taxonomy::whereTaxonomyType('category')->lists('id');

		foreach($categories as $v){
			Config::set('database.default','old');
			$lists = DB::table('entry_taxonomy')->whereTaxonomyId($v)->lists('entry_id');
			Config::set('database.default','mysql');
			Entry::whereIn('id',$lists)->update(['category_id'=>$v]);
		}
	}

	public function updateSeriesIdOfEntries()
	{

		//Lanjutan dari function updateCategoryIdOfEntries();

		Config::set('database.default','old');

		$series = Taxonomy::whereTaxonomyType('category')->lists('id');

		foreach($series as $v){
			Config::set('database.default','old');
			$lists = DB::table('entry_taxonomy')->whereTaxonomyId($v)->lists('entry_id');
			Config::set('database.default','mysql');
			Entry::whereIn('id',$lists)->update(['series_id'=>$v]);
		}
	}

	public function updateYearOfEntries()
	{

		//Lanjutan dari function updateSeriesIdOfEntries();

		$metas = Entrymeta::whereMetaKey('year_released')->where('meta_value_text','>',0)->select(['meta_value_text'])->distinct()->lists('meta_value_text');
		foreach($metas as $meta){
			$entries = Entrymeta::whereMetaKey('year_released')->whereMetaValueText($meta)->lists('entry_id');
			if(!empty($entries)){
				Entry::whereIn('id',$entries)->update(['year'=>$meta]);
			}
		}
	}

	public function updateShapeIdOfEntries()
	{
		//update shape id,filter yg dipakai di parts

		$taxo = Taxonomy::with(['entries'=>function($q){
			$q->select('entries.id');
		}])->whereTaxonomyType('parts-category')->get();

		foreach($taxo as $v)
		{
			$ids = [];
			
			foreach($v->entries as $e)
			{
				$ids[] = $e->id;
			}

			if(!empty($ids))
			{
				Entry::whereIn('id',$ids)->update(['parts_shape_id'=>$v->id]);
			}
		}

	}
	public function updateColorIdOfEntries()
	{

		//update color id,filter yg dipakai di parts

		$taxo = Taxonomy::with(['entries'=>function($q){
			$q->select('entries.id');
		}])->whereTaxonomyType('parts-color')->get();

		foreach($taxo as $v)
		{
			$ids = [];
			foreach($v->entries as $e)
			{
				$ids[] = $e->id;
			}

			if(!empty($ids))
			{
				Entry::whereIn('id',$ids)->update(['parts_color_id'=>$v->id]);
			}

		}
	}

	public function updateConditionIdOfEntries()
	{

		//update color id,filter yg dipakai di parts

		$taxo = Taxonomy::with(['entries'=>function($q){
			$q->select('entries.id');
		}])->whereTaxonomyType('condition')->get();

		foreach($taxo as $v)
		{
			$ids = [];
			foreach($v->entries as $e)
			{
				$ids[] = $e->id;
			}

			if(!empty($ids))
			{
				Entry::whereIn('id',$ids)->update(['condition_id'=>$v->id]);
			}

		}
	}


	public function updateUserAndUserMetas()
	{
		//Cuma import data via sql dump ,samakan strukturnya. Yg perlu diambil table user & usermetas
	}

	public function migrateItemsOfProduct()
	{

		/*
			1. Sesuaikan coloumn di table catalog_items lama dgn yg baru
			2. export data-nya
			3. import ke database baru
		*/

		// 4. Update item status
		CatalogItem::where('status','active')->update(['status'=>1]);

		// 5. Update qty status
		CatalogItem::where('stock_available','<=',0)->update(['stock_status'=>0]);
		CatalogItem::where('stock_available','>',0)->update(['stock_status'=>1]);

	}

	public function migratePricesOfItem()
	{
	   
	   /* 

	    migrasi semua prices,
	    ini makan banyak waktu soale update itemnya satu2,
	    make sure ga time out,
	    better kalau dibuat pagination,silahkan update :)
	   
	   */
	    
	   set_time_limit(0);

	   Config::set('database.default','old');

       $n = 0;

       $prices = DB::table('catalog_prices')->select(['item_id','price','sale_price'])->where('group','cash')->get();

       Config::set('database.default','mysql');

       foreach($prices as $k => $v){ 
	        $n++;
	        $update = CatalogItem::where('id',$v->item_id)->update(['price'=>$v->price,'sale_price'=>$v->sale_price]);

	        //kelipatan 20,pending berapa detik gitu biar ga capek databasenya

	        if($n % 20 == 0){
		         //sleep time -> ini biar ga capek database-nya dihit terus2an,ini jeda waktu :)
		         sleep(5); 
	        }
       }

	}

	//ini agak nanti

	public function migrateOrders()
	{

	}

	public function migrateAddress()
	{
	    set_time_limit(0);

	    Config::set('database.default','old');
	    $data = [];
	    $address = CatalogAddress::get();

		foreach($address as $value){
			$data[] = [
					'user_id' => $value->user_id,
					'firstname' => $value->firstname,
					'lastname' => $value->lastname,
					'address' => $value->address1,
					'city' => $value->city,
					'type' => $value->type,
					'label'=> $value->label,
					'postcode' => $value->postal_code,
					'province' => $value->province,
					'district' => $value->district,
					'phone' => $value->phone,
				
			];
		}
		
		Config::set('database.default','mysql');
		DB::table('catalog_addresses')->insert($data);

	}

	public function migrateOrderDetails()
	{

	}

	public function migrateProductsToSolr()
	{

	}
}
