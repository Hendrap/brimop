<?php 
use Illuminate\Http\Request;
namespace App;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Swift_SendmailTransport;
use Session;
use Hash;
use DB;


class PopitoiMailGuy 
{
	public $userEmails = array();

	function __construct($userEmail)
	{
		$this->userEmails[] = $userEmail;
	}

	public function getInstanceOfMailer()
	{

		$message = app('AlphaMailMessage')
					->setFrom($this->getSender())
					->setTo($this->userEmails);	

		return $message;
	}

	public function sendEmail($message)
	{
		$send = app('AlphaMailSender')->send($message);
		return $send;
	}

	public function getSender()
	{
		return array('info@popitoi.com' => 'POPITOI');
	}

	public function sendRegisterMail()
	{
		$message = $this->getInstanceOfMailer();
	}

	public function sendRegisterUser($key, $name)
	{
        $parseEmail = array(
	        "[[name]]" => $name,
	        "[[link]]" => url('/account-validate').'/'.$key,
	        "[[unsubscribe]]" => url('/unsubscribe').'/'.$key,
        );

        $emailMessages = parseEmail('email-register',$parseEmail);

        $message = $this->getInstanceOfMailer()
        			->setSubject($emailMessages->subject)
					->setBody($emailMessages->msg,'text/html');

        $this->sendEmail($message);
	}

	public function sendEmailOrder($data = array(),$name,$orderNumber = '',$user)
	{

		 $products = '';

		 foreach ($data->orderDetails as $key => $value) {
		 	$products .= view('app::popitoi.pages.grids.product-order',['value'=>$value]);
		 }

		 $parseEmail = [
		 	"[[name]]" => $name,
            "[[email]]" => $user->email,
            "[[order_number]]" => $orderNumber,
            "[[shipping_cost]]" => number_format(round($data->order->shipping_amount)),
            "[[discount]]" => number_format(round($data->order->total_discount)),
            "[[totalprice]]" => number_format(round(($data->order->shipping_amount + $data->order->subtotal) - $data->order->total_discount )),
            "[[shipping-name]]" => $data->order->shipping_firstname.' '.$data->order->shipping_lastname,
            "[[shipping-address]]" => $data->order->shipping_address,
            "[[shipping-city]]" => $data->order->shipping_address,
            "[[shipping-phone]]" => $data->order->shipping_address,
            "[[billing-name]]" => $data->order->payment_firstname.' '.$data->order->payment_lastname,
            "[[billing-address]]" => $data->order->payment_address,
            "[[billing-city]]" => $data->order->payment_city,
            "[[billing-phone]]" =>$data->order->payment_phone,
            "[[date]]" => date('d F Y g:i:s A'),
            "[[confirm-payment]]" => "http://",
            '[[bodyMessageCart]]' => $products,
            '[[link_activate]]' => '',
            "[[unsubscribe]]" =>"",
		 ];
		 $emailMessages = parseEmail('purchase-notification',$parseEmail);

		 $message = $this->getInstanceOfMailer()
        			->setSubject($emailMessages->subject)
					->setBody($emailMessages->msg,'text/html');

         $this->sendEmail($message);


	}

	public function contactMail($sendToEmail, $sendToFirstname, $From, $template)
	{
		$valkey = base64_encode($sendToEmail);
        $parseEmail = array(
                    "[[name]]" => $sendToFirstname,
                    "[[text]]" => 'Thank you for pinching us on Popitoi ID.<br>Your message will be processed soon. Please do not change the e-mail subject.',
                    "[[unsubscribe]]" => '',
                );
        $emailMessages = parseEmail($template,$parseEmail);
        
        $subject = DB::table('entries')
			        ->where('entry_type','email_template')
			        ->where('slug',$template)
			        ->first();
			        
        $message = app('AlphaMailMessage')
					->setSubject(parseMultiLang($subject->title))
					->setBody($emailMessages->msg,'text/html')
					->setFrom($From)
					->setTo(array($sendToEmail));

        $mailer = app('AlphaMailSender')->send($message);
	}

	public function sendMailToAdmin($sendToEmail, $subject, $message, $From, $template)
	{        
		$parseEmail = array(
                    "[[name]]" => "Admin",
                    "[[text]]" => $message,
                    "[[unsubscribe]]" => '',
                );    

        $emailMessages = parseEmail($template,$parseEmail);
        $subject = DB::table('entries')
			        ->where('entry_type','email_template')
			        ->where('slug',$template)
			        ->first();
			        
        $message = app('AlphaMailMessage')
					->setSubject($subject)
                    ->setBody($emailMessages->msg, 'text/html')
					->setFrom($From)
					->setTo(array($sendToEmail));

        $mailer = app('AlphaMailSender')->send($message);
	}

	public function resetPassword($sendToEmail, $template, $parseEmail)
	{

        $emailMessages = parseEmail($template,$parseEmail);
		$subject = DB::table('entries')
			        ->where('entry_type','email_template')
			        ->where('slug',$template)
			        ->first();
			        
        $message = app('AlphaMailMessage')
					->setSubject(parseMultiLang($subject->title))
					->setBody($emailMessages->msg,'text/html')
					->setFrom($this->getSender())
					->setTo(array($sendToEmail));
		$mailer = app('AlphaMailSender')->send($message);

	}
}
