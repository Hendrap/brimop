/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.5.16 : Database - alpha-clean
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*Table structure for table `alp_dashboards` */

DROP TABLE IF EXISTS `alp_dashboards`;

CREATE TABLE `alp_dashboards` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `alp_dashboards` */

/*Table structure for table `alp_entries` */

DROP TABLE IF EXISTS `alp_entries`;

CREATE TABLE `alp_entries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` text,
  `content` longtext,
  `excerpt` text,
  `author` bigint(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `entry_parent` bigint(20) DEFAULT NULL,
  `entry_type` varchar(200) DEFAULT NULL,
  `comment_status` varchar(5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `alp_entries` */

/*Table structure for table `alp_entry_media` */

DROP TABLE IF EXISTS `alp_entry_media`;

CREATE TABLE `alp_entry_media` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(20) DEFAULT NULL,
  `media_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `alp_entry_media` */

/*Table structure for table `alp_entry_taxonomy` */

DROP TABLE IF EXISTS `alp_entry_taxonomy`;

CREATE TABLE `alp_entry_taxonomy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(20) DEFAULT NULL,
  `taxonomy_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `alp_entry_taxonomy` */

/*Table structure for table `alp_entrymetas` */

DROP TABLE IF EXISTS `alp_entrymetas`;

CREATE TABLE `alp_entrymetas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(20) DEFAULT NULL,
  `meta_name` varchar(200) DEFAULT NULL,
  `meta_key` varchar(200) DEFAULT NULL,
  `meta_value_text` text,
  `meta_value_int` int(11) DEFAULT NULL,
  `meta_value_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `alp_entrymetas` */

/*Table structure for table `alp_medias` */

DROP TABLE IF EXISTS `alp_medias`;

CREATE TABLE `alp_medias` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` text,
  `description` longtext,
  `media_type` varchar(100) DEFAULT NULL,
  `media_mime` varchar(100) DEFAULT NULL,
  `media_meta` text,
  `url` varchar(200) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `author` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `alp_medias` */

/*Table structure for table `alp_role_rule` */

DROP TABLE IF EXISTS `alp_role_rule`;

CREATE TABLE `alp_role_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `rule_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `alp_role_rule` */

insert  into `alp_role_rule`(`id`,`role_id`,`rule_id`,`created_at`,`updated_at`) values (1,1,1,NULL,NULL),(2,2,2,NULL,NULL),(3,2,6,NULL,NULL),(4,2,7,NULL,NULL),(5,2,11,NULL,NULL),(6,2,12,NULL,NULL),(7,2,16,NULL,NULL),(8,2,17,NULL,NULL),(9,2,21,NULL,NULL),(10,2,22,NULL,NULL),(15,2,26,NULL,NULL),(16,2,27,NULL,NULL),(14,2,32,NULL,NULL),(17,2,31,NULL,NULL),(18,2,34,NULL,NULL),(19,2,33,NULL,NULL);

/*Table structure for table `alp_role_user` */

DROP TABLE IF EXISTS `alp_role_user`;

CREATE TABLE `alp_role_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `alp_role_user` */

insert  into `alp_role_user`(`id`,`user_id`,`role_id`,`created_at`,`updated_at`) values (1,1,1,NULL,NULL),(2,20,4,NULL,NULL),(3,22,5,NULL,NULL),(4,21,4,NULL,NULL),(5,22,2,NULL,NULL),(6,21,4,NULL,NULL),(7,21,4,NULL,NULL),(8,21,4,NULL,NULL),(9,21,4,NULL,NULL),(10,21,4,NULL,NULL),(11,22,2,NULL,NULL),(12,21,4,NULL,NULL),(13,22,2,NULL,NULL),(14,21,4,NULL,NULL);

/*Table structure for table `alp_roles` */

DROP TABLE IF EXISTS `alp_roles`;

CREATE TABLE `alp_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `alp_roles` */

insert  into `alp_roles`(`id`,`name`,`created_at`,`updated_at`,`modified_by`) values (1,'Super Administrator',NULL,NULL,NULL),(2,'Administrator',NULL,NULL,NULL),(3,'Publisher',NULL,NULL,NULL),(4,'Contributor',NULL,NULL,NULL),(5,'Registered',NULL,NULL,NULL);

/*Table structure for table `alp_rules` */

DROP TABLE IF EXISTS `alp_rules`;

CREATE TABLE `alp_rules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group` varchar(200) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `alp_rules` */

insert  into `alp_rules`(`id`,`group`,`action`,`description`,`created_at`,`updated_at`) values (1,'*','*','Godlike!','2013-05-30 16:47:24','2013-05-30 16:47:24'),(2,'Entries','*','Manage all entries','2013-05-30 16:47:24','2013-05-30 16:47:24'),(6,'Entries','View','View Entry','2013-05-30 16:47:24','2013-05-30 16:47:24'),(7,'Media','*','Manage All Media','2013-05-30 16:47:24','2013-05-30 16:47:24'),(11,'Media','View','View media','2013-05-30 16:47:24','2013-05-30 16:47:24'),(12,'Taxonomy','*','Manage all Taxonomies','2013-05-30 16:47:24','2013-05-30 16:47:24'),(16,'Taxonomy','View','View Taxonomy','2013-05-30 16:47:24','2013-05-30 16:47:24'),(17,'Comment','*','Manage Comment','2013-05-30 16:47:24','2013-05-30 16:47:24'),(21,'Comment','View','View Comment','2013-05-30 16:47:24','2013-05-30 16:47:24'),(22,'User','*','Manage Users','2013-05-30 16:47:24','2013-05-30 16:47:24'),(26,'User','View','View Users','2013-05-30 16:47:24','2013-05-30 16:47:24'),(27,'Permission','*','Manage Group And Permission','2013-05-30 16:47:24','2013-05-30 16:47:24'),(34,'Setting','View','View',NULL,NULL),(33,'Setting','*','All access',NULL,NULL),(31,'Permission','View','View Group And Permission','2013-05-30 16:47:24','2013-05-30 16:47:24'),(32,'Backend','Access','Backend Access','2013-05-30 16:47:24','2013-05-30 16:47:24');

/*Table structure for table `alp_searches` */

DROP TABLE IF EXISTS `alp_searches`;

CREATE TABLE `alp_searches` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(20) NOT NULL,
  `title` text,
  `content` text,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `alp_searches` */

/*Table structure for table `alp_settings` */

DROP TABLE IF EXISTS `alp_settings`;

CREATE TABLE `alp_settings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(255) DEFAULT NULL,
  `setting_value` text,
  `autoload` varchar(3) DEFAULT 'yes',
  `bundle` varchar(200) DEFAULT 'alpha',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setting_key` (`setting_key`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

/*Data for the table `alp_settings` */

insert  into `alp_settings`(`id`,`setting_key`,`setting_value`,`autoload`,`bundle`,`created_at`,`updated_at`,`modified_by`) values (1,'site_name','Alpha CMS Beta','yes','alpha',NULL,'2016-04-15 10:43:15',NULL),(2,'site_url','','yes','alpha',NULL,'2016-04-15 09:17:58',NULL),(3,'site_slogan','Hellomorra','yes','alpha',NULL,'2016-05-09 10:09:34',NULL),(4,'site_logo','','yes','alpha',NULL,'2016-04-15 09:17:58',NULL),(5,'site_description','Description','yes','alpha',NULL,'2016-04-15 09:17:58',NULL),(6,'site_keywords','','yes','alpha',NULL,'2016-04-15 09:17:58',NULL),(59,'entry_page','a:6:{s:4:\"slug\";s:4:\"page\";s:6:\"single\";s:4:\"Page\";s:6:\"plural\";s:5:\"Pages\";s:7:\"show_ui\";s:3:\"yes\";s:10:\"taxonomies\";a:2:{i:0;s:2:\"58\";i:1;s:2:\"55\";}s:5:\"metas\";a:3:{i:0;O:8:\"stdClass\":3:{s:8:\"meta_key\";s:7:\"display\";s:9:\"meta_name\";s:5:\"block\";s:14:\"meta_data_type\";s:4:\"text\";}i:1;O:8:\"stdClass\":3:{s:8:\"meta_key\";s:10:\"first_name\";s:9:\"meta_name\";s:3:\"aaa\";s:14:\"meta_data_type\";s:3:\"int\";}i:2;O:8:\"stdClass\":3:{s:8:\"meta_key\";s:3:\"asd\";s:9:\"meta_name\";s:7:\"asdasda\";s:14:\"meta_data_type\";s:4:\"date\";}}}','yes','alpha.entry','2016-04-18 11:05:01','2016-04-20 04:01:59',NULL),(16,'site_default_language','en','yes','alpha','2013-01-28 11:41:39','2016-04-15 09:17:58',NULL),(17,'date_format','F j, Y','yes','alpha','2013-01-28 15:29:49','2016-04-15 09:17:58',NULL),(18,'time_format','g:i a','yes','alpha','2013-01-28 15:30:40','2016-04-15 09:17:58',NULL),(19,'site_records_per_page','10','yes','alpha','2013-01-28 15:32:04','2016-04-15 09:17:58',NULL),(58,'taxonomy_color','a:4:{s:4:\"slug\";s:5:\"color\";s:6:\"single\";s:5:\"Color\";s:6:\"plural\";s:5:\"Color\";s:7:\"show_ui\";s:3:\"yes\";}','yes','alpha.taxonomy','2016-04-18 10:33:29','2016-04-20 04:20:13',NULL),(30,'google_analytics_code','','yes','alpha','2013-12-30 16:22:56','2016-04-15 09:17:58',NULL),(31,'admin_email','luthfifs97@gmail.com','yes','alpha','2014-03-14 09:18:35','2016-04-15 09:17:58',NULL),(32,'fb_app_id','','yes','alpha','2014-03-14 09:18:35','2016-04-15 09:17:58',NULL),(33,'fb_secret_id','','yes','alpha','2014-03-14 09:18:35','2016-04-15 09:17:58',NULL),(34,'tw_consumer_key','','yes','alpha','2014-03-14 09:18:35','2016-04-15 09:17:58',NULL),(35,'tw_consumer_secret','','yes','alpha','2014-03-14 09:18:35','2016-04-15 09:17:58',NULL),(36,'tw_callback','','yes','alpha','2014-03-14 09:18:35','2016-04-15 09:17:58',NULL),(56,'user_default','a:3:{s:4:\"slug\";s:7:\"default\";s:10:\"taxonomies\";a:1:{i:0;s:2:\"55\";}s:5:\"metas\";a:4:{i:0;O:8:\"stdClass\":3:{s:8:\"meta_key\";s:10:\"first_name\";s:9:\"meta_name\";s:10:\"First Name\";s:14:\"meta_data_type\";s:4:\"text\";}i:1;O:8:\"stdClass\":3:{s:8:\"meta_key\";s:9:\"last_name\";s:9:\"meta_name\";s:9:\"Last Name\";s:14:\"meta_data_type\";s:4:\"text\";}i:2;O:8:\"stdClass\":3:{s:8:\"meta_key\";s:3:\"age\";s:9:\"meta_name\";s:3:\"Age\";s:14:\"meta_data_type\";s:3:\"int\";}i:3;O:8:\"stdClass\":3:{s:8:\"meta_key\";s:13:\"date_of_birth\";s:9:\"meta_name\";s:13:\"Date Of Birth\";s:14:\"meta_data_type\";s:4:\"date\";}}}','yes','alpha.user','2016-04-18 03:00:32','2016-04-18 04:06:20',NULL),(37,'fb_url','','yes','alpha','2015-01-07 14:05:42','2016-04-15 09:17:58',NULL),(38,'tw_url','','yes','alpha','2015-01-07 14:05:42','2016-04-15 09:17:58',NULL),(39,'path_url','','yes','alpha','2015-01-07 14:05:51','2016-04-15 09:17:58',NULL),(40,'g_url',NULL,'yes','alpha',NULL,'2016-04-15 09:17:58',NULL),(41,'instagram_url',NULL,'yes','alpha',NULL,'2016-04-15 09:17:58',NULL),(55,'taxonomy_category','a:4:{s:4:\"slug\";s:8:\"category\";s:6:\"single\";s:8:\"category\";s:6:\"plural\";s:8:\"category\";s:7:\"show_ui\";s:3:\"yes\";}','yes','alpha.taxonomy','2016-04-18 02:58:09','2016-04-18 02:58:09',NULL),(54,NULL,NULL,'yes','alpha','2016-04-15 09:04:50','2016-04-15 09:17:58',NULL),(53,NULL,NULL,'yes','alpha','2016-04-15 09:03:56','2016-04-15 09:17:58',NULL),(52,NULL,NULL,'yes','alpha','2016-04-15 09:03:28','2016-04-15 09:17:58',NULL),(51,NULL,NULL,'yes','alpha','2016-04-15 09:03:15','2016-04-15 09:17:58',NULL),(61,'image_test','a:4:{s:4:\"name\";s:4:\"Test\";s:5:\"width\";s:3:\"100\";s:6:\"height\";s:3:\"100\";s:4:\"crop\";s:3:\"yes\";}','yes','alpha.image','2016-04-21 09:38:22','2016-04-21 09:38:22',NULL),(62,'image_crop','a:5:{s:4:\"name\";s:4:\"crop\";s:5:\"width\";s:3:\"200\";s:6:\"height\";s:3:\"200\";s:4:\"crop\";s:3:\"yes\";s:11:\"aspectratio\";s:3:\"yes\";}','yes','alpha.image','2016-04-22 06:40:39','2016-04-22 06:52:21',NULL),(63,'image_force','a:5:{s:4:\"name\";s:5:\"force\";s:5:\"width\";s:3:\"200\";s:6:\"height\";s:3:\"200\";s:4:\"crop\";s:3:\"yes\";s:11:\"aspectratio\";s:2:\"no\";}','yes','alpha.image','2016-04-22 06:57:17','2016-04-22 06:57:17',NULL),(64,'image_media','a:5:{s:4:\"name\";s:5:\"media\";s:5:\"width\";s:3:\"550\";s:6:\"height\";s:3:\"550\";s:4:\"crop\";s:3:\"yes\";s:11:\"aspectratio\";s:2:\"no\";}','yes','alpha.image','2016-05-02 02:15:25','2016-05-02 02:15:25',NULL),(65,'image_default','a:5:{s:4:\"name\";s:7:\"default\";s:5:\"width\";s:3:\"200\";s:6:\"height\";s:3:\"200\";s:4:\"crop\";s:3:\"yes\";s:11:\"aspectratio\";s:2:\"no\";}','yes','alpha.image','2016-05-02 02:53:33','2016-05-02 02:53:33',NULL);

/*Table structure for table `alp_taxonomies` */

DROP TABLE IF EXISTS `alp_taxonomies`;

CREATE TABLE `alp_taxonomies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `taxonomy_slug` varchar(255) DEFAULT NULL,
  `taxonomy_type` varchar(255) DEFAULT NULL,
  `parent` bigint(20) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `alp_taxonomies` */

/*Table structure for table `alp_usermetas` */

DROP TABLE IF EXISTS `alp_usermetas`;

CREATE TABLE `alp_usermetas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `meta_name` varchar(200) DEFAULT NULL,
  `meta_key` varchar(200) DEFAULT NULL,
  `meta_value_text` text,
  `meta_value_int` int(11) DEFAULT NULL,
  `meta_value_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `alp_usermetas` */

/*Table structure for table `alp_users` */

DROP TABLE IF EXISTS `alp_users`;

CREATE TABLE `alp_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT 'default',
  `status` varchar(200) DEFAULT 'inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `alp_users` */

insert  into `alp_users`(`id`,`email`,`username`,`password`,`type`,`status`,`created_at`,`updated_at`,`last_login`,`modified_by`,`remember_token`) values (1,'admin@hellomorra.com','alphas','$2y$10$JYv7rza7S8w/qHzZJpNLiulZ97Y9ZznU/mFOiD2PGZcCRTJ2Zouu.','default','active','2016-04-14 08:56:55','2016-05-10 09:19:24','2016-05-10 09:19:24',NULL,'b9Z5KBlT1lJDACVd9EbmQ6g6Y663vXtDZ8EnM067RKfACGb1GBWduE5yF434');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
