### POPITOI V2 ###
- - -

### Coding Standard ###
---
-- PSR-1 : http://www.php-fig.org/psr/psr-1/

-- PSR-2 : http://www.php-fig.org/psr/psr-2/

### GIT ###
-- Aturan Kolaborasi : https://bitbucket.org/Morra/loyalty-program/wiki/GitCollaboration	

### SOLR SEARCH ###
---
Solr v5.0

### SOLR PHP CLIENT ###
-- Repo : https://github.com/solariumphp/solarium

-- Docs : http://solarium.readthedocs.io/en/stable/documents/

#### Folder Structure ####
- - -

```
-- vendor                    // Vendor Folder(Composer)
-- alpha                       // System Folder
-- public                     // Assets Folder
-- app                       // Alpha Workspace
-- .
-- .
-- . etc
```


#### Author ####
- - -
Morra Team

#### Current Version ####
- - -
0.0.0

#### Licence ####
- - - 
Copyright 2016 hellomorra.com