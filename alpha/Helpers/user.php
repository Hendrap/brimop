<?php
/**
 * User Helper Template
 *
 * @author	veelasky <riefky.alhuraibi@gmail.com>
 *
 * @version	1.0
 */

	/**
	 * Retrieve User Avatar by ID
	 *
	 * @return 	string
	 * @param	int		UserId
	 * @param	array	size
	 */

	function getUserAvatar($uid=null, $size) {
		/*
		 * @assign WAKHID::Retrieve User Avatar
		 */
	}

	/**
	 * Retrieve User Details Data
	 *
	 * @return 	string
	 * @param  	int 		UserId
	 * @param	key			Meta Key
	 */

	function getUserData($userId=null,$key=null,$status=false) {
		if (empty($userId) and empty($key))
			return false;
		else
		{
			if($status == false)
				return UserMeta::where('user_id', '=', $userId)->where('meta_key', '=', $key)->first();
			else
				return UserMeta::where('user_id', '=', $userId)->where('meta_key', '=', $key)->get();
		}
	}

	function getUserFullName($userId=null) {
		if (empty($userId) and empty($key))
			return false;
		else {
			$firstname = DB::table('usermetas')->where('user_id', '=', $userId)->where('meta_key', '=', 'first_name')->first();
			$lastname  = DB::table('usermetas')->where('user_id', '=', $userId)->where('meta_key', '=', 'last_name')->first();

			if (empty($firstname->meta_value_text) AND empty($lastname->meta_value_text)):
				return User::find($userId)->username;
			else:
				$tmp = array();
				if (!empty($firstname->meta_value_text))
					$tmp[] = $firstname->meta_value_text;
				if (!empty($lastname->meta_value_text))
					$tmp[] = $lastname->meta_value_text;
				return implode(" ", $tmp);
			endif;
		}
	}

	function getUserFirstName($userId=null) {
		if (empty($userId) and empty($key)) {
			return false;
		}
		else {
			$firstname = DB::table('usermetas')->where('user_id', '=', $userId)->where('meta_key', '=', 'first_name')->first();

			if (empty($firstname->meta_value_text) and empty($lastname->meta_value_text)) {
				return User::find($userId)->username;
			} else {
				return $firstname->meta_value_text;
			}
		}
	}

	/**
	 * Find user by Email
	 */
	function getUserbyEmail($email) {
		return User::where('email','=',$email)->first();
	}

	/**
	 * Find user by Username
	 */
	function getUserbyUsername($username) {
		return User::where('username','=',$username)->first();
	}

	/**
	 * Get user data
	 */
	function getUser($userId) {
		return User::where('id','=',$userId)->first();
	}

	function getAllAdministrator() {
		$return = Role::where('name','=','Administrator')->first()->users()->get();
		return $return;
	}