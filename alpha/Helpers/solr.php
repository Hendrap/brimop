<?php 

function syncProductsToSolr($ids = [],$isRealTime = false)
{
	if(empty($ids)) return false;

 	$solr = new App\PopitoiSolrGuy();
	$result = $solr->syncProductsToSolr($ids,$isRealTime);

	return $result;
}

function deleteProductFromSolr($id = 0){

	$solr = new App\PopitoiSolrGuy();
	$result = $solr->delete('id:'.$id);

	return $result;

}
function parseProduct(&$products){
        $tmpProducts = [];


        foreach ($products as $key => $value) {
            $tmpProducts[] = $value;
        }

        $products = $tmpProducts;

        $brandIds = [];
        $brandsContainer = [];
        foreach ($products as $key => $value) 
        {
           $brandIds[] = $value->brand_id;
        }

        if(!empty($brandIds))
        {
            $brands = Entry::whereIn('id',$brandIds)->select(['id','slug'])->get();
            foreach ($brands as $key => $value) {
               $brandsContainer[$value->id] = $value->slug;
            }
        }

        foreach ($products as $key => $value) {
            $prefix = 'lego';
            if(!empty($brandsContainer[$value->brand_id])){
                $prefix = $brandsContainer[$value->brand_id];
            }

            $products[$key]->brand_slug = $prefix;
        }
        
}   