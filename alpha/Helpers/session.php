<?php 

function cleanUpSession(){
      $forgets = ['billing_address_id','shipping_fee','notes','shipping_address_id','shipping_courier','dropshipper_name','dropshipper_phone'];
      foreach ($forgets as $key => $value) {
           Session::forget($value);
      }
      Session::save();
}