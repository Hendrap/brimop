<?php 

function isNew($date = '')
{
	$date1 = date_create($date);
	$date2 = date_create(date('Y-m-d'));
	$diff = date_diff($date1,$date2);

	if($diff->days <= 30)
	{
		return true;
	}

	return false;

}