<?php 

function parseMedias($media = array()){
	$res = '';
	foreach ($media as $key => $value) {
			$img = asset('backend/assets/images/placeholder.jpg');
			$path = asset($value->path);
			if(!empty($value->url)){
				$path = $value->url;
			}
			$title = $value->title;
			if($value->media_type == 'image'){
				$img = asset(getCropImage($value->path,'default'));
			}elseif ($value->media_type == 'video') {
				if(!empty($value->url)){
					$img = asset(getCropImage($value->path,'default'));
				}
			}


			$res .= view('alpha::admin.media.templates.entry-media',[
				'img' => $img,
				'path' => $path,
				'title' => $title,
				'id' => $value->id,
			]);
		}
	return $res;
}