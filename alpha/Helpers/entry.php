<?php 
function getEntryParents($type = '',$limit = 0){
	$parents = Entry::select(['id','title'])->whereEntryType($type)->where('status','!=','deleted')->where('entry_parent','=',0);

	if($limit > 0){
		$parents = $parents->take($limit);
	}

	$parents = $parents->get();
	return $parents;
}
function getTaxonomiesForEntry($taxoTypes = array()){
	$taxonomies = [];
    if(!empty($taxoTypes)){
        $taxonomies = Taxonomy::select(['id','name','taxonomy_type'])->whereIn('taxonomy_type',$taxoTypes)->get();
    }
    return $taxonomies;
}
function getEntryConfig($type = ''){
	$config = getConfigBySlug('entries',$type);
	return $config;
}
function getMetaFromEntry($type = ''){
	$metas = [];
	$entryType = getEntryConfig($type);

	if(empty($entryType)) return $metas;

	$metas = @$entryType['metas'];

	return $metas;
}
function getTaxonomyFromEntry($type = ''){
	$tmpTaxoList = [];
	$tmp = getEntryConfig($type);

	$tmpTaxoList = @$tmp['taxonomies'];


    $taxoTypes = [];
    $taxo = [];
    if(!empty($tmpTaxoList)){
        foreach ($tmpTaxoList as $key => $value) {
            $tmp = @app('AlphaSetting')->taxonomies[$value];
            if(!empty($tmp)){
                $taxo[] = $tmp;
                $taxoTypes[] = $tmp['slug'];
            }
        }            
    }

    return (object)array('taxo'=>$taxo,'taxoTypes'=>$taxoTypes);

}
function getEntryMetaFromArray($metas = array(),$key = ''){
	if(empty($metas)) return false;
		$retval = NULL;
		foreach ($metas as $meta) {
			if ($meta->meta_key === $key) {
				if ($meta->meta_value_text) {
					$retval = $meta->meta_value_text;
					break;
				}

				if ($meta->meta_value_int) {
					$retval = $meta->meta_value_int;
					break;
				}

				if ($meta->meta_value_date) {
					$retval = $meta->meta_value_date;
					if($retval == '0000-00-00 00:00:00') return '';
					break;
				}
			}
		}
	return $retval;
}
function parseMultiLang($content = '',$lang = ''){
	
	$defaultLang = Config::get('alpha.application.default_locale');
	if(empty($lang)) $lang = $defaultLang;
	$parsed = parseLang($content);

	if(!empty($parsed[$lang])) return $parsed[$lang];

	return $parsed[Config::get('alpha.application.fallback_locale')];

}
function parseLang($content = ''){
	$splitter 	= '#(\[:[a-z]{2}\])#ism';
	$result		= array();

	foreach (Config::get('alpha.application.locales') as $key => $val):
		$result[$key] = '';
	endforeach;

	$blocks = preg_split($splitter, $content, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
	$current = '';
	foreach ($blocks as $block):
		if(preg_match('#^\[:([a-z]{2})\]$#ism', $block, $matches)):
			$current = $matches[1];
			continue;
		endif;
		$result[$current] = trim($block);
	endforeach;
	return $result;
}