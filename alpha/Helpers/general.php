<?php 
function getConfigBySlug($attr = '',$slug = ''){
	foreach (app('AlphaSetting')->{$attr} as $key => $value) {
		if($value['slug'] == $slug) return $value;
	}
	return [];
}
function setPageTitle($title = 'Page'){
	return $title.' | '.app('AlphaSetting')->getSetting('site_name');
}
function setHomeTitle(){
	return app('AlphaSetting')->getSetting('site_name').' | '.app('AlphaSetting')->getSetting('site_slogan');
}
function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('Bytes', 'K', 'M', 'G', 'T');   

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}