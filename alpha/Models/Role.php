<?php 
namespace Alpha\Models;
/**
* 
*/
class Role extends AlphaORM
{
	protected $fillable = ['name'];
	function __construct()
	{
		# code...
	}

	public function rules(){
		return $this->belongsToMany('Rule');
	}

	public function users(){
		return $this->belongsToMany('User');
	}
}