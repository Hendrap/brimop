<?php 
namespace Alpha\Models;
/**
* 
*/
class Taxonomy extends AlphaORM
{
	
	function __construct()
	{
		# code...
	}
	public function items(){
        return $this->BelongsToMany('Morra\Catalog\Models\Item');
    }
	public function entries()
	{
		return $this->belongsToMany('Entry');
	}

	public function parent(){
		return $this->belongsTo('Taxonomy','parent');
	}

	public function childs(){
		return $this->hasMany('Taxonomy','parent');
	}

}