<?php 

	return [
		
		'subfolder' => false,

		'profiler' => false,

		'urlasset' => 'public/',

		'locales' => ['en' => 'English'],

		'default_locale' => 'en',

		'fallback_locale' => 'en',

		'whitelist' => ['127.0.0.1']
	];