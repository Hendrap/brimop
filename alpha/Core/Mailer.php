<?php 
namespace Alpha\Core;
/**
* 
*/
class Mailer
{
	private $transport;

	public function __construct(){
		$config = \Config::get('alpha.mailer');
		$this->setTransport($config);
	}
	public function setTransport($config){
		$selectedTransport = $config['transports'][$config['transport']];
		switch ($config['type']) {
			case 'local':
				$transport = \Swift_SendmailTransport::newInstance($config['transports'][$config['transport']]);
				break;
			case 'native':
				$transport = \Swift_MailTransport::newInstance();
				break;
			case 'smtp':
				$transport = \Swift_SmtpTransport::newInstance($selectedTransport['host'], $selectedTransport['port'],$selectedTransport['encryption'])->setUsername($selectedTransport['username'])->setPassword($selectedTransport['password']);
				break;
			default:
				$transport = \Swift_MailTransport::newInstance();
				break;
		}
		$this->transport = $transport;
	}
	public function send($message){
		$mailer = \Swift_Mailer::newInstance($this->transport);
		if (!$mailer->send($message, $failures))
		{
		  return $failures;
		}
		return true;
	}
}