<?php 

Route::group(['middleware' => 'AlphaAdminGroup'], function () {

	Route::group(['namespace'=>'Alpha\Controllers','prefix' => 'admin'], function () {
		Route::get('refresh-csrf',['as'=>'alpha_admin_refresh_token','uses'=>function(){
		    return csrf_token();
		}]);


		//login logout 
		Route::get('/login',['as'=>'alpha_get_login','uses'=>'Credential@login']);
		Route::get('/logout',['as'=>'alpha_get_logout','uses'=>'Credential@logout']);
		Route::post('/login',['as'=>'alpha_post_login','uses'=>'Credential@postLogin']);

		//ini ada  middleware buat bataesin akses admin
		Route::group(['middleware' => 'AuthAdmin'],function(){
			//index dashboard
			Route::get('/',['as'=>'alpha_admin_index','uses'=>'Dashboard@index']);
			//settings prefix ,seluruh halaman /admin/setting masuk sini yah
			Route::group(['prefix' => 'setting'], function () {
				//general setting,isinya seo atau site description gitu
				Route::get('general',['as'=>'alpha_admin_get_setting_general','uses'=>'Setting@getGeneral']);
				Route::post('general',['as'=>'alpha_admin_post_setting_general','uses'=>'Setting@postGeneral']);

				//plugins
				Route::get('plugins',['as'=>'alpha_admin_get_plugins','uses'=>'Setting@getPlugins']);
				Route::get('extras',['as'=>'alpha_admin_get_extras','uses'=>'Setting@getExtras']);
				Route::get('labels',['as'=>'alpha_admin_get_labels','uses'=>'Setting@getLabels']);
				Route::get('contacts',['as'=>'alpha_admin_get_contacts','uses'=>'Setting@getContacts']);

				Route::get('change-status-plugins/{plugin}/{status}',['as'=>'alpha_admin_get_status_plugins','uses'=>'Setting@getStatusPlugins']);


				//config crop images
				Route::resource('image','Image');
				//Roles & access
				Route::resource('role','Role');
				//Entry Types
				Route::resource('entry_type','EntryType');
				//Taxonomy Types
				Route::resource('taxonomy_type','TaxonomyType');
				//user types;
				Route::resource('user_type','UserType');
			});

			//users
			Route::resource('user','Admin\User');
			Route::get('user/change-status/{id}/{status}',['as'=>'alpha_admin_user_change_status','uses'=>'Admin\User@status']);
			Route::get('my-account',['as'=>'alpha_admin_myaccount','uses'=>'Admin\User@myAccount']);
			Route::post('save-my-account',['as'=>'alpha_admin_savemyaccount','uses'=>'Admin\User@saveMyAccount']);

			//taxonomies
			Route::group(['prefix' => 'taxonomy'], function () {
				
				Route::get('/{type}',['as'=>'alpha_admin_taxonomy_index','uses'=>'Admin\Taxonomy@index']);
				Route::get('/{type}/create',['as'=>'alpha_admin_taxonomy_create','uses'=>'Admin\Taxonomy@create']);
				Route::get('/{type}/edit/{id}',['as'=>'alpha_admin_taxonomy_edit','uses'=>'Admin\Taxonomy@edit']);
				Route::post('/delete/{id}',['as'=>'alpha_admin_taxonomy_delete','uses'=>'Admin\Taxonomy@delete']);
				Route::post('/{type}/save/{id}',['as'=>'alpha_admin_taxonomy_save','uses'=>'Admin\Taxonomy@save']);
			
			});

			//entries
			Route::group(['prefix' => 'entry'], function () {
				
				Route::get('/{type}',['as'=>'alpha_admin_entry_index','uses'=>'Admin\Entry@index']);
				Route::get('/{type}/create',['as'=>'alpha_admin_entry_create','uses'=>'Admin\Entry@create']);
				Route::get('/{type}/edit/{id}',['as'=>'alpha_admin_entry_edit','uses'=>'Admin\Entry@edit']);
				Route::post('/delete/{id}',['as'=>'alpha_admin_entry_delete','uses'=>'Admin\Entry@delete']);
				Route::post('/{type}/save/{id}',['as'=>'alpha_admin_entry_save','uses'=>'Admin\Entry@save']);
				Route::get('/status/{id}/{status}',['as'=>'alpha_admin_entry_status','uses'=>'Admin\Entry@status']);
			
			});			
			//media library
			Route::group(['prefix' => 'media'], function () {
				
				Route::get('test',function(){
					return view('alpha::admin.test.index');
				});

				Route::get('image',['as'=>'alpha_admin_media_image','uses'=>'Admin\Media@image']);
				Route::post('upload-image',['as'=>'alpha_admin_media_upload_image','uses'=>'Admin\Media@uploadImage']);

				Route::get('audio',['as'=>'alpha_admin_media_audio','uses'=>'Admin\Media@audio']);
				Route::post('upload-audio',['as'=>'alpha_admin_media_upload_audio','uses'=>'Admin\Media@uploadAudio']);

				Route::get('video',['as'=>'alpha_admin_media_video','uses'=>'Admin\Media@video']);
				Route::post('upload-video',['as'=>'alpha_admin_media_upload_video','uses'=>'Admin\Media@uploadVideo']);

				Route::get('other',['as'=>'alpha_admin_media_other','uses'=>'Admin\Media@other']);
				Route::post('upload-other',['as'=>'alpha_admin_media_upload_other','uses'=>'Admin\Media@uploadOther']);

				Route::post('embed-video-media',['as'=>'alpha_admin_media_embed_video','uses'=>'Admin\Media@embedVideo']);
				Route::get('entry-media',['as'=>'alpha_admin_media_entry','uses'=>'Admin\Media@entryMedia']);

				

				Route::post('delete-media',['as'=>'alpha_admin_media_delete','uses'=>'Admin\Media@delete']);

			});	

		});

	});

});