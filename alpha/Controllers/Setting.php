<?php 

namespace Alpha\Controllers;

/**
* 
*/
class Setting extends \AlphaController
{
	
	function __construct()
	{
		parent::__construct();
		$this->layout->active = 'setting_general';
	}
	public function getPlugins(){

		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33]))
		{
               abort(403, 'Unauthorized action.');
        }

        $plugins = app('AlphaPlugin')->plugins;
        $providers = array();
        
        if(!empty($plugins))
        {
        	foreach ($plugins as $key => $value) {
        		$providers[] = $value->provider;
        	}
        }

        $parsedSettings = array();

        if(!empty($providers))
        {

	        $settings = \Setting::whereIn('setting_key',$providers)->whereBundle('alpha.plugins')->get();

	        foreach ($settings as $key => $value) {
	        	$parsedSettings[$value->setting_key] = $value;
	        }

        }
        if(empty($plugins)) $plugins = array();
        
        $this->layout->title = setPageTitle("Plugins");	
		$this->layout->content = view('alpha::admin.setting.plugins',[
			'plugins' => $plugins,
			'parsedSettings' => $parsedSettings,
			]);

	}
	public function getStatusPlugins($id,$status){

		if(!app('Alpha\Core\AlphaAcl')->multiAccess([33])){
            abort(403, 'Unauthorized action.');
        }
        $setting = \Setting::find($id);
        if(!empty($setting)){
        	$setting->setting_value = $status;
        	$setting->save();
        }
        return redirect()->back()->with('msg','Data Saved!');
	}
	public function getGeneral(){

		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(403, 'Unauthorized action.');
        }

		$settings = [];
		$varsGeneralSettings = ['site_name','site_slogan','site_description','site_keywords'];
		foreach ($varsGeneralSettings as $key => $value) {
			$settings[$value] = app('AlphaSetting')->getSetting($value);
		}
		$this->layout->title = setPageTitle("Setting General");	
		$this->layout->content = view('alpha::limitless.setting.general',[
			'settings' => $settings
			]);
	}
	public function getLabels(){
		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(403, 'Unauthorized action.');
        }
        $this->layout->title = setPageTitle("Setting Labels");	
		$this->layout->content = view('alpha::admin.setting.label',[]);

	}
	public function getExtras(){
		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(403, 'Unauthorized action.');
        }
        $this->layout->title = setPageTitle("Setting Extras");	
		$this->layout->content = view('alpha::admin.setting.extra',[]);

	}
	public function getContacts(){
		if(!app('Alpha\Core\AlphaAcl')->multiAccess([34,33])){
               abort(403, 'Unauthorized action.');
        }
        $this->layout->title = setPageTitle("Setting Contacts");	
		$this->layout->content = view('alpha::admin.setting.contact',[]);

	}

	public function postGeneral(){
		
		if(!app('Alpha\Core\AlphaAcl')->multiAccess([33])){
            abort(403, 'Unauthorized action.');
        }



		$input = \Request::all();
		unset($input['_token']);
		foreach ($input as $key => $value) {
			app('AlphaSetting')->updateSetting($key,$value,'yes');
		}
		return redirect()->back()->with('success','Data Saved!');
	}
}