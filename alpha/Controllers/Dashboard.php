<?php 

namespace Alpha\Controllers;

/**
* 
*/
class Dashboard extends \AlphaController
{
	
	function __construct()
	{
		parent::__construct();
	}
	public function index(){
		
		$pages = \Entry::whereEntryType('page')->where('status','!=','deleted')->count();
		$entryTypes = count(app('AlphaSetting')->entries);
		$medias = \Media::count();
		$user = \User::where('status','!=','deleted')->count();
		$roles = \Role::where('id','!=',1)->get();
		$userByRole = array();

		$lastLogin = $this->getTimeElapsedAsText(strtotime(\User::orderBy('last_login','desc')->first()->last_login),time());
		$lastRegister = $this->getTimeElapsedAsText(strtotime(\User::orderBy('created_at','desc')->first()->created_at),time());
		


		foreach ($roles as $key => $value) {
			$userByRole[] = (object)array('legendLabel'=>$value->name,'magnitude'=>$value->users()->where('users.status','!=','deleted')->count());
		}

		$this->layout->title = setPageTitle("Dashboard");	
		$this->layout->content = view('alpha::limitless.test.form',
								['userByRole'=>$userByRole,'user'=>$user,'pages'=>$pages,
								'entryTypes'=>$entryTypes,'medias'=>$medias,'lastLogin'=>$lastLogin,
								'lastRegister' => $lastRegister,'userByRole' =>$userByRole
								]);
	}
	function getTimeElapsedAsText($iTime0, $iTime1 = 0)
	{
		if ($iTime1 == 0) { $iTime1 = time(); }
		$iTimeElapsed = $iTime1 - $iTime0;

		if ($iTimeElapsed < (60)) {
		    return "Less than a minute ago";
		} else if ($iTimeElapsed < (60*60)) {
		    $iNum = intval($iTimeElapsed / 60); $sUnit = "minute";
		} else if ($iTimeElapsed < (24*60*60)) {
		    $iNum = intval($iTimeElapsed / (60*60)); $sUnit = "hour";
		} else if ($iTimeElapsed < (30*24*60*60)) {
		    $iNum = intval($iTimeElapsed / (24*60*60)); $sUnit = "day";
		} else if ($iTimeElapsed < (365*24*60*60)) {
		    $iNum = intval($iTimeElapsed / (30*24*60*60)); $sUnit = "month";
		} else {
		    $iNum = intval($iTimeElapsed / (365*24*60*60)); $sUnit = "year";
		}

		return $iNum . " " . $sUnit . (($iNum <= 1) ? "s" : "") . " ago";
	}
}