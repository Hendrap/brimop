<?php

namespace Alpha\Controllers\Admin;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Taxonomy as Taxonomies;

class Taxonomy extends AlphaController
{

    public function index($type = 'type')
    {
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([12,16])){
            abort(403, 'Unauthorized action.');
        }

        $taxonomies = Taxonomies::where('taxonomy_type','=',$type)->paginate(30);
        $this->layout->title = setPageTitle(ucfirst($type));   
        $this->layout->content = view('alpha::admin.taxonomy.index',[
            'taxonomies' => $taxonomies,
            'type'       => $type,
            ]);
    }

    public function create($type = 'type')
    {
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([12])){
            abort(403, 'Unauthorized action.');
        }

        $taxonomies = Taxonomies::whereTaxonomyType($type)->get();
        $this->layout->active = 'taxonomy_'.$type;
        $this->layout->title = setPageTitle("Create ".ucfirst($type));   
        $this->layout->content = view('alpha::admin.taxonomy.createEdit',[
            'type' => $type,
            'taxonomies' => $taxonomies,
            ]);
    }

    public function edit($type = 'type',$id = 0)
    {

        if(!app('Alpha\Core\AlphaAcl')->multiAccess([12,16])){
            abort(403, 'Unauthorized action.');
        }

       
       $taxonomies = Taxonomies::where('id','!=',$id)->whereTaxonomyType($type)->get();
       $taxonomy = Taxonomies::find($id);
       if(empty($taxonomy)) abort(404);
       $this->layout->title = setPageTitle("Edit ".ucfirst($type));    
       $this->layout->active = 'taxonomy_'.$type;
       $this->layout->content = view('alpha::admin.taxonomy.createEdit',[
            'type' => $type,
            'taxonomy' => $taxonomy,
            'taxonomies' => $taxonomies
            ]);
    }

    public function save($type = 'type',$id = 0,Request $requests)
    {
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([12])){
            abort(403, 'Unauthorized action.');
        }

        $input = \Request::all();
        $this->validate($requests,[
            'name' => 'required'
            ]);
        $taxonomy = Taxonomies::find($id);
        if($id == 0 || empty($taxonomy)){
            $taxonomy = new Taxonomies();
        }

        $slug = str_slug(\Request::input('slug'));
        if(empty($slug)){
            $slug = str_slug($input['name']);
        }

        $check = Taxonomies::where('id','!=',$id)->where('taxonomy_slug','like',$slug.'%')->count();
        if($check > 0){
            $slug .= '-'.$check;
        }
        $taxonomy->taxonomy_slug = $slug;
        $taxonomy->taxonomy_type = $type;
        $taxonomy->parent = \Request::input('parent',0);
        $taxonomy->name = @$input['name'];
        $taxonomy->description = @$input['desc'];
        $taxonomy->save();
        return redirect()->route('alpha_admin_taxonomy_edit',[$type,$taxonomy->id])->with('msg','Data Saved!');

    }
    public function delete($id)
    {
        
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([12])){
            abort(403, 'Unauthorized action.');
        }

       $taxonomy = Taxonomies::find($id);
      
       if(empty($taxonomy)) return redirect()->back()->with('msg','Data Deleted!');
      
       $update = Taxonomies::whereParent($taxonomy->id)->update(['parent'=>0]);
      
       \DB::table('entry_taxonomy')->where('taxonomy_id', '=', $taxonomy->id)->delete();
       $taxonomy->delete();
      
       return redirect()->back()->with('msg','Data Deleted!');

    }
}
