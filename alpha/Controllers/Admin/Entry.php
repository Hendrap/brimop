<?php

namespace Alpha\Controllers\Admin;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Taxonomy as Taxonomies;
use \Entry as Entries;

class Entry extends AlphaController
{
    public function status($entry_id = 0,$status = ''){
        $entry = Entries::find($entry_id);
        if(empty($entry)) abort(404);

        $entry->status = $status;
        $entry->save();
        return redirect()->back()->with('msg','Status Changed!');
    }
    public function index($type = 'type')
    {
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([2,6])){
            abort(403, 'Unauthorized action.');
        }

        $req = app('request');
        $sort = $req->input('sort',0);
        $q = $req->input('q','');


        $total = Entries::whereEntryType($type)->where('status','!=','deleted')->count();
        
        $entries = Entries::with('user')->whereEntryType($type)->where('status','!=','deleted');
        if($sort == 'asc' || $sort == 'desc'){
            $entries = $entries->orderby('title',$sort);
        }

        if($sort == 'oldest' || $sort == 'latest'){
            switch ($sort) {
                case 'oldest':
                    $sort = 'asc';
                    break;
                case 'latest':
                    $sort = 'desc';
                    break;
            }
            $entries = $entries->orderby('created_at',$sort)->orderby('created_at',$sort);
        }
         if($q != ''){
            $entries = $entries->where(function($query)use($q){
                        $query->where('title','like','%'.$q.'%');
                        $query->orWhere('content','like','%'.$q.'%');
            });
        }
        $entries = $entries->paginate(30);

        $info = getEntryConfig($type);
        $last = Entries::getLastUpdate($type);
        $theLastName = 'alpha';
        $theLastDate = date('d F Y H:i:s');
        if(!empty($last->modifiedBy)){
            $theLastName = $last->modifiedBy->username;
            $theLastDate = date('d F Y H:i:s',strtotime($last->updated_at));
        }

        $view = 'alpha::admin.'.$type.'.index';
        if(!\View::exists($view)){
            $view = 'alpha::admin.entry.index';
        }
        $this->layout->title = setPageTitle(ucfirst($type));
        $this->layout->content = view($view,[
                'entries' => $entries,
                'type' => $type,
                'total' => $total,
                'info'  => $info,
                'theLastName'=> $theLastName,
                'theLastDate'=> $theLastDate,
            ]);
    }
    public function getAdditionalInfo($type = ''){

        $metas = getMetaFromEntry($type);

        $taxoInfo = getTaxonomyFromEntry($type);
        $taxoTypes = $taxoInfo->taxoTypes;
        $taxo = $taxoInfo->taxo;
        $taxonomies = getTaxonomiesForEntry($taxoTypes);
        
        $parents = getEntryParents($type);

        return (object)['parents'=>$parents,'taxonomies' => $taxonomies,'metas'=>$metas,'taxo'=>$taxo];
    }
    public function create($type = 'type')
    {
         if(!app('Alpha\Core\AlphaAcl')->multiAccess([2])){
            abort(403, 'Unauthorized action.');
        }
        $config = getEntryConfig($type);
        $info = $this->getAdditionalInfo($type);
        $view = 'alpha::admin.'.$type.'.create';
        if(!\View::exists($view)){
            $view = 'alpha::admin.entry.create';
        }
        $this->layout->title = setPageTitle("Create ".$config['single']);
        $this->layout->content = view($view,[
            'type' => $type,
            'parents' => $info->parents,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
            'config'    => $config,
        ]);
    }

    public function edit($type = 'type',$id = 0)
    {
         if(!app('Alpha\Core\AlphaAcl')->multiAccess([2,6])){
            abort(403, 'Unauthorized action.');
        }

        $this->layout->active = 'entry_'.$type;

        $entry = Entries::with(['taxonomies','metas'])->find($id);
        if(empty($entry)) abort('404');

        $info = $this->getAdditionalInfo($type);
        $config = getEntryConfig($type);

        $selectedTaxo = [];
        $view = 'alpha::admin.'.$type.'.edit';
        if(!\View::exists($view)){
            $view = 'alpha::admin.entry.edit';
        }
        if(!empty($entry->taxonomies)){
            foreach ($entry->taxonomies as $key => $value) {
                $selectedTaxo[] = $value->id;
            }
        }
        $this->layout->title = setPageTitle("Edit ".$config['single']);
        $this->layout->content = view($view,[
            'type' => $type,
            'entry' => $entry,
            'parents' => $info->parents,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
            'selectedTaxo' => $selectedTaxo,
            'config' => $config,
        ]);

    }

    public function save($type = 'type',$id = 0,Request $requests)
    {
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([2])){
            abort(403, 'Unauthorized action.');
        }

        $defaultLang = \Config::get('alpha.application.default_locale');
        
        $this->validate($requests,[
            'title.'.$defaultLang => 'required',
            'content.'.$defaultLang => 'required',
            'status' => 'required'
            ]);

        $input = $requests->input();
       
        $vars = ['title','content','excerpt'];

        foreach ($vars as $key => $value) {
            ${'default'.ucfirst($value)} = @$input[$value][$defaultLang];
            ${$value} = '';
            foreach ($input[$value] as $k => $v) {
                $tmp = '';
                if($id == 0){
                    $tmp = ${'default'.ucfirst($value)};                    
                }
                if(!empty($v)) $tmp = $v;                    

                ${$value} .= '[:'.$k.']'.$tmp;
            }
        }
        
        $entry = Entries::find($id);
        if(empty($entry)){
            $entry = new Entries();
            $entry->author = app('AdminUser')->user->id;
        }

        $slug = \Request::input('slug',str_slug($defaultTitle));
        if(empty($slug)) $slug = str_slug($defaultTitle);
        $check = Entries::where('id','!=',$id)->where('slug','like',$slug.'%')->count();
        if($check > 0){
            $slug .= '-'.$check;
        }
        $metas = array();
        $tmpMetas = app('AlphaSetting')->entries;
        foreach ($tmpMetas as $key => $value) {
            if($value['slug'] == $type){
                $metas = $value['metas'];
            }
        }
        $entry->title = $title;
        $entry->slug = $slug;
        $entry->content = $content;
        $entry->excerpt = $excerpt;
        $entry->entry_type = $type;
        $entry->entry_parent = \Request::input('parent',0);
        $entry->modified_by = app('AdminUser')->user->id;
        $entry->status = $input['status'];
        $entry->published_at = date('Y-m-d H:i:s',strtotime($input['published_at']));
        $entry->touch();
        $entry->save();
        $entry->saveSEO(@$input['seo']);
        $entry->saveEntryMetaFromInput($metas,$input); 

        $entry->taxonomies()->sync([]);  
        $entry->taxonomies()->sync(\Request::input('taxonomies',[]));
        
        if(!empty($input['taxonomies'])){
            foreach ($input['taxonomies'] as $key => $value) {
                $tmpTaxo = Taxonomies::find($value);
                $tmpTaxo->count = $tmpTaxo->count + 1;
                $tmpTaxo->save();
            }
        }
        
        \DB::table('entry_media')->whereEntryId($entry->id)->delete();

        $medias = \Request::input('media',[]);
        $medias = array_reverse(\Request::input('media',[]));            
        
              
       if(!empty($medias)){
            foreach ($medias as $key => $value) {
                $entry->medias()->attach($value);
            }
       }




        \Search::rebuild($entry);

        return redirect()->route('alpha_admin_entry_edit',[$type,$entry->id])->with('msg','Data Saved!');

    }

    public function delete($id)
    {
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([2])){
                abort(403, 'Unauthorized action.');
        }
       $entry = Entries::find($id);
      
       if(empty($entry)) return redirect()->back()->with('msg','Data Deleted!');
      
       $update = Entries::whereEntryParent($entry->id)->update(['entry_parent'=>0]);
      
       $entry->status = 'deleted';
       $entry->save();
      
       return redirect()->back()->with('msg','Data Deleted!');
    }
}
