<?php

namespace Alpha\Controllers\Admin;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Setting as Settings;

class User extends AlphaController
{
    public $metas;
    public function __construct(){
        parent::__construct();
        $metas = unserialize(app('AlphaSetting')->getSetting('user_default'));
        $this->metas  = @$metas['metas'];
        $this->layout->active = 'user_edit';
    }
    public function myAccount(){
        $user = app('AdminUser')->user;
        if($user->id == 1){
            abort(404);
        }
        if(empty($user)) abort(404);

        $this->layout->title = setPageTitle("My Account");
        $this->layout->content = view('alpha::admin.user.account',[
            'user' => $user,
            ]);

    }
    public function saveMyAccount(Request $request){
        $input = \Request::all();
        
        $this->validate($request,[
            'username' => 'required|alpha_dash',
            'email' => 'required|email',
            'password' => 'confirmed'
        ]);

        //validasi edit-create
        unset($input['_token']);
        $user = \User::find($input['id']);
        if($input['id'] == 0 || empty($user)){
            $user = new \User();
        }
        //email validation
        $check = \User::whereEmail($input['email'])->where('id','!=',$input['id'])->count();
        if($check > 0){
             return redirect()->back()->with('msg','Email has been taken before !');
        }
        //username
        $check = \User::where('id','!=',$input['id'])->where('username','like',$input['username'].'%')->count();
        if($check > 0){
            $input['username'] = $input['username'].'-'.$check;
        }
        //user 
        $user->email = $input['email'];
        $user->username = $input['username'];
     
        if(!empty($input['password'])){
            $user->password = bcrypt($input['password']);
        }
        $user->modified_by = app('AdminUser')->user->id;
        $user->touch();
        $user->save();
        return redirect()->back()->with('msg','Data Saved!');
    }
    public function status($id = 0,$status = ''){
        
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([22])){
            abort(403, 'Unauthorized action.');
        }

        $user = \User::find($id);
        if(empty($user)) abort(404);

        $user->status = $status;
        $user->save();
        return redirect()->back()->with('msg','Status Changed!');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([22,26])){
            abort(403, 'Unauthorized action.');
        }
        
        $total = \User::where('id','!=',1)->where('status','!=','deleted')->count();
        $last = \User::getLastUpdate();
        $theLast = 'Alpha';
        $theLastTime = date('d F Y H:i:s');
        $roles = \Role::where('id','!=',1)->get();
        if(!empty($last->modifiedBy)){
            $theLastTime = date('d F Y H:i:s',strtotime($last->updated_at));
            $theLast = $last->modifiedBy->username;
        }

        $req = app('request');
        $role = $req->input('role',0);
        $sort = $req->input('sort',0);
        $q = $req->input('q','');

        $users = array();
        if($role != 0){
            $role = \Role::find($role);
            $users = $role->users();
        }else{
            $users = \User::with('roles');
        }
 
        if($sort == 'asc' || $sort == 'desc'){
            $users = $users->orderby('username',$sort)->orderby('email',$sort);
        }
        if($sort == 'oldest' || $sort == 'latest'){
            switch ($sort) {
                case 'oldest':
                    $sort = 'asc';
                    break;
                case 'latest':
                    $sort = 'desc';
                    break;
            }
            $users = $users->orderby('created_at',$sort)->orderby('created_at',$sort);
        }
     
        if($q != ''){
            $users = $users->where(function($query)use($q){
                        $query->where('username','like','%'.$q.'%');
                        $query->orWhere('email','like','%'.$q.'%');
            });
        }
        
        $users = $users->where('users.id','!=',1)->where('status','!=','deleted')->groupBy('users.id')->paginate(30);
        $this->layout->title = setPageTitle("Accounts");   
        $this->layout->content = view('alpha::admin.user.index',['users' => $users,'total'=>$total,'theLast'=>$theLast,'theLastTime'=>$theLastTime,'roles'=>$roles]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         if(!app('Alpha\Core\AlphaAcl')->multiAccess([22])){
            abort(403, 'Unauthorized action.');
         }
         $roles = \Role::where('id','!=',1)->get();
         $this->layout->title = setPageTitle("Create Account");  
         $this->layout->content = view('alpha::admin.user.createEdit',[
            'metas' => $this->metas,
            'roles' => $roles,
         ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if(!app('Alpha\Core\AlphaAcl')->multiAccess([22])){
            abort(403, 'Unauthorized action.');
        }
        $input = \Request::all();
        
        $this->validate($request,[
            'username' => 'required|alpha_dash',
            'email' => 'required|email',
            'password' => 'confirmed'
        ]);

        //validasi edit-create
        unset($input['_token']);
        $user = \User::find($input['id']);
        if($input['id'] == 0 || empty($user)){
            $user = new \User();
        }
        //email validation
        $check = \User::whereEmail($input['email'])->where('id','!=',$input['id'])->count();
        if($check > 0){
             return redirect()->back()->with('err','Email has been taken before !');
        }
        //username
        $check = \User::where('id','!=',$input['id'])->where('username','like',$input['username'].'%')->count();
        if($check > 0){
            $input['username'] = $input['username'].'-'.$check;
        }
        //user 
        $user->email = $input['email'];
        $user->username = $input['username'];
        $user->status = $input['status'];
        if(!empty($input['password'])){
            $user->password = bcrypt($input['password']);
        }
        $user->modified_by = app('AdminUser')->user->id;
        $user->touch();
        $user->save();
        //user metas
        $user->saveUserMetaFromInput($this->metas,$input);
        //role
        $user->roles()->attach($input['role']);


        return redirect()->route('admin.user.edit',[$user->id])->with('msg','Data Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([22,26])){
            abort(403, 'Unauthorized action.');
        }


        if($id == 1){
            abort(404);
        }
        $roles = \Role::where('id','!=',1)->get();
        $user = \User::with(['metas','roles'])->find($id);
        if(empty($user)) abort(404);
        $this->layout->title = setPageTitle("Edit Account");   
        $this->layout->active = 'user';
        $this->layout->content = view('alpha::admin.user.createEdit',[
            'user'=>$user,
            'roles' => $roles,
            'metas' => $this->metas
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([22])){
            abort(403, 'Unauthorized action.');
        }

        if($id == 1){
            return redirect()->back()->with('msg','Data Deleted!');
        }
        $user = \User::find($id);
        if(!empty($user)){
            $user->status = 'deleted';
            $user->save();
        }
        return redirect()->back()->with('msg','Data Deleted!');
    }
}
