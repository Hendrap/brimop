<?php

namespace Alpha\Controllers\Admin;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Taxonomy as Taxonomies;
use \Entry as Entries;
use \Media as Medias;
use Closure;
use Exception;
class Media extends AlphaController
{
	public function entryMedia(){
		$media = Medias::orderBy('created_at','desc');
		if(isset($_GET['editor'])){
			$media = $media->where('media_type','=','image');
		}

		if(isset($_GET['search'])){
			$keyword = $_GET['search'];
			$media = $media->where('title','like',"%$keyword%");
		}
		$media = $media->paginate(30);
		if(!empty($_GET)){
			foreach ($_GET as $key => $value) {
				$media = $media->appends([$key => $value]);
			}
		}
		$res = parseMedias($media);

		$paginate = (string)$media->render();
		return json_encode(array('paginate'=>$paginate,'res'=>$res));
	}
	public function delete(){
		$mediaId = app('request')->input('id');
		$media = Medias::find($mediaId);
		if(empty($media)) return redirect()->back();
		
		if($media->media_type == 'image'){
			$profiles = app('AlphaSetting')->images;
			foreach ($profiles as $key => $value) {
				$path = getCropImage($media->path,$value['name']);
				@unlink(base_path('public/'.$path));
			}
		}
		@unlink(base_path('public/'.$media->path));
		$media->delete();
		return redirect()->back()->with('msg','Data deleted!');
	
	}
	public function embedVideo(){

		$url = app('request')->input('url');
		$infoVideo = checkVideoURL($url);
		$imgPath = '';
		$title = '';

		if($infoVideo['type'] == 'youtube'){
			$imgPath = getImageFromEmbedVideo('youtube',$infoVideo);
			$title = $infoVideo["parsed_param"]["v"];
		}
		if($infoVideo['type'] == 'vimeo'){
			$infoVideo = @json_decode(file_get_contents("http://vimeo.com/api/oembed.json?url=".urlencode($url)), true);
			$imgPath = getImageFromEmbedVideo('youtube',$infoVideo);			
			$title = $infoVideo["video_id"];
		}

		$title .= ' - '.time();

		$media = new Medias();
		$media->title = $title;
		$media->media_type = 'video';
		$media->url = $url;
		$media->path = $imgPath;
		$media->author = app('AdminUser')->user->id;
		$media->save();

		return json_encode(array('status'=>1));


	}
	public function uploader($type = 'image',Closure $callback){
		
		$this->layout = "";
		$req = app('request');
		$file = @$req->file($type)[0];
		$info = [];
		$info  = pathinfo($_FILES[$type]['name'][0]);
		$info['size'] = $file->getClientSize();
		$info['mime_type'] = $file->getClientMimeType();
		
		$extension = $file->getClientOriginalExtension();
		if($type != 'other'){
			if(!in_array($extension, \Config::get('alpha.media.'.$type))) throw new Exception("Invalid file format", 1);
			
		}

		$title = $info['filename'];
		$name = $title.time().'.'.$extension;	
		$dest = str_replace('\\','/',base_path().'/public/uploads/'.date('Y').'/'.date('m'));

		if(!is_dir($dest)) mkdir($dest,0777,true);

		$file->move($dest,$name);
		
		$media = new Medias();
		$media->title = $title;
		$media->description = '';
		$media->media_type = $type;
		$media->media_mime = $file->getClientMimeType();
		$media->media_meta = serialize($info);
		$media->url = '';
		$media->path = 'uploads/'.date('Y').'/'.date('m').'/'.$name;
		$media->author = app('AdminUser')->user->id;
		$media->save();

		$params = [];
		$params['title'] = $title;
		$params['file'] = $file;
		$params['info'] = $info;
		$params['name'] = $name;
		$params['media'] = $media;

		$callback($params);

	}
	public function video(Request $req){

		$search = $req->input('search','');
		$videos = Medias::where('media_type','=','video')->where('title','like',"%$search%")->paginate(30);
		$this->layout->title = setPageTitle("Video");
		$this->layout->content = view('alpha::admin.test.media',[
			'medias' => $videos,
			'media_type' => 'video',
			'search' => $search
			]);
	}
	public function uploadVideo(){
		try {
			$this->uploader('video',function($params){
				
			});
			return json_encode(array('status'=>1));		
		} catch (Exception $e) {
			return json_encode(array('status'=>0,'msg'=>$e->getMessage()));	
		}
	}
	public function other(Request $req){

		$search = $req->input('search','');
		$others = Medias::where('media_type','=','other')->where('title','like',"%$search%")->paginate(30);
		$this->layout->title = setPageTitle("Other");	
		$this->layout->content = view('alpha::admin.test.media',[
			'medias' => $others,
			'media_type' => 'other',
			'search' => $search
			]);
	}
	public function uploadOther(){
		try {
			$this->uploader('other',function($params){
				
			});
			return json_encode(array('status'=>1));		
		} catch (Exception $e) {
			return json_encode(array('status'=>0,'msg'=>$e->getMessage()));	
		}
	}
	public function audio(Request $req){
		$search = $req->input('search','');
		$audio = Medias::where('media_type','=','audio')->where('title','like',"%$search%")->paginate(30);
		$this->layout->title = setPageTitle("Audio");
		$this->layout->content = view('alpha::admin.test.media',[
			'medias' => $audio,
			'media_type' => 'audio',
			'search' => $search
			]);
	}
	public function uploadAudio(){
		try {
			$this->uploader('audio',function($params){
				
			});
			return json_encode(array('status'=>1));		
		} catch (Exception $e) {
			return json_encode(array('status'=>0,'msg'=>$e->getMessage()));	
		}
	}

	public function image(Request $req)
	{
		$search = $req->input('search','');
		$medias = Medias::with(['user'])->where('media_type','=','image')->where('title','like',"%$search%")->orderBy('id','desc')->paginate(30);
		$this->layout->title = setPageTitle("Image");
		$this->layout->content = view('alpha::admin.test.media',[
			'medias' => $medias,
			'media_type' => 'image',
			'search' => $search
			]);
	}

	public function uploadImage()
	{
		try {
			$response = $this->uploader('image',function($params){
				extract($params);
				$crops = app('AlphaSetting')->images;
				foreach ($crops as $key => $value) {
					$crop = false;
					$ratio = false;
					if($value['crop'] == 'yes'){
						$crop = true;
					}
					if(@$value['aspectratio'] == 'yes'){
						$ratio = true;
					}

					cropImage('uploads/'.date('Y').'/'.date('m').'/'.$name,$value['name'],$value['width'],$value['height'],$crop,$ratio);
				}
			});	
			return json_encode(array('status'=>1));		
		} catch (Exception $e) {
			return json_encode(array('status'=>0,'msg'=>$e->getMessage()));		
		}
		
	}
}	
