<?php 

namespace Alpha\Controllers;

/**
* 
*/
class Credential extends \AlphaController
{
	
	function __construct()
	{

	}
	
	public function login()
	{
		//32 adalah backend managed lihat database ,table rules
		if(\Auth::user() && app('Alpha\Core\AlphaAcl')->multiAccess([32])){
			return redirect()->route('alpha_admin_index');	  
		}
		return view('alpha::admin.login')->with(['pageTitle' => 'Alpha Login']);
		
	}

	public function logout()
	{
		\Auth::logout();
		 return redirect()->route('alpha_get_login');
	}

	public function postLogin()
	{

		$errors = [];
		$input = \Request::all();

		$rules = [
		'email' => 'required|email',
		'password' => 'required',
		];
	
		$v = \Validator::make($input,$rules);

		if ($v->fails()){
			foreach ($v->errors()->messages() as $key => $value) {
				foreach ($value as $v) {
					$errors[] = $v;
				}
			}
	        return redirect()->back()->with('errors',$errors);
	    }

	    //buang csrf token
	    unset($input['_token']);

	    //ambil remember
	    $remember = @$input['remember'] or false;
	    unset($input['remember']);

	    //ambil redirect
	    $redirect = @$input['redirect'] or false;
	    unset($input['redirect']);

	    if(\Auth::attempt($input,$remember)){
	    	
	    	$user = \User::whereEmail($input['email'])->first();
	    	$user->last_login = date('Y-m-d H:i:s');
	    	$user->save();
	    	//redirect ke tempat yg mau dituju sebelumnya
	    	if($redirect){
		    	return redirect()->away($redirect);	    		
	    	}

	    	return redirect()->route('alpha_admin_index');	  

	    }else{

	    	$errors[] = 'Password & email doesn\'t match';
	    	return redirect()->back()->with('errors',$errors);

	    }

	}
}