<?php

namespace Alpha\Controllers;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;
use \Role as Roles;

class Role extends AlphaController
{
    public function __construct(){
        parent::__construct();
        $this->layout->active = 'setting_role';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([27,31])){
            abort(403, 'Unauthorized action.');
        }

        $roles = Roles::with('rules')->where('id','!=',1)->get();
        $this->layout->title = setPageTitle("Roles And Access");   
        $this->layout->content = view('alpha::limitless.setting.role.index',[
            'roles' => $roles
            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([27])){
            abort(403, 'Unauthorized action.');
        }
        $this->layout->title = setPageTitle("Create Role");   
        $this->layout->content = view('alpha::admin.setting.role.createEdit',[
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([27])){
            abort(403, 'Unauthorized action.');
        }

        $input = \Request::all();
        
        $validateRules = 'required';

        if($input['id'] == 0){
           $validateRules .= '|unique:roles';
        }

        $this->validate($request,[
            'name' => $validateRules
        ]);
        
        if($input['id'] == 0){
            $role = new Roles();
            $role->save();
            $input['id'] = $role->id;
        }

        $role = Roles::findOrFail($input['id']);
        $role->update($request->all());
        if(!empty($input['rules'])){
            $role->rules()->sync($input['rules']);
        }

        return redirect()->route('admin.setting.role.edit',[$role->id])->with('msg','Data Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([27,31])){
            abort(403, 'Unauthorized action.');
        }

        if($id == 1){
            abort(404);
        }
        $role = Roles::find($id);
        if(empty($role)) abort(404);
        $this->layout->title = setPageTitle("Edit Role");   
        $this->layout->content = view('alpha::admin.setting.role.createEdit',[
            'role'=>$role,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(!app('Alpha\Core\AlphaAcl')->multiAccess([27])){
            abort(403, 'Unauthorized action.');
        }

        Roles::find($id)->delete();
        return redirect()->back()->with('msg','Data Deleted!');
    }
}
