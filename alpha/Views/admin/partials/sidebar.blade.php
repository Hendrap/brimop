      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{asset("backend/dist/img/user2-160x160.jpg")}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Cadis Etrama Di Raizel</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
      
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Settings</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('alpha_admin_get_setting_general')}}"><i class="fa fa-circle-o"></i> General</a></li>
                <li><a href="{{route('admin.setting.role.index')}}"><i class="fa fa-circle-o"></i> Roles And Access</a></li>
                <li><a href="{{route('admin.setting.entry_type.index')}}"><i class="fa fa-circle-o"></i> Entry Types</a></li>
                <li><a href="{{route('admin.setting.taxonomy_type.index')}}"><i class="fa fa-circle-o"></i> Taxonomy Types</a></li>
                <li><a href="{{route('admin.setting.user_type.index')}}"><i class="fa fa-circle-o"></i> User Types</a></li>
                <li><a href="{{route('admin.setting.image.index')}}"><i class="fa fa-circle-o"></i> Image Types</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Contact</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Users</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('admin.user.index')}}"><i class="fa fa-circle-o"></i> View</a></li>
                <li><a href="{{route('admin.user.create')}}"><i class="fa fa-circle-o"></i> Create</a></li>
              </ul>
            </li>

             <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Media Library</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('alpha_admin_media_image')}}"><i class="fa fa-circle-o"></i> Image</a></li>
                <li><a href="{{route('alpha_admin_media_audio')}}"><i class="fa fa-circle-o"></i> Audio</a></li>
                <li><a href="{{route('alpha_admin_media_video')}}"><i class="fa fa-circle-o"></i> Video</a></li>
                <li><a href="{{route('alpha_admin_media_other')}}"><i class="fa fa-circle-o"></i> Other</a></li>
              </ul>
            </li>

             <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Taxonomies</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                  <?php $taxonomies = app('AlphaSetting')->taxonomies; ?>
                  @foreach($taxonomies as $value)
                      @if($value['show_ui'] == 'yes')
                       <li><a href="{{ route('alpha_admin_taxonomy_index',[$value['slug']]) }}"><i class="fa fa-circle-o"></i> {{ucfirst($value['single'])}}</a></li>
                       @endif
                  @endforeach
                  
              </ul>
            </li>
            <?php $entries = app('AlphaSetting')->entries; ?>
            @foreach($entries as $value)
              @if($value['show_ui'] == 'yes')
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>{{ $value['plural'] }}</span>
                     <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{ route('alpha_admin_entry_index',[$value['slug']]) }}"><i class="fa fa-circle-o"></i> View {{$value['single']}}</a></li>
                    <li><a href="{{ route('alpha_admin_entry_create',[$value['slug']]) }}"><i class="fa fa-circle-o"></i> Create {{$value['single']}}</a></li>
                  </ul>
                </li>
               @endif
            @endforeach



          
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>