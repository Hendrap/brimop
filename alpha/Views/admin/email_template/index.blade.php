<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">{{$info['plural']}}</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a href="{{ route('alpha_admin_entry_create',[$type]) }}"><i class="icon-plus3"></i></a></li>
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="panel-heading">
						<hr>
						<div class="row">
							<div class="col-md-4">
								<b>{{$total}} {{$info['plural']}}</b> -- Last updated by {{$theLastName}} on {{$theLastDate}}
							</div>
							<div class="col-md-2"></div>
							<form>
							<div class="col-md-3">
								<input name="q" placeholder="Type Title or Description" type="text" class="form-control">
							</div>

							<div class="col-md-2">
						
								<select name="sort" class="form-control">
									<option value="0">Sort</option>
									<option value="asc">Ascending</option>
									<option value="desc">Descending</option>
									<option value="oldest">Oldest</option>
									<option value="latest">Latest</option>
								</select>
							</div>	
							<div class="col-md-1">
								<button type="submit" class="btn btn-info">Search</button>
							</div>
							</form>

						</div>
						<hr>
					</div>
				@if(session('msg'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-success">{{session('msg')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Title</th>
						<th>User</th>
						<th>Status</th>
						<th>Created at</th>
						<th>Update at</th>
						<th></th>
					</tr>

					@foreach($entries as $entry)
					<tr>
						<td>{{parseMultiLang($entry->title)}}</td>
						<td>{{@$entry->user->username}}</td>
						<?php 
						$className = 'default';
						switch ($entry->status) {
							case 'published':
								$className = 'success';
								break;
							case 'draft':
								$className = 'info';
								break;
							case 'disabled':
								$className = 'danger';
								break;	
							default:
								# code...
								break;
						}
						 ?>
						<td><span class="label label-{{ $className }}">{{ ucfirst($entry->status) }}</span></td>
						<td>{{ date('d F Y',strtotime($entry->created_at)) }}</td>
						<td>{{ date('d F Y',strtotime($entry->updated_at)) }}</td>

						<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">
<li><a href="{{ route('alpha_admin_entry_edit',[$type,$entry->id]) }}"><i class="icon-pencil"></i> Edit</a></li>
		<?php if($entry->status == 'published'){ ?>
		<li>
			<a href="{{ route('alpha_admin_entry_status',[$entry->id,'disabled']) }}">
			<i class=" icon-cancel-square"></i> Disable</a>
		</li>
		<?php }else{ ?>
		<li>
			<a href="{{ route('alpha_admin_entry_status',[$entry->id,'published']) }}">
			<i class=" icon-checkmark4"></i> Publish</a>
		</li>
		<?php } ?>


											
											<li><a class="confirm" data-id="{{$entry->id}}"  href="#"><i class=" icon-x"></i> Delete</a></li>
											
										</ul>
								</div>
						</td>				
					</tr>
					@endforeach
				</table>
			</div>
			
		
		<div class="row">
			<div class="col-md-6">
				<?php
					 if(!empty($_GET)){
						foreach ($_GET as $key => $value) {
							$entries = $entries->appends([$key => $value]);
						}
					 }
				?>
				{!! $entries->render() !!}
			</div>
		</div>
<form style="display:none" id="form_delete" action="{{route('alpha_admin_entry_delete',[0])}}" method="POST">
	{{csrf_field()}}
	<input type="hidden" name="id" value="0" >
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				$("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
				$("#form_delete").find("[name=id]").val($(this).data('id'));
				document.getElementById("form_delete").submit();
			}
		});

	});
	
</script>