<div class="content-wrapper">
	<section class-"content-header">
			 <h1>
                Setting
              </h1>
              <ol class="breadcrumb">
                <li>Dashboard</li>
                <li>Setting</li>
                <li class="active">General</li>

              </ol>
	</section>
	<section class="content">
         <div class="row">
         	<div class="col-md-6">
         		<div class="box">
         		
                <div class="box-header with-border">
                  <h3 class="box-title">General</h3>
                </div>
                @if(session('success'))
           			<div class="alert alert-success">
           				{{session('success')}}
           			</div>
         		   @endif
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="POST" action="">
                  <div class="box-body">
                    <?php foreach ($settings as $key => $value) { ?>
                    <div class="form-group">
                      <label for="{{$key}}">{{ucfirst(str_replace('_',' ',$key))}}</label>
                      <input name="{{$key}}" type="text" class="form-control" id="{{$key}}" value="{{$value}}">
                    </div>
               		<?php } ?>
                  
                  </div><!-- /.box-body -->
                  {{ csrf_field() }}
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </form>
              </div>
         	</div>
         </div>
    </section> 
</div>