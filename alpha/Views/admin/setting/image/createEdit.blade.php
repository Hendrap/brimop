<?php 
	$name = old('name');
	$width = old('width');
	$height = old('height');
	$crop = old('crop');
	$aspectratio = old('aspectratio');
	if(!empty($setting)){
		$value = unserialize($setting->setting_value);
		$name = old('name',@$value['name']);
		$width = old('width',@$value['width']);
		$height = old('height',@$value['height']);
		$crop = old('crop',@$value['crop']);
		$aspectratio = old('aspectratio',@$value['aspectratio']);
	}
	if(empty($setting)){
		$setting = new StdClass();
		$setting->id = 0;
	}

 ?>

<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title"><?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Image Type</h5>
					</div>
	<div class="panel-body">
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<form class="form-horizontal" action="{{route('admin.setting.image.store')}}" method="POST">
				<fieldset class="content-group">

				<div class="form-group">
					<label class="control-label col-lg-2">Name</label>
					<div class="col-lg-10">
						<input name="name" type="text" class="form-control" value="{{$name}}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Width</label>
					<div class="col-lg-10">
						<input name="width" type="text" class="form-control" value="{{$width}}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Height</label>
					<div class="col-lg-10">
						<input name="height" type="text" class="form-control" value="{{$height}}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Crop ?</label>
					<div class="col-lg-10">
						<select name="crop"  class="form-control">
						<?php $options = ['yes','no']?>
						@foreach($options as $v)
						<option {{ ($v == $crop ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
						@endforeach
					</select>
					</div>
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Aspect Ratio ?</label>
					<div class="col-lg-10">
							<select name="aspectratio"  class="form-control">
						<?php $options = ['yes','no']?>
						@foreach($options as $v)
						<option {{ ($v == $aspectratio ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
						@endforeach
					</select>
					</select>
					</div>
				</div>

				

			
					
			
					{{csrf_field()}}
				
					<input type="hidden" name="id" value="{{$setting->id}}">	
					<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
			</div>
					</fieldset>
				</form>
			</div>
		</div>