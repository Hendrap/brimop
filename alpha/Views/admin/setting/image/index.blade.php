<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">Image Types</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a href="{{ route('admin.setting.image.create') }}"><i class="icon-plus3"></i></a></li>
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Name</th>
						<th>Width</th>
						<th>Height</th>
						<th>Crop ?</th>
						<th></th>
					</tr>

					@foreach($crops as $crop)
						<tr>
							<?php $value = unserialize($crop->setting_value) ?>
							<td>{{ ucfirst($value['name']) }}</td>
							<td>{{ @$value['width'] }}</td>
							<td>{{ @$value['height'] }}</td>
							<td>{{ ucfirst(@$value['crop']) }}</td>

							<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="{{ route('admin.setting.image.edit',[$crop->id]) }}"><i class="icon-pencil"></i> Edit</a></li>
											<li><a class="confirm" data-id="{{$crop->id}}"  href="{{ route('admin.setting.image.destroy',[$crop->id]) }}?_method=delete"><i class=" icon-x"></i> Delete</a></li>
											
										</ul>
								</div>
							</td>
						</tr>
					@endforeach
				</table>
</div>
<form style="display:none" id="form_delete" action="{{route('admin.setting.image.destroy',[0])}}" method="POST">
	{{csrf_field()}}
	<input type="hidden" name="id" value="0" >
	{{ method_field('DELETE') }}
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				$("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
				$("#form_delete").find("[name=id]").val($(this).data('id'));
				$("#form_delete").submit();
			}
		});

	});
	
</script>