<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title">Labels</h5>
					</div>
	<div class="panel-body">
		@if(session('success'))
           			<div class="alert alert-success">
           				{{session('success')}}
           			</div>
         		   @endif
		<form class="form-horizontal" method="POST" action="{{ route('alpha_admin_post_setting_general') }}">
			{{ csrf_field() }}
			<fieldset class="content-group">
				
				<div class="form-group">
					<label class="control-label col-lg-2">Label Home</label>
					<div class="col-lg-10">
						 <input name="label_home" type="text" class="form-control" id="label_home" value="{{ app('AlphaSetting')->getSetting('label_home') }}">
					</div>
				</div>
				
			</fieldset>
			<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
			</div>
		</form>
	</div>					
</div>