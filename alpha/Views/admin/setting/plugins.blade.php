				<!-- Small table -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Plugins</h5>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                	</ul>
	                	</div>
					</div>

					@if(session('msg'))
						<div class="alert alert-success">{{session('msg')}}</div>
					@endif

					<!-- <div class="table-responsive"> -->
						<table class="table-responsive table table-sm">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Status</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($plugins as $key => $plugin)
								<?php $setting = $parsedSettings[$plugin->provider]; ?>
								@if(!empty($setting))
								<tr>
									<td>{{ $key + 1 }}</td>
									<td>{{ $plugin->name }}</td>
									<?php 
										
										if($setting->setting_value == 'yes'){
											$class = 'success';$label = 'Active';
										}else{
											$class = 'danger';$label = 'Disable';
										}
									 ?>
									<td><label class="label label-{{$class}}">{{$label}}</label></td>
									<td>
										<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">
				                    		@if($setting->setting_value  == 'yes')
				                    			<li><a href="{{ route('alpha_admin_get_status_plugins',[$setting->id,'no']) }}">Disable</a></li>
				                    		@else
				                    			<li><a href="{{ route('alpha_admin_get_status_plugins',[$setting->id,'yes']) }}">Enable</a></li>
				                    		@endif
										</ul>
										</div>
									</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					<!-- </div> -->
				</div>
				<!-- /small table -->






					