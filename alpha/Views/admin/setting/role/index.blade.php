<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Name</th>
						<th></th>
						<th><a href="{{ route('admin.setting.role.create') }}" class="btn btn-success">Add</a></th>
					</tr>

					@foreach($roles as $role)
					<tr>
						<td>{{$role->name}}</td>
						<td><a href="{{ route('admin.setting.role.edit',[$role->id]) }}"><button class="btn btn-info">Edit</button></a></td>
						<td><a data-id="{{$role->id}}" href="{{ route('admin.setting.role.destroy',[$role->id]) }}?_method=delete" class="confirm btn btn-danger">Delete</a></td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</section>
</div>
<form style="display:none" id="form_delete" action="{{route('admin.setting.role.destroy',[0])}}" method="POST">
	{{csrf_field()}}
	<input type="hidden" name="id" value="0" >
	{{ method_field('DELETE') }}
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				$("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
				$("#form_delete").find("[name=id]").val($(this).data('id'));
				$("#form_delete").submit();
			}
		});

	});
	
</script>