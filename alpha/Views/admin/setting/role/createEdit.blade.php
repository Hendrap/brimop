<?php 

	if(empty($role)){
		$role = new StdClass();
		$role->id = 0;
		$role->name = '';
		$role->rules = array();
	}

	$roleRules = [];
	$tmpRules = Rule::where('id','!=',1)->get();
	$rules = array();
	
	foreach ($tmpRules as $key => $value) {
		@$rules[$value->group][] = $value->id;
	}

	if(!empty($role)){
		foreach ($role->rules as $key => $value) {
			$roleRules[] = $value->id;
		}
	}

	$roleRules = old('rules',$roleRules);

?>
<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title">Role</h5>
					</div>
				<div class="panel-body">
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<form action="{{route('admin.setting.role.store')}}" method="POST">
					<input placeholder="Role Name" name="name" type="text" class="form-control" value="{{old('name',$role->name)}}">
					<br>
					<br>
					<input type="hidden" name="id" value="{{$role->id}}">
					<table class="table table-responsive">
						<tr>
							<th>Name</th>
							<th>Write</th>
							<th>Read</th>
						</tr>

						@foreach($rules as $key => $rule)
						<tr>
							<td>{{$key}}</td>
							@foreach($rule as $r)
							<td><input <?php echo in_array($r, $roleRules) ? 'checked' : ''; ?> value="{{$r}}" name="rules[]"  type="checkbox"></td>
							@endforeach

							@if(count($rule) < 2)
								<td></td>
							@endif
						</tr>
						@endforeach
						{{csrf_field()}}
						<tr>
							<td></td>
							<td></td>
							<td><button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>