<?php 

	
	
	$slug = old('slug');
	$single = old('single');
	$plural = old('plural');
	$show_ui = old('show_ui');
	if(!empty($setting)){
		$value = unserialize($setting->setting_value);
		$slug = old('slug',@$value['slug']);
		$single = old('single',@$value['single']);
		$plural = old('plural',@$value['plural']);
		$show_ui = old('show_ui',@$value['show_ui']);
	}
	if(empty($setting)){
		$setting = new StdClass();
		$setting->id = 0;
	}

 ?>
<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title"><?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Taxonomy Type</h5>
					</div>
	<div class="panel-body">
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<form class="form-horizontal" action="{{route('admin.setting.taxonomy_type.store')}}" method="POST">
				<fieldset class="content-group">

				<div class="form-group">
					<label class="control-label col-lg-2">Slug</label>
					<div class="col-lg-10">
						<input name="slug" type="text" class="form-control" value="{{$slug}}">
					</div>
				</div>


				
				<div class="form-group">
					<label class="control-label col-lg-2">Label Single</label>
					<div class="col-lg-10">
						<input name="single" type="text" class="form-control" value="{{$single}}">
					</div>
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Label Plural</label>
					<div class="col-lg-10">
						<input name="plural" type="text" class="form-control" value="{{$plural}}">
					</div>
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Show UI ?</label>
					<div class="col-lg-10">
						<select name="show_ui"  class="form-control">
						<?php $options = ['yes','no']?>
						@foreach($options as $v)
						<option {{ ($v == $show_ui ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
						@endforeach
					</select>
					</div>
				</div>

					
					
					<input type="hidden" name="id" value="{{$setting->id}}">	
					<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
					</div>
					</fieldset>
					{{csrf_field()}}
				</form>
			</div>
		</div>