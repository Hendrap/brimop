<?php 
	
	$taxonomies = Setting::whereBundle('alpha.taxonomy')->get();

	$slug = old('slug');
	$single = old('single');
	$plural = old('plural');
	$show_ui = old('show_ui');
	$meta_key = old('meta_key',array());
	$meta_name = old('meta_name',array());
	$meta_data_type = old('meta_data_type',array());
	$entry_taxonomy = old('taxonomies',array());
	
	if(!empty($setting)){
		$value = unserialize($setting->setting_value);
		$slug = old('slug',@$value['slug']);
		$single = old('single',@$value['single']);
		$plural = old('plural',@$value['plural']);
		$show_ui = old('show_ui',@$value['show_ui']);
		$default = @$value['taxonomies'];
		if(empty($default)) $default = [];
		$entry_taxonomy = old('taxonomies',$default);
		$tmp_meta_key = array();
		$tmp_meta_name = array();
		$tmp_meta_data_type = array();
		if(!empty($value['metas'])){
			foreach ($value['metas'] as $k => $val) {
				$tmp_meta_key[] = $val->meta_key;
				$tmp_meta_name[] = $val->meta_name;
				$tmp_meta_data_type[] = $val->meta_data_type;
			}
			
		}

		$meta_key = old('meta_key',$tmp_meta_key);
		$meta_name = old('meta_name',$tmp_meta_name);
		$meta_data_type = old('meta_data_type',$tmp_meta_data_type);
	}
	if(empty($setting)){
		$setting = new StdClass();
		$setting->id = 0;
	}
 ?>

<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title"><?php echo ($setting->id == 0) ? 'Add a new' : 'Edit' ?> Entry Type</h5>
					</div>
	<div class="panel-body">
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<form class="form-horizontal" action="{{route('admin.setting.entry_type.store')}}" method="POST">
				<fieldset class="content-group">

				 <div class="form-group">
					<label class="control-label col-lg-2">Slug</label>
					<div class="col-lg-10">
						<input name="slug" type="text" class="form-control" value="{{$slug}}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Label Single</label>
					<div class="col-lg-10">
						<input name="single" type="text" class="form-control" value="{{$single}}">
					</div>
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Label Plural</label>
					<div class="col-lg-10">
						<input name="plural" type="text" class="form-control" value="{{$plural}}">
					</div>
				</div>
				

				<div class="form-group">
					<label class="control-label col-lg-2">Show Ui ?</label>
					<div class="col-lg-10">
						<select name="show_ui"  class="form-control">
						<?php $options = ['yes','no']?>
						@foreach($options as $v)
						<option {{ ($v == $show_ui ) ? 'selected' : '' }} value="{{$v}}">{{ucfirst($v)}}</option>
						@endforeach
					</select>
					{{csrf_field()}}
					</div>
				</div>



				<div class="form-group">
					<label class="control-label col-lg-2">Taxonomies</label>
					<div class="col-lg-10">
						@foreach($taxonomies as $taxo)
						<?php $value = unserialize($taxo->setting_value) ?>
							<label>{{ $value['single'] }}<input <?php echo in_array($taxo->id, $entry_taxonomy) ? 'checked' : ''  ?> type="checkbox" value="{{$taxo->id}}" name="taxonomies[]"></label>
						@endforeach
					</div>
				</div>



				<div class="form-group">
					<label class="control-label col-lg-2">Metas <a  id="add_meta" href="#"><i class="icon-plus3"></i></a></label>
					<div class="col-lg-10">
						<div id="meta_container" class="row">
					@foreach($meta_key as $thekey => $theval)
						<?php echo view('alpha::admin.setting.entry_type.template.meta',[
							'meta_key' => $theval,
							'meta_name' => $meta_name[$thekey],
							'meta_data_type' => $meta_data_type[$thekey]
						]) ?>
					@endforeach
				</div>	
					</div>
				</div>

			
				
		
			
				
					<input type="hidden" name="id" value="{{$setting->id}}">
					<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
			</div>
				</fieldset>
				</form>
			</div>
			</div>


<script type="text/javascript">
	var meta_template = '<?php echo view('alpha::admin.setting.entry_type.template.meta') ?>';
	$(document).ready(function(){
		$("#add_meta").click(function(e){
			e.preventDefault();
			$("#meta_container").append(meta_template);
		});
		$(document).on('click',".delete_meta",function(e){
			e.preventDefault();
			$(this).parent().parent().parent().remove();
		});
	});
</script>