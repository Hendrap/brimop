<!DOCTYPE html>
<html>
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title or 'Laravel' }}</title>
       <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset("backend/bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset("backend/dist/css/ionicons.min.css")}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("backend/dist/css/AdminLTE.min.css")}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset("backend/dist/css/skins/_all-skins.min.css")}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset("backend/plugins/iCheck/flat/blue.css")}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset("backend/plugins/morris/morris.css")}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset("backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css")}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset("backend/plugins/datepicker/datepicker3.css")}}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- jQuery 2.1.4 -->
    <script src="{{asset("backend/plugins/jQuery/jQuery-2.1.4.min.js")}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset("backend/plugins/jQueryUI/jquery-ui.min.js")}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset("backend/bootstrap/js/bootstrap.min.js")}}"></script>
    <!-- Morris.js charts -->
    <script src="{{asset("backend/dist/js/raphael-min.js")}}"></script>
    {{-- <script src="{{asset("backend/plugins/morris/morris.min.js")}}"></script> --}}
    <!-- Sparkline -->
    <script src="{{asset("backend/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
    <!-- jvectormap -->
    <script src="{{asset("backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")}}"></script>
    <script src="{{asset("backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset("backend/plugins/knob/jquery.knob.js")}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset("backend/dist/js/moment.min.js")}}"></script>
    <!-- datepicker -->
    <script src="{{asset("backend/plugins/datepicker/bootstrap-datepicker.js")}}"></script>
    <!-- Slimscroll -->
    <script src="{{asset("backend/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{asset("backend/plugins/fastclick/fastclick.min.js")}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset("backend/dist/js/app.min.js")}}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script type="text/javascript">
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
    </script>
    
  </head>
 <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @include('alpha::admin.partials.header')
        @include('alpha::admin.partials.sidebar')
        <?php 
            if(!empty($content)){
                echo $content;
            }else{
                echo view('alpha::admin.empty');
            }
        ?>
        @include('alpha::admin.partials.footer')
    </div><!-- ./wrapper -->





</body>
</html>