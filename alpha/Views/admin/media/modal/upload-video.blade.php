<script src="{{ asset('backend/fileupload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('backend/fileupload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-audio.js') }}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-video.js') }}"></script>
<!-- The File Upload validation plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-validate.js') }}"></script>


<div id="modalUploadVideo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="padding:20px">
      	<h2>Upload Video</h2>
        <form style="display:none" name="imageFileUploadVideo" id="imageFileUploadVideo">
          <input type="file" multiple name="video[]">
        </form>
      	<div id="tmpUploadContainerVideo" class="row">
      		
      	</div>
      	<div class="row">
      		<div class="col-md-1">
      			<button class="btn btn-info" id="btnTriggerAddVideo">Add Video</button>
      		</div>
          <div class="col-md-1"></div>
            <div class="col-md-1">
            <button class="btn btn-info" id="btnTriggerStartVideo">Start</button>
          </div>
      	</div>
    </div>
  </div>
</div>



<script type="text/javascript">
$("#btnTriggerStartVideo").click(function(e){
  e.preventDefault();
  var buttons = $("#tmpUploadContainerVideo button");
  $.each(buttons,function(i,k){
      $(k).click();
  });
})
$("#btnTriggerAddVideo").click(function(e){
  e.preventDefault();
  $("#imageFileUploadVideo").find("[type=file]").click();
});
$('#imageFileUploadVideo').fileupload({
  url:"{{ route('alpha_admin_media_upload_video') }}",
  add: function (e, data) {

    var tpl = $('<div class="row">' +
            '<div class="col-md-8">'  +
              '<div class="progress">' +
               '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>'  +
              '</div>'  +
           ' </div>'  +
           '<div class="col-md-4">'  +
             ' <div class="row">'  +
               ' <div class="col-md-6"><button class="btn btn-default">Start</button></div>'  +
               ' <div class="col-md-6"><p class="desc">File Name - Size</p></div>'  +
             ' </div>'  +
           ' </div>'  +
          '</div>');

    tpl.find('.desc').text(data.files[0].name + " - " + formatFileSize(data.files[0].size));
               
    tpl.find('button').click(function (e) {
                    e.preventDefault();
                    data.submit();
                });

    data.context = tpl.appendTo($("#tmpUploadContainerVideo"))

  },
  progress: function(e, data){
        var progress = parseInt(data.loaded / data.total * 100, 10);
        if(progress >= 95){
          progress = 95;
        }
        data.context.find('.progress-bar').attr('aria-valuenow',progress).attr('style','width:' + progress + "%");
    },
  progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
    },
   done: function (e, data) {
        e.preventDefault();
        var response = JSON.parse(data._response.result);
        if(response.status == 1){
          data.context.find('.progress-bar').attr('aria-valuenow',100).attr('style','width:' + 100 + "%");
          data.context.find('button').remove();        
       }else{
          alert(response.msg);
          data.context.find('.progress-bar').attr('aria-valuenow',0).attr('style','width:' + 0 + "%");
          data.context.find('button').text('Restart');
       }

    }

});
function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }

    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }

    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}  
</script>