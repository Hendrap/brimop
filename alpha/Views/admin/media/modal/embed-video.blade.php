<div id="modalEmbedVideo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="padding:20px">
      	<h2>Embed Video</h2>
        <div class="row">
          <div class="col-md-12">
              <input type="text" name="url" class="form-control">
              <br>
          </div>
        </div>
      	<div class="row">
      		<div class="col-md-1">
      			<button class="btn btn-info" id="btnTriggerAddEmbedVideo">Save</button>
      		</div>
      	</div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#btnTriggerAddEmbedVideo").click(function(e){
    $.ajax({
      url:'<?php echo route('alpha_admin_media_embed_video') ?>',
      type:'POST',
      dataType:'json',
      data:{url:$("#modalEmbedVideo").find("[name=url]").val()},
      success:function(data){
        if(data.status == 1){
          alert('Data Saved!');
        }
      }

    });
  });
</script>