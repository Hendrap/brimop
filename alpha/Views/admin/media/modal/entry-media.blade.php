<style type="text/css">
	.selectedImage{
		    border: 10px solid #166dba;
		    border-radius: 3px;
	}
	.selectedImage img{
		border-radius: 0px!important;
	}
</style>
<div id="modalEntryMedia" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div id="modalContainerEntryMedia" class="modal-dialog modal-lg">
    <div class="modal-content" style="padding:20px">
      	
        <div class="row">
        	<div class="col-md-3"><h2>Add Media</h2></div>
        	<div class="col-md-7">
        		<input id="mediaSearchPopUpTxt" style="margin-top:20px" type="text" class="form-control" name="">
        	</div>
        	<div class="col-md-1">
        		<button id="mediaSearchPopUpButton" style="margin-top:20px" class="btn btn-primary">Search</button>
        	</div>
        	<div class="col-md-1"></div>
        </div>
        <br>
        <br>
      	<div id="ajaxEntryMedia" class="row text-center">
      			
      	</div>
      	
      	<div class="row">
      		<div class="col-md-12" id="entryMediaPagination"></div>
      	</div>
      	<br>
      	<div class="row">
      		<div class="col-md-1">
      			<button class="btn btn-info" id="btnTriggerAddImageToEntry">Add Media</button>
      		</div>
      		<div class="col-md-1"></div>
      		<div class="col-md-1">
      			<button class="btn btn-info" id="btnTriggerEntryMediaUpload">Upload Images</button>
      		</div>
      	</div>

    </div>
  </div>
</div>
<?php echo view('alpha::admin.test.uploader',['media_type'=>'image']) ?>
<script type="text/javascript">
	var entry_media_url = "<?php echo route('alpha_admin_media_entry') ?>";
</script>
<script type="text/javascript">
	var loader = '<div class="pace-demo"><div class="theme_squares"><div class="pace-progress" data-progress-text="60%" data-progress="60"></div><div class="pace_activity"></div></div></div>';
	 function parseMedia(type){
    	if(type == 'init'){
    		var selected = $("#containerMedia").find('.col-md-2');
    	}else{
	    	var selected = $("#containerMedia").find('.selectedImage');
	    }
    		
		$.each(selected,function(i,k){
			var item = $(k).parent().parent();
			$(item).find('.thumb').removeClass('selectedImage');
 			$(item).find('[name=tmpmedia]').attr('name','media[]');
			$(item).find('p').remove();
			$(item).find('span').show();
		});
    }

	$(document).on('click','#ajaxEntryMedia .thumb',function(e){
		e.preventDefault();
		if($(this).hasClass('selectedImage')){
			$(this).removeClass('selectedImage');
		}else{
			$(this).addClass('selectedImage');
		}
	});

	$(document).on('click','#entryMediaPagination a',function(e){
		e.preventDefault();
		loadMedias($(this).attr('href'),function(){

		});
	});
	$(document).on('click','#mediaSearchPopUpButton',function(e){
		e.preventDefault();
		var keyword = $("#mediaSearchPopUpTxt").val();
		loadMedias(entry_media_url + "?search=" + keyword,function(){

		});
	});
	function loadMedias(url,callback){

		$("#btnTriggerAddImageToEntry").hide();
		$("#entryMediaPagination").hide();
		$("#ajaxEntryMedia").html(loader);
		$("#modalContainerEntryMedia").attr('class','modal-dialog modal-xs');
		$("#modalContainerEntryMedia h2").hide();
		$("#modalEntryMedia").modal('show');
		$.ajax({
			url:url,
			type:'GET',
			dataType:'json',
			success:function(data){
				$("#modalContainerEntryMedia").attr('class','modal-dialog modal-lg');
				$("#ajaxEntryMedia").html(data.res);
				$("#entryMediaPagination").html(data.paginate);
				$("#btnTriggerAddImageToEntry").show();
				$("#entryMediaPagination").show();
				$("#modalContainerEntryMedia h2").show();

				callback();
			}
		});
	}

	$("#btnShowEntryMedia").click(function(e){
		e.preventDefault();
		loadMedias("<?php echo route('alpha_admin_media_entry')."?page=1" ?>",function(){
			$("#modalEntryMedia").modal('show');	
			editorName = '';
		});
	});

	$("#btnTriggerAddImageToEntry").click(function(e){
		e.preventDefault();
		if(editorName == ''){
			

			var selected = $("#modalEntryMedia").find('.selectedImage');
			$.each(selected,function(i,k){
				var item  = $(k).parent().parent().html();
				item = '<div class="col-md-2">' + item + '</div>';
				$("#containerMedia").prepend(item);
			});

			parseMedia('add');

			$("#containerMedia").sortable({
	     		 revert: true
	    	});			
		}else{
			var selected = $("#modalEntryMedia").find('.selectedImage');
			$.each(selected,function(i,k){
				var imgSrc = $(k).find('.link').attr('href');
				var altText = $(k).find('p').text();
				var editor = CKEDITOR.instances[editorName];
				editor.insertHtml('<img  alt="' +altText+ '" data-cke-saved-src="' +imgSrc+ '" src="' +imgSrc+ '" '+
						'style="border:1px solid rgb(204, 204, 204);margin-left:15px; padding:5px" class="right" />');
			});
		}
		$("#modalEntryMedia").modal('hide');	

	});

	$(document).on('click','.delete',function(e){
		e.preventDefault();
		if(confirm("Are you sure ?")){
			$(this).parent().parent().parent().parent().parent().remove();			
		}
	});
	$(document).on('click','.link',function(e){
		e.preventDefault();
		prompt("",$(this).attr('href'));
	});
	$(document).ready(function(){
		parseMedia('init');
	
	})
	$("#btnTriggerEntryMediaUpload").click(function(e){
		e.preventDefault();
		$("#modalEntryMedia").modal('hide');
		$("#modalUploadimage").modal('show');

	});
</script>