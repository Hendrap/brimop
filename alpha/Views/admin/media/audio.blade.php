<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-2">
						<button id="btnUploadAudio" class="btn btn-default">Add Audio </button><br><br>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Title</th>
						<td></td>
					</tr>

					@foreach($audios as $audio)
					<tr>
						<td>{{ $audio->title }}</td>
						<td><a target="_blank" href="{{ asset($audio->path) }}">Open</a></td>
						<td><button  data-id="{{ $audio->id }}" class="confirm btn btn-danger">Delete</button></td>
					</tr>
					@endforeach
				</table>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-6">
				{!! $audios->render() !!}
			</div>
		</div>
	</section>
</div>
<form style="display:none" id="form_delete" action="{{route('alpha_admin_media_delete',[0])}}" method="POST">
	{{csrf_field()}}
	<input type="hidden" name="id" value="0" >
</form>

<script type="text/javascript">
	$(document).ready(function(){

		$("#btnUploadAudio").click(function(e){
			e.preventDefault();
			$("#modalUploadAudio").modal('show');
		});


		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				$("#form_delete").attr('action',$("#form_delete").attr('action').replace(0,$(this).data('id')));
				$("#form_delete").find("[name=id]").val($(this).data('id'));
				$("#form_delete").submit();
			}
		});

	});
	
</script>

<?php echo view('alpha::admin.media.modal.upload-audio') ?>