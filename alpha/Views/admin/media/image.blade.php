<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-2">
						<button id="btnUploadImage" class="btn btn-default">Add Images </button><br><br>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">	
					@foreach($medias as $media)
						<div class="col-md-2">
							<img src="{{ asset(getCropImage($media->path,'force')) }}" alt="{{ $media->title }}" class="img-rounded clickable">
							<br>
							<a href="{{ asset($media->path) }}" target="_blank">Open</a>
							<a data-id="{{ $media->id }}" href="#" class="delete">Delete</a><br><br>
						</div>
					@endforeach
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-6">
				{!! $medias->render() !!}
			</div>
		</div>
	</section>
</div>
<form id="formDelete" method="POST" class="" style="display:none" action="{{ route('alpha_admin_media_delete') }}">
	<input type="hidden" name="id" value="0">
	{{ csrf_field() }}
</form>
<script type="text/javascript">
	$("#btnUploadImage").click(function(e){
		e.preventDefault();
		$("#modalUploadImage").modal('show');
	});

	$(".delete").click(function(e){
		e.preventDefault();
		if(confirm("Are You sure ?")){
			$("#formDelete").find('[name=id]').val($(this).data('id'));
			$("#formDelete").submit();			
		}
	});
</script>
<?php echo view('alpha::admin.media.modal.upload-image') ?>