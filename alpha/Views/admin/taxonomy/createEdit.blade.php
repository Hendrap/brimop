<?php 
	$id = 0;
	$name = old('name');
	$slug = old('slug');
	$desc = old('desc');
	$parent = old('parent');
	if(!empty($taxonomy)){
		$id = $taxonomy->id;
		$name = old('name',$taxonomy->name);
		$slug = old('slug',$taxonomy->taxonomy_slug);
		$desc = old('desc',$taxonomy->description);
		$parent = old('parent',$taxonomy->parent);
	}

 ?>
<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title"><?php echo ($id == 0) ? 'Add a new' : 'Edit' ?> {{ ucfirst($type) }}</h5>
					</div>
	<div class="panel-body">
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<form class="form-horizontal" action="{{route('alpha_admin_taxonomy_save',[$type,$id])}}" method="POST">
				<fieldset class="content-group">
				<h2>Main Info</h2>
				
				<div class="form-group">
					<label class="control-label col-lg-2">Name</label>
					<div class="col-lg-10">
						<input name="name" type="text" class="form-control" value="{{$name}}">
						<br>
						<p>Permalink <?php echo url('category/'.$type) ?>/<input style="border: none;background-color:#ebebeb!important" name="slug" type="text" class="input-small slug" value="{{$slug}}"></p>

					</div>
				</div>
				<!-- <p>Slug</p>
					<input name="slug" disabled="" type="text" class="slug form-control" value="{{$slug}}"> -->
				<div class="form-group">
					<label class="control-label col-lg-2">Description</label>
					<div class="col-lg-10">
						<textarea name="desc" class="form-control">{{$desc}}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Parent</label>
					<div class="col-lg-10">
						<select class="form-control" name="parent">
							<option value="0">None</option>
						@foreach($taxonomies as $taxo)
							<option <?php echo ($taxo->id == $parent) ? 'selected' : '' ?> value="{{ $taxo->id }}">{{ $taxo->name }}</option>
						@endforeach
					</select>
					</div>
				</div>
					
					
				{{csrf_field()}}
	
					<input type="hidden" name="id" value="{{@$id}}">
					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
					</div>
					</fieldset>
				</form>
			</div>
		</div>
<script type="text/javascript" src="{{ asset('vendor/js/jquery.slug.js') }}"></script>
<script src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('[name=name]').slug({
			slug: 'slug',
			hide:false
		});
		CKEDITOR.replace("desc");
	});
	 CKEDITOR.on("instanceReady", function(event){
		$(".cke_button__image_icon").hide();
		$(".cke_button__image_icon").parent().hide();
	});

</script>
