<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">{{ ucfirst($type) }}</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a href="{{ route('alpha_admin_taxonomy_create',[$type]) }}"><i class="icon-plus3"></i></a></li>
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<!-- <div class="panel-heading">
						<hr>
						<div class="row">
						
							<div class="col-md-4">
								<b>100 Users</b> -- Last updated by Raizel on May 1 2015 H:i:s
							</div>
							<div class="col-md-3">
								<input placeholder="Email or Username" type="text" class="form-control">
							
							</div>
							<div class="col-md-1">
								<button class="btn btn-info">Search</button>
							</div>
							<div class="col-md-2">
								
								<select class="form-control">
									<option value="1">Sort</option>
								</select>
							</div>	
							
						</div>
						<hr>
					</div> -->
				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Name</th>
						<th>Desc</th>
						<th>Count</th>
						<th></th>
					</tr>

					@foreach($taxonomies as $taxo)
					<tr>
						<td>{{$taxo->name}}</td>
						<td>{{ strip_tags($taxo->description) }}</td>
						<td>{{(int)@$taxo->count}}</td>



						<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="{{route('alpha_admin_taxonomy_edit',[$type,$taxo->id])}}"><i class="icon-pencil"></i> Edit</a></li>
											<li><a class="confirm" data-id="{{$taxo->id}}"  href="{{route('alpha_admin_taxonomy_delete',[$taxo->id])}}"><i class=" icon-x"></i> Delete</a></li>
											
										</ul>
								</div>
							</td>				



						
					</tr>
					@endforeach
				</table>
			</div>
			
		
		
<div class="row">
			<div class="col-md-6">
				{!! $taxonomies->render() !!}
			</div>
		</div>
<form style="display:none" id="form_delete" action="{{route('alpha_admin_taxonomy_delete',[0])}}" method="POST">
	{{csrf_field()}}
	<input type="hidden" name="id" value="0" >
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				$("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
				$("#form_delete").find("[name=id]").val($(this).data('id'));
				$("#form_delete").submit();
			}
		});

	});
	
</script>