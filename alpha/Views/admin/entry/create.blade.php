<style type="text/css">
	.taxo{
    max-height: 350px;
    min-height: 350px;
    overflow-y: scroll;
	}
</style>
<script src="{{ asset('backend/plugins/jQueryUI/jquery-ui.js') }}"></script>
<form class="form-horizontal" action="{{ route('alpha_admin_entry_save',[$type,0]) }}" method="POST">
<div class="panel panel-flat">

	<div class="panel-heading">
				<div class="row">
					<div class="col-md-8"><h1 class="panel-title">Add a new {{ $config['single'] }}</h1></div>
					<div class="col-md-2">
						<?php $status = ['published','draft','disabled'] ?>
					<select class="form-control" name="status">
						@foreach($status as $s)
							<option <?php echo ($s == old('status')) ? 'selected' : '' ?> value="{{ $s }}">{{ ucfirst($s) }}</option>
						@endforeach
					</select>
					</div>
					<div class="col-md-2">
						<select id="changeLang" class="form-control">
							@foreach(Config::get('alpha.application.locales') as $key  => $lang)
								<option value="{{ $key }}">{{ $lang }}</option>
							@endforeach
						</select>
					</div>
				</div>

					</div>
	<div class="panel-body">
	<h2>Basic</h2>
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				
				<div class="form-group">
					<label class="control-label col-lg-2">Title</label>
					<div class="col-lg-10">
					@foreach(Config::get('alpha.application.locales') as $key  => $lang)
						<input style="display:none" id="title_{{$key}}" name="title[{{$key}}]" type="text" class="form-control" value="{{old('title.'.$key)}}">
					@endforeach
					<br>
						<p>Permalink <?php echo url($config['slug']) ?>/<input style="border: none;background-color:#ebebeb!important" name="slug" type="text" class="input-small slug" value=""></p>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Description</label>
					<div class="col-lg-10">
						@foreach(Config::get('alpha.application.locales') as $key  => $lang)
						<textarea style="display:none" id="content_{{$key}}" name="content[{{$key}}]" rows="10" cols="80">{{old('content.'.$key)}}</textarea>
						@endforeach
					</div>
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Excerpt</label>
					<div class="col-lg-10">
						@foreach(Config::get('alpha.application.locales') as $key  => $lang)
					<textarea style="display:none" id="excerpt_{{$key}}" name="excerpt[{{$key}}]" rows="10" cols="80">{{old('excerpt.'.$key)}}</textarea>
					@endforeach
					</div>
				</div>				
				
					
				<div class="form-group">
					<label class="control-label col-lg-2">Parent</label>
					<div class="col-lg-10">
						<select class="form-control" name="parent">
							<option selected="" value="0">None</option>
						@foreach($parents as $parent)
							<option <?php echo ($parent->id == old('parent')) ? 'selected' : '' ?> value="{{ $parent->id }}">{{ parseMultiLang($parent->title) }}</option>
						@endforeach
					</select>
					</div>
				</div>			

				
					
				<div class="form-group">
					<label class="control-label col-lg-2">Published at</label>
					<div class="col-lg-10">
						<input id="date_published_at" name="published_at" type="text" class="date form-control" value="{{ date('Y-m-d H:i:s',strtotime(old('published_at',date('Y-m-d H:i:s')))) }}">	
					</div>
				</div>					
				{{csrf_field()}}


		</div>
		<div class="panel-body">
			<h2>SEO</h2>
			<div class="form-group">
					<label class="control-label col-lg-2">Title</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" name="seo[title]" value="{{ old('seo.title') }}">
					</div>
			</div>
			<div class="form-group">
					<label class="control-label col-lg-2">Description</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" name="seo[desc]" value="{{ old('seo.desc') }}">
					</div>
			</div>	
			<div class="form-group">
					<label class="control-label col-lg-2">Keywords</label>
					<div class="col-lg-10">
						<input type="text" class="form-control" name="seo[keys]" value="{{ old('seo.keys') }}">
					</div>
			</div>
		</div>
		
		
		
		<input type="hidden" name="id" value="{{@$id}}">
					<div class="panel-body text-right">
						<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
					</div>
</div>
				@if(!empty($metas))
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h2>Additional Info</h2>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse" class=""></a></li>
				                		<!-- <li><a data-action="close"></a></li> -->
				                	</ul>
			                	</div>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							
								<div class="panel-body">
									
									@foreach($metas as $meta)
											<div class="form-group">
												<label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
												<div class="col-lg-10">
													<?php echo view('alpha::admin.meta.'.$meta->meta_data_type,[
												 	'meta_key' => $meta->meta_key,
												 	'value' => old('metas.'.$meta->meta_key)
													 ]) ?>
												</div>
											</div>

										@endforeach
								</div>
						</div>
					</div>
				</div>
				@endif


				@if(!empty($taxo))
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h2>Taxonomy</h2>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse" class=""></a></li>
				                		<!-- <li><a data-action="close"></a></li> -->
				                	</ul>
			                	</div>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="panel-body">
								
									<div class="row">
										@if(!empty($taxo))
										@foreach($taxo as $val)
										
										<div class="col-md-4">
										<div class="panel-heading">
											<b>{{$val['single']}}</b>
										</div>
										<div class="panel panel-body taxo">
											<div class="form-group">
											
											 @foreach($taxonomies as $item)
											 	@if($item->taxonomy_type == $val['slug'])
							                      <div class="checkbox">
							                        <label>
							                          <input <?php echo in_array($item->id, old('taxonomies',[])) ? 'checked' : '' ?> name="taxonomies[]" value="{{ $item->id }}" type="checkbox">
							                          {{ $item->name }}
							                        </label>
							                      </div>
							                     @endif
						                      @endforeach
					                   		 </div>
										</div>
										</div>
										@endforeach
										@endif
									</div>
							</div>
								
						</div>
					</div>
				</div>
				@endif


				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h2>Media</h2>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a href="#" id="btnShowEntryMedia"><i class="icon-plus3"></i></a></li>
				                		<li><a data-action="collapse" class=""></a></li>
				                	</ul>
			                	</div>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="panel-body">
								
									<div class="row" id="containerMedia">
											
									</div>
							</div>
								
						</div>
					</div>
				</div>



		</form>

<script src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/js/jquery.slug.js') }}"></script>
<script type="text/javascript">

	$( "#containerMedia" ).sortable({
      revert: true
    });
	function hideAllTitle(){
		@foreach(Config::get('alpha.application.locales') as $key  => $lang)		
			$("#title_" + "{{ $key }}").hide();
		@endforeach
	}
	function hideAllEditors(){
		@foreach(Config::get('alpha.application.locales') as $key  => $lang)		
			$("#cke_content_" + "{{ $key }}").hide();
			$("#cke_excerpt_" + "{{ $key }}").hide();
		@endforeach
	}
	function showDefaultEditor(){
		$("#cke_content_" + defaultLang).show();
		$("#cke_excerpt_" + defaultLang).show();
	}
	var defaultTitle = "{{ Config::get('alpha.application.default_locale') }}";
	var defaultLang = "{{ Config::get('alpha.application.default_locale') }}";
	@foreach(Config::get('alpha.application.locales') as $key  => $lang)
			CKEDITOR.replace("content_" + "{{ $key }}");
			CKEDITOR.replace("excerpt_" + "{{ $key }}");		
	@endforeach
	CKEDITOR.on("instanceReady", function(event)
	{
		hideAllEditors();
		showDefaultEditor();
		$(".cke_button__image_icon").hide();
		$(".cke_button__image_icon").parent().hide();
	});

	$("#changeLang").change(function(e){
		hideAllEditors();
		hideAllTitle();
		defaultLang = $("#changeLang option:selected").val();
		$("#title_" + defaultLang).show();
		$("[name=slug]").show();
		showDefaultEditor();

	});

	
	$(document).ready(function(){
		hideAllTitle();
		$("#title_" + defaultTitle).show();
		$("#title_" + defaultTitle).slug({
			slug: 'slug',
			hide:false
		});

		$("#date_published_at").AnyTime_picker({
		  format: "%Y-%m-%d %H:%i:%s",
		});
	    
	});

</script>