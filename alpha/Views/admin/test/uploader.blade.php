<script src="{{ asset('backend/fileupload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('backend/fileupload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-audio.js') }}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-video.js') }}"></script>
<!-- The File Upload validation plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-validate.js') }}"></script>
<div id="modalUpload{{$media_type}}" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="padding:20px">
        <h2>Upload {{str_plural(ucfirst($media_type))}}</h2>
        <form style="display:none" name="{{$media_type}}FileUpload" id="{{$media_type}}FileUpload">
          <input type="file" multiple name="{{$media_type}}[]">
        </form>
        <div id="tmpUploadContainer{{$media_type}}" class="row">
          
        </div>
        <div class="row">
          <div class="col-md-1">
            <button class="btn btn-info" id="btnTriggerAdd{{$media_type}}">Add {{str_plural(ucfirst($media_type))}}</button>
          </div>
          <div class="col-md-1"></div>
            <div class="col-md-1">
              <button class="btn btn-info" id="btnTriggerStartAll{{$media_type}}">Start All</button>
            </div>
             <div class="col-md-1"></div>
            <div class="col-md-1">
              <button class="btn btn-info" id="btnTriggerClearAll{{$media_type}}">Clear All</button>
            </div>
        </div>
    </div>
  </div>
</div>



<script type="text/javascript">
var errors = 0;
var totalReload = 0;
$( document ).ajaxStop(function( event,request, settings ) {
  if(totalReload > 100){
    alert("Session Expired! Please Reload This Page!");
    return false;
  }
  if(errors > 0 && totalReload <= 100){
    $.get('{{route('alpha_admin_refresh_token')}}').done(function(res){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': res
                }
          });
          totalReload++;
          errors = 0;
          $("#btnTriggerStartAll{{$media_type}}").click(); 
       });
  }
});
var jqXHR = {};

$("#btnTriggerClearAll{{$media_type}}").click(function(e){
    $("#btnTriggerStartAll{{$media_type}}").hide(); 
    $("#btnTriggerClearAll{{$media_type}}").hide(); 
    $("#tmpUploadContainer{{$media_type}}").html('');
});
$("#btnTriggerStartAll{{$media_type}}").hide(); 
$("#btnTriggerClearAll{{$media_type}}").hide(); 
$("#btnTriggerStartAll{{$media_type}}").click(function(e){
  e.preventDefault();
  var buttons = $("#tmpUploadContainer{{$media_type}} button");
   
          $.each(buttons,function(i,k){
              $(k).click();
           });
});
$("#btnTriggerAdd{{$media_type}}").click(function(e){
  e.preventDefault();
  $("#{{$media_type}}FileUpload").find("[type=file]").click();
});
$('#{{$media_type}}FileUpload').fileupload({
  url:"{{ route('alpha_admin_media_upload_'.$media_type) }}",
  add: function (e, data) {
    var total = $("#tmpUploadContainer{{$media_type}} button").length;
    if(total > 10){
      alert("Maximum files per upload is 10");
      return false;
    }

    var tpl = $('<div class="row">' +
            '<div class="col-md-11">'  +
            '<p class="desc">2c926559ed5b9c6ab28327ee4552d970.png - 90.44 KB</p>' +
              '<div class="progress content-group-sm">' +
               '<div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">' +
               '</div>'  +
              '</div>'  +
           ' </div>'  +
           '<div class="col-md-1"><br><button style="display:none" class="btn btn-default">Start</button></div>' + 
          '</div>');

    tpl.find('.desc').text(data.files[0].name + " - " + formatFileSize(data.files[0].size));
               
    tpl.find('button').click(function (e) {
                    e.preventDefault();
                   
                   data.submit();
                    
                    
                });

    data.context = tpl.appendTo($("#tmpUploadContainer{{$media_type}}"))
    $("#btnTriggerStartAll{{$media_type}}").show();
    $("#btnTriggerClearAll{{$media_type}}").show(); 
  },
  progress: function(e, data){
        var progress = parseInt(data.loaded / data.total * 100, 10);
        if(progress >= 95){
          progress = 95;
        }
        data.context.find('.progress-bar').attr('aria-valuenow',progress).attr('style','width:' + progress + "%");
    },
  progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
    },
  done: function (e, data) {
        e.preventDefault();
        var response = JSON.parse(data._response.result);
        if(response.status == 1){
          data.context.find('.progress-bar').attr('aria-valuenow',100).attr('style','width:' + 100 + "%");
          data.context.find('button').remove()
          data.context.find('.desc').html(data.context.find('.desc').html() + '  <span style="margin-top: 8px;" class="label label-success">' + 'Success' + '</span>');

       }else{
          data.context.find('.progress-bar').attr('aria-valuenow',0).attr('style','width:' + 0 + "%");
          data.context.find('button').remove()
          data.context.find('.desc').html(data.context.find('.desc').html() + '  <span style="margin-top: 8px;" class="label label-danger">' + response.msg + '</span>');
          //.replaceWith('<span style="margin-top: 8px;" class="label label-danger">' + response.msg + '</span>');      
       }

    },
  error:function(e,data){
      errors++;
  },

});
function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }

    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }

    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}  
</script>
