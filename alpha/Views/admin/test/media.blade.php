<script type="text/javascript" src="{{ asset('backend/assets/js/fancybox.min.js') }}"></script>
<div class="panel panel-white">
					<div class="panel-heading">
						<div class="row">
							<div class="col-md-2"><h2>Media Library - {{ str_plural(ucfirst($media_type)) }}</h2></div>
							<div class="text-right col-md-6">
							<h2>
								<button id="btnUpload{{$media_type}}" class="btn btn-primary">Add {{str_plural(ucfirst($media_type))}}</button>

								<?php if($media_type == 'video'){ ?>
									<button id="btnEmbedVideo" class="btn btn-primary">Add Embed Video</button>
								<?php } ?>
							</h2>

							</div>
							<div class="col-md-2"><input value="{{ @$search }}" id="txtMediaSearch" style="margin-top: 20px;" class="form-control" type="text" name=""></div>
							<div class="col-md-2"><button style="margin-top: 20px;" id="btnSearch" class="btn btn-primary">Search</button></div>
							
						</div>
						@if(session('msg'))
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-success">{{session('msg')}}</div>
							</div>
						</div>
						@endif
					</div>
					
					<table class="table table-striped media-library table-lg">
                        <thead>
                            <tr>
                       
                                <th>Preview</th>
                                <th>Name</th>
                                <th>Author</th>
                                <th>Date</th>
                                <th>File info</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($medias as $key => $value) { 
                        	$info = unserialize($value->media_meta);
                        	?>
                        	 <tr>
                            	
		                        <td>
		                        	<?php if((!empty($value->path) && $media_type == 'image') || (!empty($value->url) && !empty($value->path))){ ?>
				                        <a href="{{ asset($value->path) }}" data-popup="lightbox">
					                        <img src="{{ asset(getCropImage($value->path,'default')) }}" alt="" class="img-rounded img-preview">
				                        </a>
			                        <?php }else{ ?>
				                       <a href="{{ asset('backend/assets/images/placeholder.jpg') }}" data-popup="lightbox">
					                        <img src="{{ asset('backend/assets/images/placeholder.jpg') }}" alt="" class="img-rounded img-preview">
				                        </a>
			                        <?php } ?>
		                        </td>
		                        <?php if($media_type == 'video' && !empty($value->url)){ ?>
			                        <td><a href="{{ $value->url }}" target="_blank">{{ $value->title }}</a></td>
		                        <?php }else{ ?>
			                        <td><a href="{{ asset($value->path) }}" target="_blank">{{ $value->title }}</a></td>
		                        <?php } ?>

		                        <td><a href="#">{{ $value->user->username }}</a></td>
		                        <td>{{ date('d F Y',strtotime($value->created_at)) }}</td>
		                        <td>
		                        	<ul class="list-condensed list-unstyled no-margin">					   	
		                        		<?php if(empty($value->url)){ ?>                     		
			                        	<li><span class="text-semibold">Size:</span> {{ formatBytes((int)@$info['size']) }}</li>
			                        	<li><span class="text-semibold">Format:</span> .{{ @$info['extension'] }}</li>
		                        		<?php }else{ ?>
		                        			<li><span class="text-semibold">Embed Video</li>
		                        		<?php } ?>
		                        	</ul>
		                        </td>
		                        <td class="text-center">
		                            <div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">
				                    		<?php if($media_type == 'video' && !empty($value->url)){ ?>
						                        <li><a class="pop-copy" href="{{ asset($value->url) }}"><i class=" icon-menu-open"></i> Copy URL</a></li>
					                        <?php }else{ ?>
						                        <li><a class="pop-copy" href="{{ asset($value->path) }}"><i class=" icon-menu-open"></i> Copy URL</a></li>
					                        <?php } ?>
											<li><a class="delete_media" data-id="{{ $value->id }}"  href="#"><i class=" icon-x"></i> Delete</a></li>
											
										</ul>
								</div>
		                        </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                      </table> 
</div>
<div class="row">
			<div class="col-md-6">
				<?php
					 if(!empty($_GET)){
						foreach ($_GET as $key => $value) {
							$medias = $medias->appends([$key => $value]);
						}
					 }
				?>
				{!! $medias->render() !!}
			</div>
		</div>
		<?php 
			echo view('alpha::admin.test.uploader',['media_type'=>$media_type]);		
		 ?>
		<?php 
			if($media_type == 'video'){
				echo view('alpha::admin.media.modal.embed-video');
			} 
		?>
<form id="formDelete" method="POST" class="" style="display:none" action="{{ route('alpha_admin_media_delete') }}">
	<input type="hidden" name="id" value="0">
	{{ csrf_field() }}
</form>
 <script type="text/javascript">
     $("#btnEmbedVideo").click(function(e){
     	$("#modalEmbedVideo").modal('show');
     });

	 $("#btnUpload{{$media_type}}").click(function(e){
		e.preventDefault();
		$("#modalUpload{{$media_type}}").modal('show');
	});
	$('[data-popup="lightbox"]').fancybox({
		padding: 3
	});
	 $(".pop-copy").click(function(e){
	 	e.preventDefault();
	 	prompt('Copy to clipboard!',$(this).attr('href'));
	 })
	 $(".delete_media").click(function(e){
		e.preventDefault();
		if(confirm("Are You sure ?")){
			$("#formDelete").find('[name=id]').val($(this).data('id'));
			$("#formDelete").submit();			
		}
	});
	 $("#btnSearch").click(function(e){
	 	e.preventDefault();
	 	window.location.href = window.location.origin + window.location.pathname + "?search=" + $("#txtMediaSearch").val();
	 });
</script>