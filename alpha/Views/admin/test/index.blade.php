<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{{ asset('backend/fileupload/js/vendor/jquery.ui.widget.js') }}"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<!-- <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
 --><!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('backend/fileupload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-audio.js') }}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-video.js') }}"></script>
<!-- The File Upload validation plugin -->
<script src="{{ asset('backend/fileupload/js/jquery.fileupload-validate.js') }}"></script>
<!-- The main application script -->
<!-- <script src="{{ asset('backend/fileupload/js/main.js') }}"></script>
 --><form id="upload" method="post" action="{{ route('alpha_admin_media_upload_video') }}" enctype="multipart/form-data">
  <input type="file" name="video[]" multiple />
  <ul id="fileList">

  </ul>
  <input type="submit" name="submit" id="btnSubmit">
</form>
<script type="text/javascript">
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
$("#btnSubmit").click(function(e){
	e.preventDefault();
	var list = $("#fileList li button");
	$.each(list,function(i,k){
		$(k).click();
	});
});
var upload = {};
$('#upload').fileupload({
//autoUpload: true,
  url:"{{ route('alpha_admin_media_upload_video') }}",
  add: function (e, data) {

    var tpl = $('<li class="working">'+
                '<input type="text" value="0" data-width="48" data-height="48" data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" />'+
                '<p></p><button>upload</button><span></span></li>' );

    tpl.find('p').text(data.files[0].name)
                 .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

    tpl.find('button').click(function (e) {
                	e.preventDefault();
                    data.submit();
                });

    data.context = tpl.appendTo($("#fileList"))

  },
  progress: function(e, data){
        var progress = parseInt(data.loaded / data.total * 100, 10);
        if(progress >= 95){
        	progress = 95;
        }

        data.context.find('[type=text]').val(progress).change();
    },
  progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        console.log(progress);
    },
   done: function (e, data) {
        e.preventDefault();
   		var response = JSON.parse(data._response.result);
        data.context.find('[type=text]').val(100).change();
        data.context.find('button').remove();
    }

});
function formatFileSize(bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }

    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }

    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}
</script>
