<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">Accounts</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a href="{{ route('admin.user.create') }}"><i class="icon-plus3"></i></a></li>
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="panel-heading">
						<hr>
						<div class="row">
							<div class="col-md-4">
								<b>{{$total}} Users</b> -- Last updated by {{$theLast}} on {{$theLastTime}}
							</div>
							<form>
							<div class="col-md-3">
								<input name="q" placeholder="Type Email or Username" type="text" class="form-control">
							</div>
							<div class="col-md-2">
								
								<select name="sort" class="form-control">
									<option value="0">Sort</option>
									<option value="asc">Ascending</option>
									<option value="desc">Descending</option>
									<option value="oldest">Oldest</option>
									<option value="latest">Latest</option>
								</select>
							</div>	
							<div class="col-md-2">
								
								<select name="role" class="form-control">
									<option value="0">Select Role</option>
									@foreach($roles as $role)
										<option value="{{$role->id}}">{{$role->name}}</option>
									@endforeach
								</select>
							
							</div>
							<div class="col-md-1">
								<button type="submit" class="btn btn-info">Apply Filter</button>
							</div>
							</form>
						</div>
						<hr>
					</div>
				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Role</th>
						<th>Status</th>
						<th>Created at</th>
						<th>Last Login</th>
						<th></th>
					</tr>

					@foreach($users as $user)
					<tr>
						<td>{{$user->username}}</td>
						<td>{{$user->email}}</td>
						<td>{{@$user->roles[0]->name}}</td>
						<td>
						<?php 
						$className ='default';
						switch ($user->status) {
							case 'active':
								$className = 'success';
								break;
							case 'disabled':
								$className = 'danger';
								break;	
							
							
						}
						 ?>
						<span class="label label-{{ $className }}">{{ ucfirst($user->status) }}</span>

						</td>

						<td>{{ date('d F Y',strtotime($user->created_at)) }}</td>
						<td>{{ date('d F Y',strtotime($user->last_login)) }}</td>

						<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

	<ul class="dropdown-menu dropdown-menu-right">
		<li>
			<a href="{{ route('admin.user.edit',[$user->id]) }}">
			<i class="icon-pencil"></i> Edit</a>
		</li>
		<?php if($user->status == 'active'){ ?>
		<li>
			<a href="{{ route('alpha_admin_user_change_status',[$user->id,'disabled']) }}">
			<i class=" icon-cancel-square"></i> Disable</a>
		</li>
		<?php }else{ ?>
		<li>
			<a href="{{ route('alpha_admin_user_change_status',[$user->id,'active']) }}">
			<i class=" icon-checkmark4"></i> Enable</a>
		</li>
		<?php } ?>
		<li>
			<a class="confirm" data-id="{{$user->id}}"  href="{{ route('admin.user.destroy',[$user->id]) }}?_method=delete">
			<i class=" icon-x"></i> Delete</a>
		</li>
											
	</ul>
								</div>
						</td>						
					</tr>
					@endforeach
				</table>
		
</div>
<div class="row">
			<div class="col-md-6">
				<?php
					 if(!empty($_GET)){
						foreach ($_GET as $key => $value) {
							$users = $users->appends([$key => $value]);
						}
					 }
				?>
				{!! $users->render() !!}
			</div>
		</div>
<form style="display:none" id="form_delete" action="{{route('admin.user.destroy',[0])}}" method="POST">
	{{csrf_field()}}
	<input type="hidden" name="id" value="0" >
	{{ method_field('DELETE') }}
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				$("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
				$("#form_delete").find("[name=id]").val($(this).data('id'));
				$("#form_delete").submit();
			}
		});

	});
	
</script>