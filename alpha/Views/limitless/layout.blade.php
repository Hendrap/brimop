<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ $title or 'Alpha CMS' }}</title>

	<!-- Global stylesheets -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
	<link href="{{ asset('backend/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backend/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backend/assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backend/assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backend/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backend/assets/css/custom.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('backend/assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('backend/assets/js/core/libraries/bootstrap.min.js')}}"></script>
	<!-- /core JS files -->

	<script type="text/javascript" src="{{ asset('backend/assets/js/core/app.js')}}"></script>
	<!-- /theme JS files -->
	<script type="text/javascript" src="{{ asset('backend/assets/js/anytime.min.js')}}"></script>

	  <script type="text/javascript">
	  //ini di ppakai buat alpha media image
	  var editorName = '';
	  var mediaSite = "<?php echo route('alpha_admin_media_entry')."?editor=true&page=1" ?>";
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
    </script>

</head>

    <body class="navbar-bottom">
 


	
    	@include('alpha::limitless.partials.header')
		
		<!-- Page container -->
		<div class="page-container">

			<!-- Page content -->
			<div class="page-content">
				@include('alpha::limitless.partials.sidebar')
				<!-- Main content -->
				<div class="content-wrapper">

			        <?php 
			            if(!empty($content)){
			                echo $content;
			            }else{
			                //echo view('alpha::admin.empty');
			            }
			        ?>
			        
				</div>

			</div>


		</div>	
		@include('alpha::limitless.partials.footer')	
    </body>
    <?php 
	    if(Request::path() != 'admin/media/image'){
	    	echo view('alpha::admin.media.modal.entry-media');
	    }
    ?>

    </html>