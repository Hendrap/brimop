
	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a target="_blank" class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('backend/assets/images/v2.png')}}" alt=""></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li>
					<a class="sidebar-control sidebar-main-toggle hidden-xs">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

				
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ asset('backend/assets/images/placeholder.jpg') }}" alt="">
						<span>{{app('AdminUser')->user->username}}</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{route('alpha_admin_myaccount')}}"><i class="icon-user-plus"></i> My profile</a></li>
						<!-- <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li> -->
						<li><a href="{{route('alpha_get_logout')}}"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page header -->
	<?php $segments = Request::segments();
			if(!empty($segments[4])){
				unset($segments[4]);
			}
			unset($segments[0]);
	?>
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="{{route('alpha_admin_index')}}"><i class="icon-home2 position-left"></i> Home</a></li>
				<?php if(!empty(Request::segments()[1])){ ?>

					<?php if(Request::segments()[1] == 'setting'){ ?>
						<li><a href="#">Setting</a></li>
						<?php if(Request::segments()[2] == 'role'){ ?>
							<li><a href="{{route('admin.setting.role.index')}}">{{ucfirst(Request::segments()[2])}}</a></li>
							@if(!empty(Request::segments()[3]))
								@if(is_numeric(Request::segments()[3]))
									<li><a href="#">{{ucfirst(Request::segments()[4])}}</a></li>
								@else
									<li><a href="#">{{ucfirst(Request::segments()[3])}}</a></li>
								@endif
							@endif
						<?php }else{ ?>
							<li><a href="#">{{ucfirst(Request::segments()[2])}}</a></li>
						<?php } ?>
					<?php } ?>

					<?php if(Request::segments()[1] == 'user'){ ?>
						<li><a href="{{route('admin.user.index')}}">User</a></li>
						@if(!empty(Request::segments()[2]))
							@if(!is_numeric(Request::segments()[2]))
								<li><a href="#">{{ucfirst(Request::segments()[2])}}</a></li>
							@else
								<li><a href="#">{{ucfirst(Request::segments()[3])}}</a></li>
							@endif
						@endif
					<?php } ?>


					<?php if(Request::segments()[1] == 'media'){ ?>
						<li><a href="#">Media</a></li>
						<li><a href="#">{{ucfirst(Request::segments()[2])}}</a></li>
					<?php } ?>

					<?php if(Request::segments()[1] == 'taxonomy'){ ?>
						<li><a href="#">Taxonomy</a></li>
						<li><a href="{{route('alpha_admin_taxonomy_index',Request::segments()[2])}}">{{ucfirst(Request::segments()[2])}}</a></li>
						@if(!empty(Request::segments()[3]))
							@if(is_numeric(Request::segments()[3]))
								<li><a href="#">{{ucfirst(Request::segments()[4])}}</a></li>
							@else
								<li><a href="#">{{ucfirst(Request::segments()[3])}}</a></li>
							@endif
						@endif

					<?php } ?>


					<?php if(Request::segments()[1] == 'entry'){ ?>
						<li><a href="#">Entry</a></li>
						<li><a href="{{route('alpha_admin_entry_index',Request::segments()[2])}}">{{ucfirst(Request::segments()[2])}}</a></li>
						@if(!empty(Request::segments()[3]))
							@if(is_numeric(Request::segments()[3]))
								<li><a href="#">{{ucfirst(Request::segments()[4])}}</a></li>
							@else
								<li><a href="#">{{ucfirst(Request::segments()[3])}}</a></li>
							@endif
						@endif

					<?php } ?>




				<?php } ?>
			</ul>
		</div>
		<?php foreach ($segments as $key => $value) {
		$segments[$key] = ucfirst(str_replace('_', ' ', $value));
		} 
		
		?>
		<div class="page-header-content">
			<div class="page-title">
				<?php if(!empty($segments[1])){ ?>
				<h4><i class="icon-arrow-left52 position-left"></i>
				
				<span class="text-semibold">{{ucfirst($segments[1])}}<?php unset($segments[1]) ?></span>
				@if(!empty($segments[2]))
					- {{$segments[2]}}
				@endif
				</h4>
				<?php }else{ ?>
					<h4><i class="icon-arrow-left52 position-left"></i>
				
				<span class="text-semibold">Home</span></h4>
				<?php } ?>
			</div>
		</div>
	</div>
	<!-- /page header -->