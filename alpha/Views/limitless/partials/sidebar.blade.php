		<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-default">
				<div class="sidebar-content">

					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-title h6">
							<span>Main navigation</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content sidebar-user">
							<div class="media">
								<a href="#" class="media-left"><img src="<?php echo asset('backend/assets/images/placeholder.jpg') ?>" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold"><?php echo app("AdminUser")->user->username ?></span>
									<div class="text-size-mini text-muted">
										<?php echo app("AdminUser")->user->email ?>
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"></a>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li><a href="<?php echo url('admin') ?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>


								<li>
									<a href="#"><i class="icon-gear"></i><span>Settings</span></a>
									<ul>
										<li><a href="{{route('alpha_admin_get_setting_general')}}"><i class="icon-pencil7"></i>General</a></li>
						                <li><a href="{{route('admin.setting.role.index')}}"><i class="icon-accessibility"></i>Roles And Access</a></li>
						                <li><a href="{{route('admin.setting.entry_type.index')}}"><i class="icon-grid-alt"></i>Entry Types</a></li>
						                <li><a href="{{route('admin.setting.taxonomy_type.index')}}"><i class=" icon-tree6"></i>Taxonomy Types</a></li>
						                <li><a href="{{route('admin.setting.user_type.index')}}"><i class=" icon-user-check"></i>User Type</a></li>
						                <li><a href="{{route('admin.setting.image.index')}}"><i class=" icon-images2"></i>Image Types</a></li>
						                <li><a href="{{route('alpha_admin_get_plugins')}}"><i class="icon-cube"></i>Plugins</a></li>
						                <li><a href="{{route('alpha_admin_get_contacts')}}"><i class="icon-phone"></i>Contact</a></li>
						                <li><a href="{{route('alpha_admin_get_extras')}}"><i class="icon-package"></i>Extras</a></li>
						                <li><a href="{{route('alpha_admin_get_labels')}}"><i class="icon-text-color"></i>Labels</a></li>
										
									</ul>
								</li>


								
								<li class="<?php echo ($active == 'user') ? 'active' : ''; ?>">
									<a href="#"><i class="icon-user"></i><span>Accounts</span></a>
									<ul>
										 <li><a href="{{route('admin.user.index')}}"><i class=" icon-users4"></i>All Accounts</a></li>
               							 <li><a href="{{route('admin.user.create')}}"><i class=" icon-user-plus"></i>Create</a></li>
									</ul>
								</li>





								<li>
									<a href="#"><i class=" icon-media"></i><span>Media Library</span></a>
									<ul>
										
										 <li><a href="{{route('alpha_admin_media_image')}}"><i class=" icon-image5"></i>Image</a></li>
						                <li><a href="{{route('alpha_admin_media_audio')}}"><i class=" icon-music"></i>Audio</a></li>
						                <li><a href="{{route('alpha_admin_media_video')}}"><i class=" icon-video-camera2"></i>Video</a></li>
						                <li><a href="{{route('alpha_admin_media_other')}}"><i class=" icon-file-empty2"></i>Other</a></li>
										
									</ul>
								</li>



								<li>
									<a href="#"><i class=" icon-tree5"></i><span>Taxonomy</span></a>
									<ul>
										
										 <?php $taxonomies = app('AlphaSetting')->taxonomies; ?>
						                  @foreach($taxonomies as $value)
						                      @if($value['show_ui'] == 'yes')
						                       <li class="<?php echo ($active == 'taxonomy_'.$value['slug']) ? 'active' : ''; ?>"><a href="{{ route('alpha_admin_taxonomy_index',[$value['slug']]) }}"><i class="icon-lan2"></i>{{ucfirst($value['single'])}}</a></li>
						                       @endif
						                  @endforeach
									</ul>
								</li>


								 <?php $entries = app('AlphaSetting')->entries; ?>
	            @foreach($entries as $value)
	              @if($value['show_ui'] == 'yes')
	                <li class="treeview <?php echo ($active == 'entry_'.$value['slug']) ? 'active' : ''; ?>">
	                  <a href="#">
	                   
	                    <i class="icon-books"></i><span>{{ $value['plural'] }}</span>
	                     
	                  </a>
	                  <ul>
	                    <li><a href="{{ route('alpha_admin_entry_index',[$value['slug']]) }}"> <i class=" icon-book2"></i>View {{$value['single']}}</a></li>
	                    <li><a href="{{ route('alpha_admin_entry_create',[$value['slug']]) }}"><i class="  icon-file-plus"></i>Create {{$value['single']}}</a></li>
	                  </ul>
	                </li>
	               @endif
	            @endforeach


	            @if(!empty(app('AlphaMenu')->menus))
	            	@foreach(app('AlphaMenu')->menus as $key => $value)
	            		<li class="{{ !empty($value->childs) ? 'treeview' : '' }} <?php echo ($active == $key) ? 'active' : ''; ?>">
	            			<a href="{{ $value->link }}"><i class="{{ $value->class }}"></i><span>{{ $value->name }}</span></a>
	            			@if(!empty($value->childs))
	            				<ul>
	            					@foreach($value->childs as $k => $v)
		            					<li class="<?php echo ($active == $k) ? 'active' : ''; ?>"><a href="{{ $v->link }}"><i class="{{ $v->class }}"></i>{{ $v->name }}</a></li>
	            					@endforeach
	            				</ul>
	            			@endif
	            		</li>
	            	@endforeach
	            @endif

					

								<!-- /page kits -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->

			<script type="text/javascript">
				var list = $(".navigation-main li");
				$.each(list,function(i,k){
					if(window.location.href == $(k).find('a').attr('href')){
						$(k).addClass('active');
					}
				});
			</script>