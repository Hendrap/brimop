				<!-- Small table -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Roles And Access</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a href="{{ route('admin.setting.role.create') }}"><i class="icon-plus3"></i></a></li>
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

					@if(session('msg'))
						<div class="alert alert-success">{{session('msg')}}</div>
					@endif

					<!-- <div class="table-responsive"> -->
						<table class="table-responsive table table-sm">
							<thead>
								<tr>
									<th>#</th>
									<th>Role Name</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($roles as $key => $role)
								<tr>
									<td>{{ $key + 1 }}</td>
									<td>{{ $role->name }}</td>
									<td>
										<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="{{ route('admin.setting.role.edit',[$role->id]) }}"><i class="icon-pencil"></i> Edit</a></li>
											<li><a class="confirm" data-id="{{$role->id}}"  href="{{ route('admin.setting.role.destroy',[$role->id]) }}?_method=delete"><i class=" icon-x"></i> Delete</a></li>
											
										</ul>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					<!-- </div> -->
				</div>
				<!-- /small table -->
				<form style="display:none" id="form_delete" action="{{route('admin.setting.role.destroy',[0])}}" method="POST">
	{{csrf_field()}}
	<input type="hidden" name="id" value="0" >
	{{ method_field('DELETE') }}
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				$("#form_delete").attr('action',$("#form_delete").attr('action').replace('/' + 0,'/' + $(this).data('id')));
				$("#form_delete").find("[name=id]").val($(this).data('id'));
				$("#form_delete").submit();
			}
		});

	});
	
</script>