<script type="text/javascript" src="{{ asset('backend/assets/js/d3.min.js') }}"></script>
<div class="row">
					

					<div class="col-lg-5">

						<!-- Sales stats -->
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">Dashboard Statistics</h6>
								<!-- <div class="heading-elements">
									<form class="heading-form" action="#">
										<div class="form-group">
											<select class="change-date select-sm" id="select_date" style="display: none;">
												<optgroup label="<i class='icon-watch pull-right'></i> Time period">
													<option value="val1">June, 29 - July, 5</option>
													<option value="val2">June, 22 - June 28</option>
													<option value="val3" selected="selected">June, 15 - June, 21</option>
													<option value="val4">June, 8 - June, 14</option>
												</optgroup>
											</select><div class="btn-group"><button type="button" class="multiselect dropdown-toggle btn btn-link text-semibold" data-toggle="dropdown" title="June, 15 - June, 21"><span class="multiselect-selected-text"><span class="status-mark border-warning position-left"></span>June, 15 - June, 21</span> <b class="caret"></b></button><ul class="multiselect-container dropdown-menu pull-right"><li class="multiselect-item multiselect-group"><label><i class="icon-watch pull-right"></i> Time period</label></li><li><a tabindex="0"><label class="radio"><div class="choice"><span><input type="radio" value="val1"></span></div> June, 29 - July, 5</label></a></li><li><a tabindex="0"><label class="radio"><div class="choice"><span><input type="radio" value="val2"></span></div> June, 22 - June 28</label></a></li><li class="active"><a tabindex="0"><label class="radio"><div class="choice"><span class="checked"><input type="radio" value="val3"></span></div> June, 15 - June, 21</label></a></li><li><a tabindex="0"><label class="radio"><div class="choice"><span><input type="radio" value="val4"></span></div> June, 8 - June, 14</label></a></li></ul></div>
										</div>
									</form>
			                	</div> -->
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="container-fluid">
								<div class="row text-center">
									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class=" icon-file-text2 position-left text-slate"></i> {{ $pages }}</h5>
											<span class="text-muted text-size-small">Pages</span>
										</div>
									</div>

									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class=" icon-books position-left text-slate"></i> {{ $entryTypes }}</h5>
											<span class="text-muted text-size-small">Entry Types</span>
										</div>
									</div>

									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class=" icon-film2 position-left text-slate"></i> {{$medias}}</h5>
											<span class="text-muted text-size-small">Media Library</span>
										</div>
									</div>
								</div>
							</div>


						</div>


						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">User Accounts</h6>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="container-fluid">
								<div class="row text-center">
									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="  icon-users2 position-left text-slate"></i> {{ $user }}</h5>
											<span class="text-muted text-size-small">Users</span>
										</div>
									</div>

									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin">Last Login</h5>
											<span class="text-muted text-size-small">{{ $lastLogin }}</span>
										</div>
									</div>

									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin">Last Register</h5>
											<span class="text-muted text-size-small">{{ $lastRegister }}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						

					</div>
					<div style="display:none" class="col-lg-3">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">User By Role</h6>

							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="container-fluid">
								<div class="row text-center">
									

									<div class="col-md-6">
										<div class="content-group" id="userContainer">
											

										</div>
									</div>

								
								</div>
							</div>
						</div>
					</div>
				</div>


				   <script type="text/javascript">

    var canvasWidth = 350, //width
      canvasHeight = 350,   //height
      outerRadius = 120,   //radius
      color = d3.scale.category20(); //builtin range of colors

    var dataSet = <?php echo json_encode($userByRole) ?>;
    
    var vis = d3.select("#userContainer")
      .append("svg:svg") //create the SVG element inside the <body>
        .data([dataSet]) //associate our data with the document
        .attr("width", canvasWidth) //set the width of the canvas
        .attr("height", canvasHeight) //set the height of the canvas
        .append("svg:g") //make a group to hold our pie chart
          .attr("transform", "translate(" + 1.5*outerRadius + "," + 1.5*outerRadius + ")") // relocate center of pie to 'outerRadius,outerRadius'

    // This will create <path> elements for us using arc data...
    var arc = d3.svg.arc()
      .outerRadius(outerRadius);

    var pie = d3.layout.pie() //this will create arc data for us given a list of values
      .value(function(d) { return d.magnitude; }) // Binding each value to the pie
      .sort( function(d) { return null; } );

    // Select all <g> elements with class slice (there aren't any yet)
    var arcs = vis.selectAll("g.slice")
      // Associate the generated pie data (an array of arcs, each having startAngle,
      // endAngle and value properties) 
      .data(pie)
      // This will create <g> elements for every "extra" data element that should be associated
      // with a selection. The result is creating a <g> for every object in the data array
      .enter()
      // Create a group to hold each slice (we will have a <path> and a <text>
      // element associated with each slice)
      .append("svg:g")
      .attr("class", "slice");    //allow us to style things in the slices (like text)

    arcs.append("svg:path")
      //set the color for each slice to be chosen from the color function defined above
      .attr("fill", function(d, i) { return color(i); } )
      //this creates the actual SVG path using the associated data (pie) with the arc drawing function
      .attr("d", arc);

    // Add a legendLabel to each arc slice...
    arcs.append("svg:text")
      .attr("transform", function(d) { //set the label's origin to the center of the arc
        //we have to make sure to set these before calling arc.centroid
        d.outerRadius = outerRadius + 50; // Set Outer Coordinate
        d.innerRadius = outerRadius + 45; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")";
      })
      .attr("text-anchor", "middle") //center the text on it's origin
      .style("fill", "Purple")
      .style("font", "bold 12px Arial")
      .text(function(d, i) { return dataSet[i].legendLabel; }); //get the label from our original data array

    // Add a magnitude value to the larger arcs, translated to the arc centroid and rotated.
    arcs.filter(function(d) { return d.endAngle - d.startAngle > .2; }).append("svg:text")
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      //.attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")"; })
      .attr("transform", function(d) { //set the label's origin to the center of the arc
        //we have to make sure to set these before calling arc.centroid
        d.outerRadius = outerRadius; // Set Outer Coordinate
        d.innerRadius = outerRadius/2; // Set Inner Coordinate
        return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")";
      })
      .style("fill", "White")
      .style("font", "bold 12px Arial")
      .text(function(d) { return d.data.magnitude; });

    // Computes the angle of an arc, converting from radians to degrees.
    function angle(d) {
      var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
      return a > 90 ? a - 180 : a;
    }

        
    </script>