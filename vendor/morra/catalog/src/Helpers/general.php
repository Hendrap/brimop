<?php 

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
    }
    return $token;
}

function getOrderNumber($length = 6){
	return getToken($length);
}

function defaultOrderVars(){
	$default =  ['subtotal','tax','tracking_number','user_id','total_discount','shipping_amount',
				'grand_total','customer_note','status','shipping_method','carrier_id',
				'carrier_id','carrier_name','carrier_level','note','shipping_date',
				'confirmation','confirmation_date','modified_by',
                'dropshipper_name','dropshipper_phone','data'
                ];

    $vars = array('address','method','email','firstname','lastname','company','address_1','address_2','city','postcode','country','county','state','district','province','phone','mobile_phone');
    $payment = [];
    $shipping = [];
    foreach ($vars as $key => $value) {
    	$payment[] = 'payment_'.$value;
    	$shipping[] = 'shipping_'.$value;
    }
    $default = array_merge($default,$payment);
    $default = array_merge($default,$shipping);

    return $default;
}

function defaulOrderDetailsVars(){
	return ['total_discount','final_amount','product_id','item_id','item_title','product_title','sku','qty','amount','product_desc','item_desc','type','modified_by'];	
}