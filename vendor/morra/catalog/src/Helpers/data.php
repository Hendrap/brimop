<?php 

function castingPrice($item,$isRetail = false){
	if($item->sale_price == 0){
		if($isRetail) return $item->msrp;
		return $item->price;
	}

	if($item->schedule_sale == 0) return $item->sale_price;
	
	$now = strtotime(date('Y-m-d H:i:s'));
	
	if($now < strtotime($item->sale_end_date) && $now > strtotime($item->sale_start_date)) return $item->sale_price; 
	return $item->price;
}
function translateCartToParsedData()
{
	$cart = CatalogCart::getItems(['item','item.entry','item.entry.metas','item.entry.brand','item.entry.medias'=>function($q){
		$q->select(['medias.path']);
	}]);
	$orderDetails = [];
	$subTotal = 0;
	$totalWeight = 0;

	foreach($cart as $v){
		$price = castingPrice($v->item);
		$finalAmount = $v->qty * $price;
		$subTotal += $finalAmount;
		$weight = (float)getEntryMetaFromArray(@$v->item->entry->metas,'package_weight') * $v->qty;
		$totalWeight += $weight;
		$orderDetails[] = (object)[
			'id' => $v->id,
			'item_id' => $v->item->id,
			'item_title' => parseMultiLang($v->item->item_title),
			'product_id' => $v->item->product_id,
			'product_title' => parseMultiLang($v->item->entry->title),
			'product_slug' => $v->item->entry->slug,
			'sku' => $v->item->sku,
			'brand_name' => parseMultiLang(@$v->item->entry->brand->title),
			'amount' => $price,
			'qty' => $v->qty,
			'image_thumbnail' => asset(getCropImage(@$v->item->entry->medias[0]->path,'thumbnail')),
			'image_original' => asset(@$v->item->entry->medias[0]->path),
			'qty_available' => $v->item->stock_available,
			'qty_total' => $v->item->stock_available + $v->qty,
			'total_discount' => 0,
			'final_amount' => $finalAmount,
			'final_amount_non_discount' => $finalAmount,
			'weight' => $weight,
		];
	}

	$userId = 0;
	if(Auth::user()) $userId = Auth::user()->id;
	$data = array(
		'order' => (object)[
			'user_id' => $userId,
			//global discount
			'total_discount' => 0,
			'totalWeight' => ceil($totalWeight),
			//jumlah SUM unitprice/amount * qty semua items,belum terpengaruh discount apapun sama sekali
			'original_subtotal' => $subTotal,
			'shipping_amount' => (float)Session::get('shipping_fee'),
			'subtotal' => $subTotal,
			'tax'=>0,
		],
		'varsOrder' => defaultOrderVars(),
		'orderDetails' => $orderDetails,
		'varsOrderDetail' => defaulOrderDetailsVars(),
	);
	return $data;
}	