<?php

/*
 * Catalog Bank Transfer
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
*/


namespace Morra\Catalog\Core\Promo;
use Morra\Catalog\Core\Promo as AbsPromo;

class PercentageDicount extends AbsPromo
{	
	//param array|object:catalog_promos|boolean
	public function process($paramData,$coupon,$isCheckout)
	{
		
		$data = $paramData;
		//validate
		if($this->validate($paramData,$coupon)){

			$total = $data['order']->original_subtotal + $data['order']->shipping_amount;
			$total = (float)($coupon->rule_detail / 100) * $total;
			$data['order']->total_discount += $total;			
		}
		$data['promos'][] = (object)[
				'level' => 'order',
				'amount' => $total,
				'promo_code' => $coupon->coupon_code
		];
		
		return $data;

	}
	
}