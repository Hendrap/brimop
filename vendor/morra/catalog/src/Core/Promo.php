<?php

namespace Morra\Catalog\Core;

abstract class Promo
{
	abstract public function process($paramData,$coupon,$isCheckout);
	public function validate($paramData,$coupon){
		$data = $paramData;
		$orderTotal = $data['order']->original_subtotal + $data['order']->shipping_amount;

		$user_id = 0;
		if(\Auth::user()) $user_id = \Auth::user()->id;

		if($coupon->user_id != 0 && ($coupon->user_id != $user_id)){
			return false;
		}

		
		if($orderTotal >= (float)$coupon->min_order && $coupon->status == 'active'){
			return true;
		}
		return false;
	}
}