<?php

/*
 * Catalog Cart
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
 */


namespace Morra\Catalog\Core;

/**
* 
*/

abstract class Checkout
{
	abstract public function process();
}