<?php

/*
 * Catalog Shipping Method
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
 */


namespace Morra\Catalog\Core;

/**
* 
*/

abstract class ShippingMethod
{	//return string
	abstract public function getShippingMethodType();
	//return double
	abstract public function getShippingCost();

	abstract public function getNiceShippingMethodName($order);
}