<?php

/*
 * Catalog Setup
 *
 * @author CadisEtramaDiRaizel <Luthfifs97@gmail.com>
 * 
 * 
 */


namespace Morra\Catalog\Core;

/**
* 
*/
class Setup
{
	
	function __construct()
	{
			
	}
	
	function run(){
		$this->setMenu();
		$this->setCore();
	}
	public function setCore(){
		
		app()->bind('Alpha_Catalog_Cart','Morra\Catalog\Core\Cart\CartSession');
		app()->bind('Alpha_Catalog_ShippingMethod','Morra\Catalog\Core\Shipping\Jne');
		app()->bind('Alpha_Catalog_Checkout','Morra\Catalog\Core\Checkout\SimpleCheckout');
		app()->bind('Alpha_Catalog_PaymentMethod','Morra\Catalog\Core\PaymentMethod\Bank');


		//binding payment method goes heere
		app()->bind('Alpha_Catalog_PaymentMethodList_bank','Morra\Catalog\Core\PaymentMethod\Bank');

		//binding shipping method goes here
		app()->bind('Alpha_Catalog_ShippingMethodList_jne','Morra\Catalog\Core\Shipping\Jne');

		//binding promo
		app()->bind('Alpha_Catalog_PromoContainer','Morra\Catalog\Core\PromoContainer');
		$promos = \Config::get('catalog.promo-algoritm');
		foreach ($promos as $key => $value) {
			app()->bind('Alpha_Catalog_Promo_'.$key,$value->class);
		}




	}
	public function setMenu(){
		$settings = [];

		foreach (app('AlphaSetting')->entries as $key => $value) {
			if(strpos($value['slug'],'catalog_') === 0){
				$settings[] = $value;
			}
		}

		app('AlphaMenu')->add((object)['name'=>'Catalog','class'=>' icon-cart4','link'=>'#'],'alpha_catalog');
		app('AlphaMenu')->addChildTo('alpha_catalog',(object)['name'=>'Statistics','class'=>'icon-stats-bars2','link'=>route('catalog_stat')],'alpha_catalog_stat');
		app('AlphaMenu')->addChildTo('alpha_catalog',(object)['name'=>'Setting','class'=>'icon-gear','link'=>route('catalog_setting')],'alpha_catalog_setting');
		app('AlphaMenu')->addChildTo('alpha_catalog',(object)['name'=>'Orders','class'=>' icon-coins','link'=>route('catalog_orders_index')],'alpha_catalog_order_index');
		app('AlphaMenu')->addChildTo('alpha_catalog',(object)['name'=>'Promos','class'=>' icon-coin-dollar','link'=>route('catalog_promo_index')],'alpha_catalog_promo_index');

		$icons = ['single'=>'icon-store','variant'=>'icon-store2'];

		foreach ($settings as $key => $value) {
			$type = explode('_', $value['slug']);
			app('AlphaMenu')->addChildTo('alpha_catalog',(object)['name'=>$value['plural'],'class'=>@$icons[@$type[1]],'link'=>route('catalog_product_variant_index',@$type[2])],'alpha_catalog_type_'.$value['slug']);
		}
		app('AlphaMenu')->addChildTo('alpha_catalog',(object)['name'=>'Wishlists','class'=>'icon-basket','link'=>route('catalog_wishlists_index')],'alpha_catalog_wishlists');

	}
}
