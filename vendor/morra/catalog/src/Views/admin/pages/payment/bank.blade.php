@if(!empty($payment))
<span class="text-muted">Payment Details:</span>
<ul class="list-condensed list-unstyled invoice-payment-details">
	<li><h5>Amount: <span class="text-right text-semibold">{{number_format($payment->amount)}}</span></h5></li>
	<li>Bank Name: <span class="text-semibold">{{$payment->bank}}</span></li>
	<li>User Account: <span>{{$payment->user_account}}</span></li>
	<li>User Holder: <span>{{$payment->user_holder}}</span></li>
	<li>User Bank: <span>{{$payment->user_bank}}</span></li>
	<li>Refnumber: <span>{{$payment->refnumber}}</span></li>
</ul>
@endif
