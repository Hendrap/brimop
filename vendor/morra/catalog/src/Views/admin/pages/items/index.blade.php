<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">
						<?php $parts = explode('_', $product->entry_type) ?>
						<a href="<?php echo route('catalog_product_variant_edit',[$parts[2],$product->id]) ?>"><button class="btn" style="margin-right: 20px;">Back To Product</button></a>
						<?php echo parseMultiLang($product->title) ?>'s Items
						</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<?php if($catalogtype == 'group'){ ?>
									<li><a href="{{ route('catalog_item_group_create',$product->id) }}"><i class="icon-plus3"></i></a></li>
		                		<?php }else{ ?>
		                			<li><a href="{{ route('catalog_item_variant_create',$product->id) }}"><i class="icon-plus3"></i></a></li>
		                		<?php } ?>
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="panel-heading">
						<hr>
						<div class="row">
							<div class="col-md-4">
								<?php echo parseMultiLang($product->title) ?>'s Items
							</div>
						</div>
						<hr>
					</div>
				@if(session('msg'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-success">{{session('msg')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				@if(session('err'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-danger">{{session('err')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Title</th>
						<th>SKU</th>
						<th>MSRP</th>
						<th>Price</th>
						<th>Stock Status</th>
						<th>Status</th>
						<th></th>
					</tr>

					<?php $stock_status = [1=>'In Stock',0=>'Out Of Stock'] ?>
					<?php $status = [1=>'Active',0=>'In Active'] ?>
					@foreach($items as $item)
					<tr>
						<td>{{ $item->item_title }}</td>
						<td>{{ $item->sku }}</td>
						<td>{{ number_format($item->msrp,2,'.','') }}</td>
						<td>{{ number_format($item->price,2,'.','') }}<p class="text-success">{{ number_format($item->sale_price,2,'.','') }}</p></td>
						<td>{{ $stock_status[$item->stock_status] }}</td>
						<td>{{ $status[$item->status] }}</td>
						<td></td>
					<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">

				                    	<?php if($catalogtype == 'group'){ ?>
		<li><a href="{{ route('catalog_item_group_edit',[$product->id,$item->id]) }}"><i class="icon-pencil"></i> Edit</a></li>										
										<?php }else{ ?>
		<li><a href="{{ route('catalog_item_variant_edit',[$product->id,$item->id]) }}"><i class="icon-pencil"></i> Edit</a></li>
										
										<?php } ?>


		<li><a class="confirm" href="{{ route('catalog_item_variant_status',[$item->id,'deleted']) }}"><i class=" icon-x"></i> Delete</a></li>
											
										</ul>
								</div>
						</td>				
					</tr>
					@endforeach
				</table>
			</div>
			
		
		<div class="row">
			<div class="col-md-6">
				<?php
					 if(!empty($_GET)){
						foreach ($_GET as $key => $value) {
							$items = $items->appends([$key => $value]);
						}
					 }
				?>
				{!! $items->render() !!}
			</div>
		</div>


<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				window.location.href = $(this).attr('href');
			}
		});

	});
	
</script>