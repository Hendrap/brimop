<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">{{$info['plural']}}</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a href="{{ route('catalog_product_variant_create',[$catalogType]) }}"><i class="icon-plus3"></i></a></li>
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="panel-heading">
						<hr>
						<div class="row">
							<div class="col-md-4">
								<b>{{$total}} {{$info['plural']}}</b> -- Last updated by {{$theLastName}} on {{$theLastDate}}
							</div>
							<form>
							<div class="col-md-3">
								<input name="q" placeholder="Type Title or Description" type="text" class="form-control">
							</div>
								<div class="col-md-1">
						
								<select name="sort" class="form-control">
									<option value="0">Sort</option>
									<option {{ ($sort == 'asc') ? 'selected' : '' }} value="asc">Ascending</option>
									<option {{ ($sort == 'desc') ? 'selected' : '' }} value="desc">Descending</option>
									<option {{ ($sort == 'oldest') ? 'selected' : '' }} value="oldest">Oldest</option>
									<option {{ ($sort == 'latest') ? 'selected' : '' }} value="latest">Latest</option>
								</select>
							</div>	
							<div class="col-md-1">
						
								<select name="stock" class="form-control">
									<option value="0">Stock</option>
									<option {{ ($stock == 'in-stock') ? 'selected' : '' }} value="in-stock">In-Stock</option>
									<option {{ ($stock == 'out-of-stock') ? 'selected' : '' }} value="out-of-stock">Out Of Stock</option>
									
								</select>
							</div>	
							<div class="col-md-2">
						
								<select name="brand" class="form-control">
									<option value="0">Brand</option>
									@foreach($brands as $theBrand)
									<option {{ ($brand == $theBrand->id) ? 'selected' : '' }} value="{{ $theBrand->id }}">{{ parseMultiLang($theBrand->title) }}</option>
									@endforeach
									
								</select>
							</div>	
							<div class="col-md-1">
								<button type="submit" class="btn btn-info">Search</button>
							</div>
							</form>

						</div>
						<hr>
					</div>
				@if(session('msg'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-success">{{session('msg')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				@if(session('err'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-danger">{{session('err')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Title</th>
						<th>User</th>
						<th>Status</th>
						<th>Created at</th>
						<th>Update at</th>
						<th></th>
					</tr>

					@foreach($entries as $entry)
					<tr>
						<td>{{parseMultiLang($entry->title)}}</td>
						<td>{{@$entry->user->username}}</td>
						<?php 
						$className = 'default';
						switch ($entry->status) {
							case 'published':
								$className = 'success';
								break;
							case 'draft':
								$className = 'info';
								break;
							case 'disabled':
								$className = 'danger';
								break;	
							default:
								# code...
								break;
						}
						 ?>
						<td><span class="label label-{{ $className }}">{{ ucfirst($entry->status) }}</span></td>
						<td>{{ date('d F Y',strtotime($entry->created_at)) }}</td>
						<td>{{ date('d F Y',strtotime($entry->updated_at)) }}</td>

						<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">
<li><a href="{{ route('catalog_product_variant_edit',[$catalogType,$entry->id]) }}"><i class="icon-pencil"></i> Edit</a></li>
		<?php if($entry->status == 'published'){ ?>
		<li>
			<a href="{{ route('catalog_product_variant_status',[$entry->id,'disabled']) }}">
			<i class=" icon-cancel-square"></i> Disable</a>
		</li>
		<?php }else{ ?>
		<li>
			<a href="{{ route('catalog_product_variant_status',[$entry->id,'published']) }}">
			<i class=" icon-checkmark4"></i> Publish</a>
		</li>
		<?php } ?>

		<li><a class="confirm" href="{{ route('catalog_product_variant_status',[$entry->id,'deleted']) }}"><i class=" icon-x"></i> Delete</a></li>
											
										</ul>
								</div>
						</td>				
					</tr>
					@endforeach
				</table>
			</div>
			
		
		<div class="row">
			<div class="col-md-6">
				<?php
					 if(!empty($_GET)){
						foreach ($_GET as $key => $value) {
							$entries = $entries->appends([$key => $value]);
						}
					 }
				?>
				{!! $entries->render() !!}
			</div>
		</div>


<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				window.location.href = $(this).attr('href');
			}
		});

	});
	
</script>