<div class="row">
	<div class="col-md-12">
		<button id="btnDoMigrate">Klik Disini</button>
	</div>
</div>
<script type="text/javascript">
	var page = 1;
	var maxPage = 304;
	function doMigrate(){
		$.ajax({
			url:"",
			type:"POST",
			dateType:"json",
			data:{page:page},
			success:function(data){
				console.log('Page : ' + page);
				page++;
				if(page < maxPage){
					doMigrate();					
				}else{
					alert("Done!");
				}
			}
		});
	}
	$("#btnDoMigrate").click(function(e){
		e.preventDefault();
		doMigrate();
	})
</script>