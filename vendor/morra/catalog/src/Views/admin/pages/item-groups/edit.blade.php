<style type="text/css">
	.taxo{
    max-height: 350px;
    min-height: 350px;
    overflow-y: scroll;
	}
</style>
<script src="{{ asset('backend/plugins/jQueryUI/jquery-ui.js') }}"></script>
<form class="form-horizontal" action="{{ route('catalog_item_group_save',[$product->id,$item->id]) }}" method="POST">
		<div class="panel panel-flat">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-10">
							<?php $parts = explode('_', $product->entry_type) ?>

							<h1 class="panel-title">Edit Item</h1>

						</div>
						<div class="col-md-1">
							<a href="<?php echo route('catalog_product_variant_edit',[$parts[2],$product->id]) ?>" class="btn" style="background-color: buttonface;margin-right: 20px;">Back To Product
							</a>
						</div>
						<div class="col-md-1">
							<a href="<?php echo route('catalog_item_variant_index',[$product->id]) ?>" class="btn" style="background-color: buttonface;margin-right: 20px;">Back To Item
							</a>
						</div>
						
						
					</div>
				</div>
			<div class="panel-body">
				<h2>Basic</h2>
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif

				

				<div class="form-group">
					<label class="control-label col-lg-2">Item Title</label>
					<div class="col-lg-10">
					<input id="item_title" name="item_title" type="text" class="form-control" value="{{old('item_title',$item->item_title)}}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Item Description</label>
					<div class="col-lg-10">
						<textarea style="display:none" id="item_description" name="item_description" rows="10" cols="80">{{old('item_description',$item->item_description)}}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">SKU</label>
					<div class="col-lg-10">
					<input id="sku" name="sku" type="text" class="form-control" value="{{old('sku',$item->sku)}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">MSRP</label>
					<div class="col-lg-10">
					<input id="msrp" name="msrp" type="text" class="form-control" value="{{old('msrp',number_format($item->msrp,2,'.',''))}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Price</label>
					<div class="col-lg-10">
					<input id="price" name="price" type="text" class="form-control" value="{{old('price',number_format($item->price,2,'.',''))}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Sale Price</label>
					<div class="col-lg-10">
					<input id="sale_price" name="sale_price" type="text" class="form-control" value="{{old('sale_price',number_format($item->sale_price,2,'.',''))}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Stock</label>
					<div class="col-lg-10">
					<input id="stock" name="stock" type="text" class="form-control" value="{{old('stock',$item->stock)}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Stock Available</label>
					<div class="col-lg-10">
					<input disabled="" type="text" class="form-control" value="{{$item->stock_available}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Stock Status</label>
					<div class="col-lg-10">
						<select class="form-control" name="stock_status">
							<option {{ (old('stock_status',$item->stock_status) == 1) ? 'selected' : "" }} value="1">In Stock</option>
							<option {{ (old('stock_status',$item->stock_status) == 0) ? 'selected' : "" }} value="0">Out Of Stock</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Schedule Sale</label>
					<div class="col-lg-10">
						<select class="form-control" name="schedule_sale">
							<option {{ (old('schedule_sale',$item->schedule_sale) == 1) ? 'selected' : "" }} value="1">Yes</option>
							<option {{ (old('schedule_sale',$item->schedule_sale) == 0) ? 'selected' : "" }} value="0">No</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Sale Start</label>
					<div class="col-lg-10">
						<input id="sale_start_date" name="sale_start_date" type="text" class="date form-control" value="{{ date('Y-m-d H:i:s',strtotime(old('sale_start_date',$item->sale_start_date))) }}">	
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Sale End</label>
					<div class="col-lg-10">
						<input id="sale_end_date" name="sale_end_date" type="text" class="date form-control" value="{{ date('Y-m-d H:i:s',strtotime(old('sale_end_date',$item->sale_end_date))) }}">	
					</div>
				</div>					
				<div class="form-group">
					<label class="control-label col-lg-2">Status</label>
					<div class="col-lg-10">
						<select class="form-control" name="status">
							<option {{ (old('status',$item->status) == 1) ? 'selected' : "" }} value="1">Active</option>
							<option {{ (old('status',$item->status) == 0) ? 'selected' : "" }} value="0">In-Active</option>
						</select>
					</div>
				</div>

			
				{{csrf_field()}}
				
			</div>


			<div class="panel-body text-right">
						<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
			</div>
</div>


				
				<div class="row">
					<div class="col-md-12">
						<!-- Add options dynamically -->
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h2 class="panel-title">Item Group</h2>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">
								<div class="content-group">
									<select name="group_items[]" multiple="multiple" class="form-control listbox-dynamic-options">
									<?php 
											if(!empty(old('group_items'))){ 
											$group = CatalogItem::with('entry')->whereIn('id',old('group_items'))->get();
											foreach ($group as $key => $value) {
								    ?>
											<option selected="" value="<?php echo $value->id ?>"><?php echo parseMultiLang(@$value->entry->title).' | '.$value->id.' | '.@$value->item_title ?> </option>

									<?php }}else{ ?>
										<?php foreach ($item->group as $key => $value) { ?>
											<option selected="" value="<?php echo $value->item_id ?>"><?php echo parseMultiLang(@$value->item->entry->title).' | '.$value->item_id.' | '.@$value->item->item_title ?> </option>
										<?php }} ?>
									</select>
								</div>	
							</div>
						</div>
						<!-- /add options dynamically -->
					</div>
				</div>


				@if(!empty($metas))
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h2>Additional Info</h2>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse" class=""></a></li>
				                		<!-- <li><a data-action="close"></a></li> -->
				                	</ul>
			                	</div>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							
								<div class="panel-body">
									
									@foreach($metas as $meta)
											<div class="form-group">
												<label class="control-label col-lg-2">{{ $meta->meta_name }}</label>
												<div class="col-lg-10">
													<?php echo view('alpha::admin.meta.'.$meta->meta_data_type,[
													 	'meta_key' => $meta->meta_key,
													 	'value' => old('metas.'.$meta->meta_key,getEntryMetaFromArray($item->metas,$meta->meta_key)),
														 ]) ?>
												</div>
											</div>

										@endforeach
								</div>
						</div>
					</div>
				</div>
				@endif

				@if(!empty($taxo))
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h2>Taxonomy</h2>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse" class=""></a></li>
				                		<!-- <li><a data-action="close"></a></li> -->
				                	</ul>
			                	</div>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="panel-body">
								
									<div class="row">
										@if(!empty($taxo))
										@foreach($taxo as $val)
										
										<div class="col-md-4">
										<div class="panel-heading">
											<b>{{$val['single']}}</b>
										</div>
										<div class="panel panel-body taxo">
											<div class="form-group">
											
											 @foreach($taxonomies as $taxo)
											 	@if($taxo->taxonomy_type == $val['slug'])
							                      <div class="checkbox">
							                        <label>
							                          <input <?php echo in_array($taxo->id, old('taxonomies',$selectedTaxo)) ? 'checked' : '' ?> name="taxonomies[]" value="{{ $taxo->id }}" type="checkbox">
							                          {{ $taxo->name }}
							                        </label>
							                      </div>
							                     @endif
						                      @endforeach
					                   		 </div>
										</div>
										</div>
										@endforeach
										@endif
									</div>
							</div>
								
						</div>
					</div>
				</div>
				@endif



<div class="row">
					<div class="col-md-12">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h2>Media</h2>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a href="#" id="btnShowEntryMedia"><i class="icon-plus3"></i></a></li>
				                		<li><a data-action="collapse" class=""></a></li>
				                	</ul>
			                	</div>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="panel-body">
								
									<div class="row" id="containerMedia">
											<?php 
							
											$res = parseMedias($item->medias);
											echo $res;
											 ?>
									</div>
							</div>
								
						</div>
					</div>
				</div>




</form>
<script src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/js/jquery.slug.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/assets/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/assets/js/duallistbox.min.js') }}"></script>
<script type="text/javascript">

	$( "#containerMedia" ).sortable({
      revert: true
    });
	function hideAllTitle(){
		
	}
	function hideAllEditors(){	
		
	}
	function showDefaultEditor(){
		
	}

	CKEDITOR.replace("item_description");

	CKEDITOR.on("instanceReady", function(event)
	{
		hideAllEditors();
		showDefaultEditor();
		$(".cke_button__image_icon").hide();
		$(".cke_button__image_icon").parent().hide();
	});

	$("#changeLang").change(function(e){
		hideAllEditors();
		hideAllTitle();
		defaultLang = $("#changeLang option:selected").val();
		$("#title").show();
		showDefaultEditor();

	});

	
	$(document).ready(function(){
		hideAllTitle();
		$("#title").show();
		$(".date").AnyTime_picker({
		  format: "%Y-%m-%d %H:%i:%s",
		});
	     $('select[name="group_items[]"]').bootstrapDualListbox({
        	nonSelectedListLabel: 'Non-selected Items',
		    selectedListLabel: 'Selected Items',
        	moveOnSelect: false
  	    });
	   

	    var checkExist = setInterval(function() {
		if ($('.box1').length) {
		      $(".box1").find(".filter").replaceWith('<div class="row"><div class="col-md-10"><input id="txt_items" class="form-control" type="text" placeholder="Filter"></div><div class="col-md-2"><button id="btn-item-search" class="btn btn-info">Search Items</button></div></div><br>');
		      clearInterval(checkExist);
		   }
		}, 100);
	});
	$(document).on('click','#btn-item-search',function(e){
		e.preventDefault();
		$.ajax({
			'url':'<?php echo route('catalog_item_variant_search') ?>',
			'type':'GET',
			'dataType':'json',
			'data':{q:$("#txt_items").val()},
			'success':function(result){
				$('.listbox-dynamic-options option:not(:selected)').remove();
				var title = '';
				$.each(result.data,function(i,k){
					title = '';title = k.title + ' | ' + k.id + ' | ' + k.item_title;
					$('.listbox-dynamic-options').append('<option value="' + k.id + '">' + title + '</option>');
				});
        		$('.listbox-dynamic-options').trigger('bootstrapDualListbox.refresh');
			}
		});
	});

</script>