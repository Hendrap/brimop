 <div class="row">
 <div class="col-lg-4">
<div class="panel panel-flat">
	<div class="panel-heading">
						<h5 class="panel-title">Product Type</h5>
					</div>
	<div class="panel-body">
		@if(session('success'))
           			<div class="alert alert-success">
           				{{session('success')}}
           			</div>
         		   @endif
      
        	
        		<form class="form-horizontal" method="POST" action="">
					{{ csrf_field() }}
					<fieldset class="content-group">
						
						<div class="form-group">
							<div class="col-lg-4">
								<input name="" type="text" class="form-control" id="" value="">
							</div>
							<div class="col-lg-8">
								<select class="form-control"></select>
							</div>
						</div>
						
					</fieldset>
					<div class="text-right">
						<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
					</div>
				</form>
        	
       
		
	</div>					
</div>
</div>
 </div>