<script type="text/javascript">
	var echart_url = "<?php echo asset('backend/assets/js/echarts') ?>";
</script>
<script type="text/javascript" src="{{ asset('backend/assets/js/echarts/echarts.js') }}"></script>
<script type="text/javascript">
	$(function () {

    require.config({
        paths: {
            echarts: echart_url
        }
    });

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],

        function (ec, limitless) {

            var basic_columns = ec.init(document.getElementById('basic_columns'), limitless);

            basic_columns_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 40,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['Order', 'Revenue']
                },

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: 'Order',
                        type: 'bar',
                        data: <?php echo json_encode($totalOrder) ?>,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    },
                    {
                        name: 'Revenue',
                        type: 'bar',
                        data: <?php echo json_encode($totalRevenue) ?>,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    }
                ]
            };
            basic_columns.setOption(basic_columns_options);
            window.onresize = function () {
                setTimeout(function () {
                    basic_columns.resize();
                }, 200);
            }
        }
    );
});

</script>
<div class="row">
					<div class="col-lg-5">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">CATALOGS</h6>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="container-fluid">
								<div class="row text-center">
									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class=" icon-store2 position-left text-slate"></i>{{ $totalProduct }}</h5>
											<span class="text-muted text-size-small">Products</span>
										</div>
									</div>

									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="  icon-store position-left text-slate"></i> {{ $totalItem }}</h5>
											<span class="text-muted text-size-small">Items</span>
										</div>
									</div>

									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class=" icon-basket position-left text-slate"></i>{{ $totalWishlist }}</h5>
											<span class="text-muted text-size-small">Wishlists</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-5">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h6 class="panel-title">SALES ORDER</h6>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

							<div class="container-fluid">
								<div class="row text-center">
									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class=" icon-coins position-left text-slate"></i>{{ $totalAllOrder }}</h5>
											<span class="text-muted text-size-small">Orders</span>
										</div>
									</div>

									<div class="col-md-4">
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="  icon-cash position-left text-slate"></i> {{ $totalOrderRevenue }}</h5>
											<span class="text-muted text-size-small">REVENUE </span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
<div class="row">
	<div class="col-lg-10">
	
				<!-- Basic column chart -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">SALES {{ $selectYear }}</h5>
						<div class="heading-elements">
									<form class="heading-form" action="#">
										<div class="form-group">
										
											<div class="btn-group">
										    <button type="button" class="multiselect dropdown-toggle btn btn-link text-semibold" data-toggle="dropdown" title="June, 15 - June, 21">
																					<span class="multiselect-selected-text">
																					<span class="status-mark border-warning position-left"></span>

																					SALES {{ $selectYear }}</span> 
																					<b class="caret"></b></button>
										    <ul class="multiselect-container dropdown-menu pull-right">
										        <li class="multiselect-item multiselect-group">
										            <label><i class="icon-watch pull-right"></i> Time period</label></li>
										        @foreach($years as $v)
										        <li>
											        <a href="<?php echo route('catalog_stat').'?year='.$v->year ?>" tabindex="0">
												        <label class="radio">
													        <div class="{{ ($selectYear == $v->year) ? 'choice' : '' }}">
														        <span>
													        		
													        	</span>
															</div>
															{{$v->year}}
														</label>
													</a>
												</li>
										       @endforeach
										      
										     

										    </ul>
										</div>
										</div>
									</form>
			                	</div>
					</div>

					<div class="panel-body">
						<div class="chart-container">
							<div class="chart has-fixed-height" id="basic_columns"></div>
						</div>
					</div>
				</div>
				<!-- /basic column chart -->


	</div>
</div>