<script src="{{ asset('backend/plugins/jQueryUI/jquery-ui.js') }}"></script>
<form class="form-horizontal" action="{{ route('catalog_promo_save') }}" method="POST">
<input type="hidden" name="promo_id" value="{{ $promo->id }}">
<div class="panel panel-flat">
	<div class="panel-body">
	<h2>Edit Promo</h2>
				@if(count($errors) > 0)
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">{{$error}}</div>
					@endforeach
				@endif

				@if(session('msg'))
					<div class="alert alert-success">{{session('msg')}}</div>
				@endif
				

				<div class="form-group">
					<label class="control-label col-lg-2">Title</label>
					<div class="col-lg-10">
					<input id="title" name="title" type="text" class="form-control" value="{{old('title',$promo->title)}}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Description</label>
					<div class="col-lg-10">
						<textarea style="display:none" id="content" name="content" rows="10" cols="80">{{old('content',$promo->content)}}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Status</label>
					<div class="col-lg-10">
					<select name="status" class="form-control">
						<option {{ (old('content',$promo->status) == 'active') ? 'selected' : '' }} value="active">Active</option>
						<option {{ (old('content',$promo->status) == 'in-active') ? 'selected' : '' }} value="in-active">In-Active</option>
					</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Coupon Code</label>
					<div class="col-lg-8">
						<input id="coupon_code" name="coupon_code" type="text" class="form-control" value="{{old('coupon_code',$promo->coupon_code)}}">
					</div>
					<div class="col-lg-2">
						<button id="generate" class="btn btn-info">Generate Code</button>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Coupon Type</label>
					<div class="col-lg-10">
					<select name="coupon_type" class="form-control">
						<option {{ (old('content',$promo->coupon_type) == 'common') ? 'selected' : '' }} value="common">Common</option>
						<option {{ (old('content',$promo->coupon_type) == 'onetime') ? 'selected' : '' }} value="onetime">One-Time Use</option>
					</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Algorithm</label>
					<div class="col-lg-10">
					<select name="algorithm" class="form-control">
						<?php $algo = Config::get('catalog.promo-algoritm'); ?>
						<?php foreach ($algo as $key => $value) { ?>
						<option {{ (old('content',$promo->algorithm) == $key) ? 'selected' : '' }} value="{{ $key }}">{{$value->title}}</option>
						<?php } ?>

					</select>
					</div>
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Min Order</label>
					<div class="col-lg-10">
					<input id="min_order" name="min_order" type="text" class="form-control" value="{{old('min_order',$promo->min_order)}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Amount / Percent(%)</label>
					<div class="col-lg-10">
					<input id="rule_detail" name="rule_detail" type="text" class="form-control" value="{{old('rule_detail',$promo->rule_detail)}}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">User</label>
					<div class="col-lg-10">
					<input id="user_id" name="user_id" type="text" class="form-control" value="{{old('user_id',$promo->user_id)}}">
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="control-label col-lg-2">Start</label>
					<div class="col-lg-10">
						<input id="start" name="start" type="text" class="date form-control" value="{{ date('Y-m-d H:i:s',strtotime(old('start',$promo->start))) }}">	
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">End</label>
					<div class="col-lg-10">
						<input id="end" name="end" type="text" class="date form-control" value="{{ date('Y-m-d H:i:s',strtotime(old('end',$promo->end))) }}">	
					</div>
				</div>					
				

				{{csrf_field()}}

		</div>
		<div class="panel-body text-right">
				<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
		</div>
</div>
				


			


				



		</form>

<script src="{{ asset('backend/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/assets/js/select2.min.js') }}"></script>
<script type="text/javascript">

	CKEDITOR.replace("content");

	CKEDITOR.on("instanceReady", function(event)
	{
		$(".cke_button__image_icon").hide();
		$(".cke_button__image_icon").parent().hide();
	});

	function makeid()
	{
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 6; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	}

	
	$(document).ready(function(){
		$("#title").show();
		$(".date").AnyTime_picker({
		  format: "%Y-%m-%d %H:%i:%s",
		});

		$("#generate").click(function(e){
			e.preventDefault();
			$("#coupon_code").val(makeid());
		})
	    
	});

</script>