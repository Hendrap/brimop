<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">
						Promos
						</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								
		                		<li><a href="{{ route('catalog_promo_create') }}"><i class="icon-plus3"></i></a></li>
		                		
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="panel-heading">
						<hr>
						<div class="row">
							<div class="col-md-4">
								Promos
							</div>
						</div>
						<hr>
					</div>
				@if(session('msg'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-success">{{session('msg')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				@if(session('err'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-danger">{{session('err')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Title</th>
						<th>Coupon Code</th>
						<th>Min Order</th>
						<th>Start</th>
						<th>End</th>
						<th>Status</th>
						<th></th>
					</tr>

					@foreach($promos as $item)
					<tr>
						<td>{{ $item->title }}</td>
						<td>{{ $item->coupon_code }}</td>
						<td>{{ number_format((float)$item->min_order,2,'.','') }}</td>
						<td>{{ date('d-m-Y',strtotime($item->start)) }}</td>
						<td>{{ date('d-m-Y',strtotime($item->end)) }}</td>
						<td>{{ $item->status }}</td>
						<td></td>
					<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>

				                    	<ul class="dropdown-menu dropdown-menu-right">

				                   
		<li><a href="{{route('catalog_promo_edit',$item->id)}}"><i class="icon-pencil"></i> Edit</a></li>
										
									


		<li><a class="confirm" href="{{route('catalog_promo_delete',$item->id)}}"><i class=" icon-x"></i> Delete</a></li>
											
										</ul>
								</div>
						</td>				
					</tr>
					@endforeach
				</table>
			</div>
			
		
		<div class="row">
			<div class="col-md-6">
				<?php
					 if(!empty($_GET)){
						foreach ($_GET as $key => $value) {
							$promos = $promos->appends([$key => $value]);
						}
					 }
				?>
				{!! $promos->render() !!}
			</div>
		</div>


<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
				window.location.href = $(this).attr('href');
			}
		});

	});
	
</script>