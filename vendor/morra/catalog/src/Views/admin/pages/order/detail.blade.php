<form method="POST" action="{{ route('catalog_orders_save') }}">
<input type="hidden" name="order_id" value="{{ $order->id }}">
{!! csrf_field() !!}
<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">Order #{{$order->order_number}}</h6>
						<div class="heading-elements">
							<button type="submit" class="btn btn-default btn-xs heading-btn"><i class="icon-file-check position-left"></i> Save</button>
							<button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-file-check position-left"></i> Edit</button>
							<button id="print_btn" type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
	                	</div>
					<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

					<div class="panel-body no-padding-bottom">
						<div class="row">
							<div class="col-sm-6 content-group">
								<span class="text-muted">Billing Address:</span>
	 							<ul class="list-condensed list-unstyled">
									<li><h5>{{$order->payment_firstname.' '.$order->payment_firstname}}</h5></li>
									<li><span class="text-semibold">{{$order->payment_address_1}}</span></li>
									<li>{{$order->payment_address_2}}</li>
									<li>{{$order->payment_city}}</li>
									<li>{{$order->payment_state}}</li>
									<li>{{$order->payment_phone}}</li>
									<li><a href="#">{{$order->payment_email}}</a></li>
								</ul>
							</div>

							<div class="col-sm-6 content-group">
								<div class="invoice-details">
									<h5 class="text-uppercase text-semibold">Order #{{$order->order_number}}</h5>
									<ul class="list-condensed list-unstyled">
										<li>Date: <span class="text-semibold">{{ date('F d Y',strtotime($order->created_at)) }}</span></li>
									
									</ul>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6 col-lg-9 content-group">
								<span class="text-muted">Shipping Address:</span>
	 							<ul class="list-condensed list-unstyled">
									<li><h5>{{$order->shipping_firstname.' '.$order->shipping_firstname}}</h5></li>
									<li><span class="text-semibold">{{$order->shipping_address_1}}</span></li>
									<li>{{$order->shipping_address_2}}</li>
									<li>{{$order->shipping_city}}</li>
									<li>{{$order->shipping_state}}</li>
									<li>{{$order->shipping_phone}}</li>
									<li><a href="#">{{$order->shipping_email}}</a></li>
								</ul>
							</div>

							<div class="col-md-6 col-lg-3 content-group">
								{!! $payment->render($order->id); !!}
							</div>
						</div>
					</div>

					<div class="table-responsive">
					    <table class="table table-lg">
					        <thead>
					            <tr>
					                <th>Description</th>
					                <th class="col-sm-1">QTY</th>
					                <th class="col-sm-1">Unit Price</th>
					                <th class="col-sm-1">Discount</th>
					                <th class="col-sm-1">Total</th>
					            </tr>
					        </thead>
					        <tbody>
					        	<?php $subtotal = 0 ?>
					        	<?php foreach ($order->details as $key => $value) { ?>
					            <tr>
					                <td>
					                	<h6 class="no-margin">{{$value->item_title}}</h6>
					                	<span class="text-muted">{{$value->item_desc}}</span>
				                	</td>
					                <td>{{$value->qty}}</td>
					                <td>{{number_format($value->amount)}}</td>
					                <td>{{number_format($value->total_discount)}}</td>
					                <td><span class="text-semibold">{{ number_format($value->final_amount) }}</span></td>
					            </tr>
					           <?php } ?>
					         
					        </tbody>
					    </table>
					</div>

					<div class="panel-body">
						<div class="row invoice-payment">
							<div class="col-sm-6">
								<div class="content-group">
									<h6>Additional Info</h6>
									<div class="table-responsive no-border">
										<table class="table">
											<tbody>
												<tr>
													<th>Order Status</th>
													<td>
														<select name="status" style="padding:0;height:24px;" class="form-control">
																<?php foreach ($status as $key => $value) { ?>
																<option {{ ($order->status == $value) ? 'selected' : '' }} value="{{ $value }}">{{ $value }}</option>
																<?php } ?>
														</select>
													</td>
												</tr>
												<tr>
													<th>Tracking Number</th>
													<td>
														<input style="height:24px" type="text" name="tracking_number" class="form-control" value="{{ $order->tracking_number }}">
													</td>
												</tr>
												<tr>
													<th>Shipping Method</th>
													<td>
														{{$shipping->getNiceShippingMethodName($order)}}
													</td>
												</tr>
												<tr>
													<th>Payment Method</th>
													<td>{{$payment->getNicePaymentMethodName()}}</td>
												</tr>
												<tr><th></th><td></td></tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="content-group">
									<h6>Total due</h6>
									<div class="table-responsive no-border">
										<table class="table">
											<tbody>
												<tr>
													<th>Subtotal:</th>
													<td class="text-right">{{number_format($order->subtotal)}}</td>
												</tr>
												<tr>
													<th>Shipping Amount:</th>
													<td class="text-right">{{number_format($order->shipping_amount)}}</td>
												</tr>
												<tr>
													<th>Tax Amount:</th>
													<td class="text-right">{{number_format($order->tax)}}</td>
												</tr>
												<tr>
													<th>Order Discount:</th>
													<td class="text-right">{{number_format($order->total_discount)}}</td>
												</tr>
												<tr>
													<th>Total:</th>
													<td class="text-right text-primary"><h5 class="text-semibold">{{number_format($order->grand_total)}}</h5></td>
													</tr>
											</tbody>
										</table>
									</div>

									<div class="text-right">
										
									</div>
								</div>
							</div>
						</div>

						<h6>Customer Note</h6>
						<p class="text-muted">{{ $order->customer_note }}</p>

						<!-- <h6>Note</h6>
						<p class="text-muted">{{ $order->note }}</p> -->
					</div>
				</div>
				<form>
				<script type="text/javascript">
					$("#print_btn").click(function(e){
						e.preventDefault();
						window.print();
					});
				</script>