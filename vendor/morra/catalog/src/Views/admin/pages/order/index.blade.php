<div class="panel panel-flat">
<div class="panel-heading">
						<h5 class="panel-title">Orders</h5>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a href="#"><i class="icon-plus3"></i></a></li>
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="panel-heading">
						<hr>
						<div class="row">
							<div class="col-md-4">
								@if(!empty($lastUpdate))
									Last Update "#{{ $lastUpdate->order_number }}" By {{ ($lastUpdate->user_id == 0) ? 'Guest' : $lastUpdate->user->username }} 
								@endif
							</div>
							<div class="col-md-4"></div>
							<form>
							<div class="col-md-3">
								<input name="q" placeholder="Type Order Number" type="text" class="form-control">
							</div>
							
							<div class="col-md-1">
								<button type="submit" class="btn btn-info">Search</button>
							</div>
							</form>

						</div>
						<hr>
					</div>
				@if(session('msg'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-success">{{session('msg')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				@if(session('err'))
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10"><div class="alert alert-success">{{session('err')}}</div></div>
						<div class="col-md-1"></div>
					</div>
				@endif
				<table class="table table-responsive">
					<tr>
						<th>Order Number</th>
						<th>Amount</th>
						<th>Status</th>
						<th>Payment Method</th>
						<th>Created at</th>
						<th>Update at</th>
						<th></th>
					</tr>

					@foreach($orders as $order)
					<tr>
						<td>{{$order->order_number}}</td>
						<td>{{number_format($order->grand_total)}}</td>
						
						<td>{{ ucfirst($order->status) }}</td>
						<td>{{ ucfirst($order->payment_method) }}</td>
						<td>{{ date('d F Y',strtotime($order->created_at)) }}</td>
						<td>{{ date('d F Y',strtotime($order->updated_at)) }}</td>

						<td>
								<div class="btn-group">
				                    	<button data-toggle="dropdown" class="btn btn-primary btn-icon dropdown-toggle" type="button" aria-expanded="false">
					                    	<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
				                    	</button>
				                    	<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="{{route('catalog_orders_detail',[$order->id])}}"><i class="icon-pencil"></i> Detail</a></li>
										</ul>
								</div>
						</td>				
					</tr>
					@endforeach
				</table>
			</div>
			
		
		<div class="row">
			<div class="col-md-6">
				<?php
					 if(!empty($_GET)){
						foreach ($_GET as $key => $value) {
							$orders = $orders->appends([$key => $value]);
						}
					 }
				?>
				{!! $orders->render() !!}
			</div>
		</div>


<script type="text/javascript">
	$(document).ready(function(){
		$(".confirm").click(function(e){
			e.preventDefault();
			if(confirm("Are you sure ?")){
			
			}
		});

	});
	
</script>