<div class="row" id="vueInvoice">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h6 class="panel-title">Customer Data</h6>
			<div class="heading-elements">
				<ul class="icons-list">
				    <li><a data-action="collapse"></a></li>
				</ul>
			</div>
		<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
		<div class="panel-body">
			<div class="row">
				<label class="control-label col-md-1">Customer</label>
				<div class="col-md-4">
					<div class="input-group">
						<div class="input-group-btn">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span></button>
				             <ul class="dropdown-menu">
								<li><a @click="showCreateEditUser('create')" href="#">Create</a></li>
								<li><a @click="showCreateEditUser()" href="#">Edit</a></li>
							</ul>
						</div>
						<input id="userField" type="text" class="form-control clearable" placeholder="Left dropdown">
					</div>
				</div>	
			</div>
		</div>
	</div>
	<div class="panel panel-warning">
		<div class="panel-heading">
			<h6 class="panel-title">Billing & Shipping Address</h6>
			<div class="heading-elements">
				<ul class="icons-list">
				    <li><a data-action="collapse"></a></li>
				</ul>
			</div>
		<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6 content-group">
					<div class="row">
						<div class="col-md-2">
							<span class="text-muted">Billing Address:</span>							
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<div class="input-group-btn">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span></button>
						             <ul class="dropdown-menu">
										<li><a @click="showModalAddress('create','billing')" href="#">Create</a></li>
										<li><a @click="showModalAddress('edit','billing')" href="#">Edit</a></li>
									</ul>
								</div>
								<select @change="changeAddress('billing')" v-model="selectedBillingId" class="form-control">
									<option value="${value.id}" v-for="value in listBillingAddress">${value.address.substring(0,10)}</option>
								</select>
							</div>
						</div>
					</div>
	 				<ul class="list-condensed list-unstyled">
						<li><h5>${nullToEmpty(activeBilling.firstname) + ' ' + nullToEmpty(activeBilling.lastname)}</h5></li>
						<li><span class="text-semibold">${activeBilling.address}</span></li>
						<li>${activeBilling.district},${activeBilling.city}</li>
						<li>${activeBilling.province}</li>
						<li>${activeBilling.postcode}</li>
						<li><a href="#">${activeBilling.phone}</a></li>
					</ul>
				</div>
				<div class="col-md-6 content-group">
					<div class="row">
						<div class="col-md-2">
							<span class="text-muted">Shipping Address:</span>							
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<div class="input-group-btn">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span></button>
						             <ul class="dropdown-menu">
										<li><a @click="showModalAddress('create','shipping')" href="#">Create</a></li>
										<li><a @click="showModalAddress('edit','shipping')" href="#">Edit</a></li>
									</ul>
								</div>
								<select @change="changeAddress('shipping')" v-model="selectedShippingId" class="form-control">
									<option value="${value.id}" v-for="value in listShippingAddress">${value.address.substring(0,10)}</option>
								</select>
							</div>
						</div>
					</div>
	 				<ul class="list-condensed list-unstyled">
						<li><h5>${nullToEmpty(activeShipping.firstname) + ' ' + nullToEmpty(activeShipping.lastname)}</h5></li>
						<li><span class="text-semibold">${activeShipping.address}</span></li>
						<li>${activeShipping.district},${activeShipping.city}</li>
						<li>${activeShipping.province}</li>
						<li>${activeShipping.postcode}</li>
						<li><a href="#">${activeShipping.phone}</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-white">
		<div class="panel-heading">
			<h6 class="panel-title">Products</h6>
			<div class="heading-elements">
				<ul class="icons-list">
				    <li><a data-action="collapse"></a></li>
				</ul>
			</div>
		<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-lg">
					<thead>
					    <tr>
					        <th>Products</th>
					        <th class="col-sm-1">QTY</th>
					        <th class="col-sm-1">Unit Price</th>
					        <th class="col-sm-1">Discount</th>
					        <th class="col-sm-1">Total</th>
					    </tr>
					</thead>
					<tbody>
						
					    <tr>
					        <td>
					        	<h6 class="no-margin">XXX</h6>
					        	<span class="text-muted">XXX</span>
						 	 </td>
					        <td>12121</td>
					        <td>asdasd</td>
					        <td>2313123</td>
					        <td><span class="text-semibold">12312312</span></td>
					    </tr>
					 
					</tbody>
				</table>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Teal button">
						<span class="input-group-btn">
							<button class="btn btn0info" type="button">Search Products</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-white">
		<div class="panel-heading">
			<h6 class="panel-title">Search Products</h6>
			<div class="heading-elements">
				<ul class="icons-list">
				    <li><a data-action="collapse"></a></li>
				</ul>
			</div>
		<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-lg">
					<thead>
					    <tr>
					        <th>Products</th>
					        <th class="col-sm-1">QTY</th>
					        <th class="col-sm-1">Unit Price</th>
					        <th class="col-sm-1">Discount</th>
					        <th class="col-sm-1">Total</th>
					    </tr>
					</thead>
					<tbody>
						
					    <tr>
					        <td>
					        	<h6 class="no-margin">XXX</h6>
					        	<span class="text-muted">XXX</span>
						 	 </td>
					        <td>12121</td>
					        <td>asdasd</td>
					        <td>2313123</td>
					        <td><span class="text-semibold">12312312</span></td>
					    </tr>
					 
					</tbody>
				</table>
			</div>
			
		</div>
	</div>
	<div class="panel panel-white">
		<div class="panel-heading">
			<h6 class="panel-title">Shipping</h6>
			<div class="heading-elements">
				<ul class="icons-list">
				    <li><a data-action="collapse"></a></li>
				</ul>
			</div>
		<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-white">
		<div class="panel-heading">
			<h6 class="panel-title">Confirmation</h6>
			<div class="heading-elements">
				<ul class="icons-list">
				    <li><a data-action="collapse"></a></li>
				</ul>
			</div>
		<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Default text input</label>
						<div class="col-md-10">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-white">
		<div class="panel-heading">
			<h6 class="panel-title">Total</h6>
			<div class="heading-elements">
				<ul class="icons-list">
				    <li><a data-action="collapse"></a></li>
				</ul>
			</div>
		<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-lg">
					<thead>
					    <tr>
					        <th>Products</th>
					        <th class="col-sm-1">QTY</th>
					        <th class="col-sm-1">Unit Price</th>
					        <th class="col-sm-1">Discount</th>
					        <th class="col-sm-1">Total</th>
					    </tr>
					</thead>
					<tbody>
						
					    <tr>
					        <td>
					        	<h6 class="no-margin">XXX</h6>
					        	<span class="text-muted">XXX</span>
						 	 </td>
					        <td>12121</td>
					        <td>asdasd</td>
					        <td>2313123</td>
					        <td><span class="text-semibold">12312312</span></td>
					    </tr>
					 
					</tbody>
				</table>
			</div>
			
		</div>
	</div>
	<div id="modalUser" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h6 class="modal-title">${ (this.user.id == 0) ? 'Add' : 'Edit' } User</h6>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="alert alert-danger" v-for="error in errors" track-by="$index">${error}</div>
						<div class="alert alert-success" v-show="showSuccesMessage">Data Saved!</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label col-md-2">Email</label>
								<div class="col-md-10">
									<input v-model="modalUser.email" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">First Name</label>
								<div class="col-md-10">
									<input v-model="modalUser.firstname" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">Last Name</label>
								<div class="col-md-10">
									<input v-model="modalUser.lastname" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">Address</label>
								<div class="col-md-10">
									<textarea v-model="modalUser.address" class="form-control"></textarea>
								</div>
							</div>

						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button @click='saveUser()' type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	<div id="modalAddress" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-warning">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h6 class="modal-title">${ (this.modalAddress.id == 0) ? 'Add' : 'Edit' } Address</h6>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="alert alert-danger" v-for="error in errors" track-by="$index">${error}</div>
						<div class="alert alert-success" v-show="showSuccesMessage">Data Saved!</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label col-md-2">Email</label>
								<div class="col-md-10">
									<input v-model="modalAddress.email" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">First Name</label>
								<div class="col-md-10">
									<input v-model="modalAddress.firstname" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">Last Name</label>
								<div class="col-md-10">
									<input v-model="modalAddress.lastname" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">Address</label>
								<div class="col-md-10">
									<textarea v-model="modalAddress.address" class="form-control"></textarea>
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">Address 2</label>
								<div class="col-md-10">
									<textarea v-model="modalAddress.address_2" class="form-control"></textarea>
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">Postal Code</label>
								<div class="col-md-10">
									<input v-model="modalAddress.postcode" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">City</label>
								<div class="col-md-10">
									<input id="address-city" v-model="modalAddress.city" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">District</label>
								<div class="col-md-10">
									<input id="address-district" v-model="modalAddress.district" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">Province</label>
								<div class="col-md-10">
									<input v-model="modalAddress.province" type="text" class="form-control">
								</div>
							</div>
							<br><br>
							<div class="form-group">
								<label class="control-label col-md-2">Phone</label>
								<div class="col-md-10">
									<input v-model="modalAddress.phone" type="text" class="form-control">
								</div>
							</div>
							<br><br>


						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button @click='saveAddress()' type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>	
</div>

<link rel="stylesheet" type="text/css" href="{{ asset('popitoi/css/token-input.css') }}">
<style type="text/css">
	.ui-front{
    z-index: 10050;
  }
  .token-input-list{
  	height: 35px!important;
  }
  .clearable{
    background: #fff url("data:image/gif;base64,R0lGODlhBwAHAIAAAP///5KSkiH5BAAAAAAALAAAAAAHAAcAAAIMTICmsGrIXnLxuDMLADs=") no-repeat right -10px center;
    transition: background 0.4s;
  }
  .clearable.x  { background-position: right 5px center;cursor: pointer;background-color: rgb(238, 238, 238) } /* (jQ) Show icon */
  .clearable::-ms-clear {display: none; width:0; height:0;} /* Remove IE default X */
</style>
<script type="text/javascript">
	var urlGetCustomer = "{{ route('catalog_get_user') }}";	
	var urlSaveUser = "{{ route('catalog_save_user') }}";
	var urlSaveAddress = "{{ route('catalog_save_address') }}";
	var urlGetCity = "{{ route('catalog_get_city') }}";
	var urlGetDistrict = "{{ route('catalog_get_district') }}";
	var urlGetShippingMethods = "{{ route('catalog_get_shipping_methods') }}";
</script>
<script type="text/javascript" src="{{ asset('popitoi/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('popitoi/js/money.js') }}"></script>
<script type="text/javascript" src="{{ asset('popitoi/js/framework/vue.js') }}"></script>
<script type="text/javascript" src="{{ asset('popitoi/js/framework/configs/vue.setting.js') }}"></script>
<script type="text/javascript" src="{{ asset('popitoi/js/framework/vue-resource.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('popitoi/js/jquery.tokeninput.js') }}"></script>
<script type="text/javascript">	
	var objAddress = {
		email:"",
		firstname:"",
		lastname:"",	
		city:"",
		district:"",
		province:"",
		address:"",
		phone:"",
		postcode:"",
		type:"",
		id:0,
		user_id:0
	};
	var objUser = {
		id:0,
		email:"",
		firstname:"",
		lastname:"",
		name:"",
		address:""
	};
	var data = {
		user:{
			id:0,
			name:""
		},
		listBillingAddress:[],
		listShippingAddress:[],
		selectedBillingId:0,
		selectedShippingId:0,
		errors:[],
		showSuccesMessage:false,
		products:[],
		totalWeight:1,
		jne_reg:0,
		jne_yes:0,
		jne_oke:0,
		selectedShippingMethod:0,
	};
	data.modalUser = JSON.parse(JSON.stringify(objUser));
	data.activeBilling = JSON.parse(JSON.stringify(objAddress));
	data.activeShipping = JSON.parse(JSON.stringify(objAddress));
	data.modalAddress = JSON.parse(JSON.stringify(objAddress));
	data.shippingMethods = {!! json_encode($shippingMethods) !!};
	var vueInvoice = new Vue({
		el:"#vueInvoice",
		data:data,
		created:function(){
			this.shippingMethods.push({id:'jne_reg','name':'JNE REG'});
			this.shippingMethods.push({id:'jne_yes','name':'JNE YES'});
			this.shippingMethods.push({id:'jne_oke','name':'JNE OKE'});
		},
		ready:function(){	
			var vm = this;
        	this.initUserAutoComplete(vm);
        	this.initCityAutoComplete(vm);
        	this.initDistrictAutoComplete(vm);

		},
		watch:{
			'user':function(val,oldVal){
				this.reloadAddressList();
			},
			'selectedShippingId':function(val,oldVal)
			{
				this.getShippingMethods();
			},
			'activeShipping':function(val,oldVal)
			{
				this.getShippingMethods();
			}

		},
		methods:{
			unReactiveData(data){
				return JSON.parse(JSON.stringify(data));
			},
			
			nullToEmpty(val)
			{
				if(typeof val == 'undefined' || val == null)
				{
					return "";
				}

				return val;
			},
			
			initCityAutoComplete(vm)
			{
				$("#address-city").autocomplete({
				     source: urlGetCity,
				     minLength: 1
				});
			},

			initDistrictAutoComplete(vm)
			{
				$("#address-district").autocomplete({
				          source: function(request, response) {
				            $.getJSON(urlGetDistrict, { city: $("#address-city").val(), term:$("#address-district").val() },response);
				          },
				          minLength: 1,
				});
			},

			initUserAutoComplete(vm)
			{
				$("#userField").tokenInput(urlGetCustomer, {
					hintText: "Type Customer or Business name",
					noResultsText: "Customer or Business not found",
					searchingText: "Searching ...",
					minChars: 1,
					tokenLimit: 1,
					preventDuplicates: true,
					propertyToSearch: "name",
					resultsFormatter: function(item){ 
						return "<li>" + "<div style='display: inline-block;'><div class='full_name'>" +" " + item.name + "</div><div class='email'> </div></div></li>";
					},	
					onAdd: function (item) {
						vm.reloadUser(item);
					},
					onDelete: function (item) {
						vm.user = vm.unReactiveData(objUser);
						vm.selectedBillingId = 0; 
						vm.selectedShippingId = 0;
						vm.listShippingAddress = [];
						vm.listBillingAddress = [];
						vm.modalAddress = vm.unReactiveData(objAddress);
						vm.modalUser = vm.unReactiveData(objUser);
						vm.activeBilling = vm.unReactiveData(objAddress);
						vm.activeShipping = vm.unReactiveData(objAddress);
					},
					onResult: function (results) {
						return results;
					}
				});
			},

			reloadUser(item){
				this.user = this.unReactiveData(item);
				this.listBillingAddress = this.unReactiveData(item.billing_address);
				this.listShippingAddress = this.unReactiveData(item.shipping_address);
			},

			reloadAddressList(){
				this.getBillingAddress();
				this.getShippingAddress();
			},

			getBillingAddress(){
				if(this.user.id == 0) this.listBillingAddress = [];

				if(this.listBillingAddress.length && this.selectedBillingId == 0)
				{
					this.selectedBillingId = this.listBillingAddress[0].id;
					this.activeBilling = this.listBillingAddress[0];
				}
			},

			getShippingAddress(){
				if(this.user.id == 0) this.listShippingAddress = [];

				if(this.listShippingAddress.length && this.selectedShippingId == 0)
				{
					this.selectedShippingId = this.listShippingAddress[0].id;
					this.activeShipping = this.listShippingAddress[0];
				}

			},

			showCreateEditUser(type){
				this.showSuccesMessage = false;
				this.errors = [];
				if(this.user.id == 0 || type == 'create'){
					this.modalUser = this.unReactiveData(objUser);
				}else{
					this.modalUser = this.unReactiveData(this.user);
				}
				$("#modalUser").modal('show');
			},
			
			changeAddress(type)
			{
				var col = 'activeShipping';
				var colList = 'listShippingAddress';
				var activeId = 'selectedShippingId';
				if(type == 'billing')
				{
					col = 'activeBilling';
					colList = 'listBillingAddress';
					activeId = 'selectedBillingId';
				}


				for (var i = this[colList].length - 1; i >= 0; i--) {
					var item = this[colList][i];
					if(item.id == this[activeId]){
						this[col] = this[colList][i];
					}
				}

			},

			saveUser()
			{
				var vm = this;
				this.errors = [];
				this.showSuccesMessage = false;

				this.$http.post(urlSaveUser,this.modalUser,function(data,s,r){
					if(data.status == 1)
					{
						this.showSuccesMessage = true;
						$("#userField").tokenInput("clear");
						$("#userField").tokenInput("add",data.user);
					}else{
						this.errors.push(data.msg);
					}

				}).error(function(data,s,r){
					$.each(data,function(i,k){
						for (var i = k.length - 1; i >= 0; i--) {
							vm.errors.push(k[i]);
						}
					})
				});
			},

			showModalAddress(action,type)
			{
				this.errors = [];
				this.showSuccesMessage = false;

				if(this.user.id == 0){
					alert("Please select user first!");
					return false;
				}

				var col = 'selectedBillingId';
				var colList = 'listBillingAddress';
				if(type == 'shipping'){
					col = 'selectedShippingId';
					colList = 'listShippingAddress';
				}
				
				if(action == 'create'){
					this.modalAddress = this.unReactiveData(objAddress);
					this.modalAddress.type = type;
				}else{
					var id = this[col];
					for (var i = this[colList].length - 1; i >= 0; i--) {
						if(this[colList][i].id == id){
							this.modalAddress = this.unReactiveData(this[colList][i]);
							this.modalAddress.type = type;
						}
					}
				}

				$("#modalAddress").modal('show');
			},

			saveAddress()
			{	
				if(this.user.id == 0) return false;

				this.errors = [];
				this.showSuccesMessage = false;
				this.modalAddress.user_id = this.user.id;
				var vm = this;
				var oldId = this.unReactiveData(this.modalAddress.id);

				this.$http.post(urlSaveAddress,this.modalAddress,function(data,s,r){
					if(data.status == 1){
						this.showSuccesMessage = true;
						var col = 'activeShipping';
						var colList = 'listShippingAddress';
						if(data.address.type == 'billing'){
							col = 'activeBilling';
							colList = 'listBillingAddress';
						}

						if(this[col].id == data.address.id){
							this[col] = this.unReactiveData(data.address);							
						}

						if(oldId == 0)
						{
							this[colList].push(data.address);
						}

						for (var i = this[colList].length - 1; i >= 0; i--) {
							var item = this[colList][i];
							if(item.id == data.address.id)
							{
								this[colList].$set(i,this.unReactiveData(data.address));
							}
						}
					}
				}).error(function(data,s,r){
					$.each(data,function(i,k){
						for (var i = k.length - 1; i >= 0; i--) {
							vm.errors.push(k[i]);
						}
					})
				});
			},

			getShippingMethods()
			{
				this.$http.get(urlGetShippingMethods,{city:this.activeShipping.city,district:this.activeShipping.district},function(data,s,r){
					if(data.status == 1){
						var vm = this;
						$.each(data.couriers,function(i,k){
							vm[k.value] = parseFloat(k.amount);
						});
					}	
				});
			}


		}
	})
</script>