<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class Order extends AlphaORM		
{
	protected $table = 'catalog_orders';

	public function user(){
		return $this->belongsTo('User');
	}
	public function details(){
		return $this->hasMany('Morra\Catalog\Models\OrderDetail');
	}
	public function orderPromos(){
		return $this->hasMany('Morra\Catalog\Models\OrderPromo','order_id');
	}

}