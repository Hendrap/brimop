<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class Address extends AlphaORM		
{
	protected $table = 'catalog_addresses';

	public function user(){
		return $this->belongsTo('User');
	}
}