<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class Search extends AlphaORM		
{
	protected $table = 'catalog_search';

	public static function rebuildProduct($entry){
		syncProductsToSolr([$entry->id]);
	}
	public static function rebuildItem($item){
		syncProductsToSolr([$item->product_id]);
	}
	public function item(){
		return $this->belongsTo('Morra\Catalog\Models\Item');
	}
	public function product(){
		return $this->belongsTo('Entry','product_id');
	}
}