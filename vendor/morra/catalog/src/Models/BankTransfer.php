<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class BankTransfer extends AlphaORM		
{
	protected $table = 'catalog_payment_banktransfer';

	public function user(){
		return $this->belongsTo('User');
	}
	public function order(){
		return $this->belongsTo('Morra\Catalog\Models\Order');
	}
}