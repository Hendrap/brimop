<?php 
namespace Morra\Catalog\Models;

use Alpha\Models\AlphaORM;
/**
* 
*/
class OrderDetail extends AlphaORM		
{
	protected $table = 'catalog_order_details';

	public function product(){
		return $this->belongsTo('Entry','product_id');
	}
	public function item(){
		return $this->belongsTo('Morra\Catalog\Models\Item');
	}
	public function order(){
		return $this->belongsTo('Morra\Catalog\Models\Order');
	}
	public function orderDetailPromos(){
		return $this->hasMany('Morra\Catalog\Models\OrderPromo','order_detail_id');
	}

}