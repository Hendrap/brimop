<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;

class OrderController extends AlphaController
{
	public function index(Request $req){
		$this->layout->active = 'alpha_catalog_order_index';

		$this->layout->title = setPageTitle("Orders");
		$orders = \CatalogOrder::where('order_number','like',$req->input('q','%%'))->orderBy('created_at','desc')->paginate(30);
		$lastUpdate = \CatalogOrder::orderBy('updated_at','desc')->first();
		$this->layout->content = view('catalog::admin.pages.order.index',[
			'orders' => $orders,
			'lastUpdate' => $lastUpdate
			]);
	}
	public function save(Request $req){
		$order = \CatalogOrder::find($req->order_id);
		if(!empty($order)){
			$order->status = $req->status;
			$order->tracking_number = $req->tracking_number;
			$order->save();

		}
		return redirect()->back();
	}
	public function detail($id = 0){
		$this->layout->active = 'alpha_catalog_order_index';
		$order = \CatalogOrder::with('orderPromos','details','details.orderDetailPromos')->find($id);
		if(empty($order)) abort(404);

		$payment = app('Alpha_Catalog_PaymentMethodList_'.$order->payment_method);
		if(empty($payment)) abort(500,'Payment Method Not Found');

		$shipping = app('Alpha_Catalog_ShippingMethodList_'.$order->shipping_method);		
		if(empty($shipping)) abort(500,'Shipping Method Not Found');

		$status = \Config::get('catalog.order-status');

		$this->layout->title = setPageTitle('Order #'.$order->order_number);
		$this->layout->content = view('catalog::admin.pages.order.detail',[
			'order' => $order,
			'payment' => $payment,
			'shipping' => $shipping,
			'status' => $status
			]);		

	}
}