<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;

class PromoController extends AlphaController
{
	public function index(){
		$this->layout->active = 'alpha_catalog_promo_index';
		$this->layout->title = setPageTitle("Promo");
		$promos = \CatalogPromo::paginate(30);
		$this->layout->content = view('catalog::admin.pages.promo.index',[
			'promos' => $promos
			]);
	}
	public function create(){
		$this->layout->active = 'alpha_catalog_promo_index';
		$this->layout->title = setPageTitle("Create Promo");	
		$this->layout->content = view('catalog::admin.pages.promo.create');	
	}
	public function edit($id = 0){
		$this->layout->active = 'alpha_catalog_promo_index';
		$this->layout->title = setPageTitle("Edit Promo");		
		$promo = \CatalogPromo::find($id);
		if(empty($promo)) abort(404);

		$this->layout->title = setPageTitle("Edit Promo");	
		$this->layout->content = view('catalog::admin.pages.promo.edit',[
			'promo' => $promo
			]);	
	}
	public function save(Request $req){
		
		$messages = [
		    'rule_detail.required' => 'The field Discount Amount/Percent is required!',
		];

		$rules = [
			'coupon_code' => 'required',
			'min_order' => 'required',
			'rule_detail' => 'required',
		];
		$id = (int)$req->promo_id;
		if($id == 0){
			$rules['coupon_code'] = 'required|unique:catalog_promos';
		}

		$this->validate($req,$rules,$messages);

		$promo = \CatalogPromo::find($id);
		if(empty($promo)) $promo = new \CatalogPromo();

		$promo->title = $req->title;
		$promo->content = $req->content;
		$promo->status = $req->status;
		$promo->coupon_type = $req->coupon_type;
		$promo->coupon_code = $req->coupon_code;
		$promo->rule_detail = $req->rule_detail;
		$promo->start = date('Y-m-d H:i:s',strtotime($req->start));
		$promo->end = date('Y-m-d H:i:s',strtotime($req->end));
		$promo->user_id = $req->user_id;
		$promo->algorithm = $req->algorithm;
		$promo->min_order = $req->min_order;
		$promo->save();

		return redirect(url('admin/catalog/promo-edit/'.$promo->id));
	}
	public function delete($id){
		\CatalogPromo::find($id)->delete();
		return redirect()->back();
	}
}