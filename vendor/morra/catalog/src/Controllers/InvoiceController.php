<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Alpha\Core\AlphaController;
use Morra\Catalog\Core\Shipping\Jne;
use Morra\Catalog\Models\Jne as JneModel;
use User;
use Usermeta;
use DB;
use CatalogAddress;

class InvoiceController extends AlphaController
{
	use ValidatesRequests;

	public function view(Request $req)
	{
		$shippingMethods = Taxonomy::where('id','!=',1223)->where('id','!=',1224)->where('id','!=',1225)->where('taxonomy_type','=','shipping-carrier')->get();
		$this->layout->active = 'alpha_catalog_order_index';
		$this->layout->title = setPageTitle('Order #');
		$this->layout->content = view('catalog::admin.pages.order.invoice',[
				'shippingMethods' => $shippingMethods
			]);		
	}

	public function getUser(Request $req)
	{
		$data = [];

		$q = $req->q;

		$users = User::with(['metas','billingAddress','shippingAddress'])->select(array('users.id','users.email','users.username'))
		->join('usermetas',function($j){
		
			$j->on('usermetas.user_id','=','users.id');
		
		})->where(function($w){
		
			$w->where('meta_key','=','first_name');
			$w->orWhere('meta_key','=','last_name');
		
		})->where(function($w) use($q){
			$w->where('username','like','%'.$q.'%');
			$w->orWhere('email','like','%'.$q.'%');
			$w->orWhere('meta_value_text','like','%'.$q.'%');

		})->take(30)->groupBy('user_id')->orderBy('email','asc')->get();


		foreach ($users as $key => $value) {
			$data[] = $this->parseUser($value);
		}

		return json_encode($data);
	}

	public function parseUser($value)
	{
		return [
					'id' => $value->id,
					'email' => $value->email,
					'billing_address' => $value->billingAddress,
					'shipping_address' => $value->shippingAddress,
					'firstname' => getEntryMetaFromArray($value->metas,'first_name'),
					'lastname' => getEntryMetaFromArray($value->metas,'last_name'),
					'address' => getEntryMetaFromArray($value->metas,'address'),
					'name' => getEntryMetaFromArray($value->metas,'first_name').' '.getEntryMetaFromArray($value->metas,'last_name'),
			];
	}

	public function validateDuplicateEmail($email = '')
	{
		$user = User::whereEmail($email)->first();

		return count($user);
	}

	public function saveUser(Request $req)
	{
		$this->validate($req,[
				'firstname' => 'required'
			]);


		$user = User::find((int)$req->id);
		if(!empty($user))
		{
			if($user->email != $req->email){
				if(!$this->validateDuplicateEmail($req->email)){
					return json_encode(array('status'=>0,'msg'=>'Duplicate Email Address!'));
				}
			}
		}

		if(empty($user))
		{

			if($this->validateDuplicateEmail($req->email) > 0){
				return json_encode(array('status'=>0,'msg'=>'Duplicate Email Address!'));
			}
			$user = new User();
		}

		$user->email = $req->email;
		if(empty($req->email))
		{
			$tmpName = Str::slug($req->firstname);
			$countUser = User::where('email','like',$tmpName.'%')->count();
			
			if($countUser > 0){
				$tmpName .= '-'.$countUser;
			}

			$tmpName .= '@pos-customer.popitoi.com';
			$user->email = $tmpName;
			$user->type = 'customer-pos';
		}

		$user->save();
		$metas = ['firstname'=>'first_name','lastname'=>'last_name','address'=>'address'];

		$deleted = Usermeta::where(function($w) use($metas){
			foreach ($metas as $key => $value) {
				if($key == 'first_name'){
					$w->where('meta_key','=',$value);
				}else{
					$w->orWhere('meta_key','=',$value);
				}
			}
		})->whereUserId($user->id)->delete();

		$insertToMeta = [];
		foreach ($metas as $key => $value) {
			$insertToMeta[] = [
				'user_id' => $user->id,
				'meta_key' => $value,
				'meta_value_text' => $req->{$key}
			];
		}

		
		if(!empty($insertToMeta))
		{
			DB::table('usermetas')->insert($insertToMeta);
		}

		$user = User::with(['metas','billingAddress','shippingAddress'])->find($user->id);
		$user = $this->parseUser($user);
		return json_encode(array('status'=>1,'user'=>$user));
	}

	public function saveAddress(Request $req)
	{
		$this->validate($req,[
				'address' => 'required',
				'phone' => 'numeric'
			]);

		$fields = ['type','email','user_id','firstname','lastname','address','address_2','phone','district','city','province','postcode'];
		$address = CatalogAddress::find($req->id);
		if(empty($address)) $address = new CatalogAddress();

		foreach ($fields as $key => $value) {
			$address->{$value} = $req->{$value};
		}
		$address->save();

		return json_encode(array('status'=>1,'address'=>$address));
	}

	public function getCity(Request $req)
    {
        $data = [];
        $jne = new Jne();
        $cities = $jne->getCity($req->term);
        foreach ($cities as $key => $value) {
            $data[] = $value->regency;
        }

        return json_encode($data);
    }

    public function getDistrict(Request $req)
    {
        $data = [];
        $jne = new Jne();
        $cities = $jne->getDistrict($req->city,$req->term,false);
        foreach ($cities as $key => $value) {
            $data[] = $value->district;
        }

        return json_encode($data);
    }

	public function getShippingMethods(Request $req)
	{
		$couriers = [];
        $couriersDB = JneModel::where('regency','like','%'.$req->city.'%')->orWhere('district','like','%'.$req->district.'%')->first();

        if(!empty($couriersDB)){
            $types = ['reg','oke','yes'];
            foreach ($types as $key => $value) {
                 $couriers[] = (object)['value'=>'jne_'.$value,'amount' => $couriersDB->{'jne_'.$value}];
            }
        }
        return json_encode(array('status'=>1,'couriers'=>$couriers));
	}
}