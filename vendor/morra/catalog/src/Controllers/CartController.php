<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class CartController extends BaseController
{
	public function clean(Request $req){
		set_time_limit(0);

		if($req->input('token') != \Config::get('catalog.cart.encryptCart')) exit();

		\CatalogCart::cleanUpExpiredCart(\Config::get('catalog.cart.period'));

		echo "Done!";

	}
}