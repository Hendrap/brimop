<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Requests;
use Alpha\Core\AlphaController;

class ProductController extends AlphaController
{
    public function getAdditionalInfo($type = ''){

        $metas = getMetaFromEntry($type);

        $taxoInfo = getTaxonomyFromEntry($type);
        $taxoTypes = $taxoInfo->taxoTypes;
        $taxo = $taxoInfo->taxo;
        $taxonomies = getTaxonomiesForEntry($taxoTypes);
        
        $brands = \Entry::select(['id','title'])->whereEntryType('brand')->where('status','!=','deleted')->get();

        return (object)['brands'=>$brands,'taxonomies' => $taxonomies,'metas'=>$metas,'taxo'=>$taxo];
    }
	public function index($catalogType = '')
	{
		$req = app('request');
        $sort = $req->input('sort',0);
        $stock = $req->input('stock',0);
        $brand = $req->input('brand',0);
        $q = $req->input('q','');

        $type = 'catalog_variant_'.$catalogType;
        
        $total = \Entry::whereEntryType($type)->where('entries.status','!=','deleted')->count();
        
        $entries = \Entry::with('user')->select('entries.*')->whereEntryType($type)->where('entries.status','!=','deleted');
        if($sort == 'asc' || $sort == 'desc'){
            $entries = $entries->orderby('entries.title',$sort);
        }
        if($sort == 'oldest' || $sort == 'latest'){
            switch ($sort) {
                case 'oldest':
                    $sort = 'asc';
                    break;
                case 'latest':
                    $sort = 'desc';
                    break;
            }
            $entries = $entries->orderby('entries.created_at',$sort)->orderby('entries.created_at',$sort);
        }
         
         if($q != ''){
            $entries = $entries->where(function($query)use($q){
                        $query->where('entries.title','like','%'.$q.'%');
                        $query->orWhere('entries.content','like','%'.$q.'%');
            });
        }
        if((int)$brand != 0){
             $entries = $entries->whereBrandId($brand);
        }
        if($stock === 'out-of-stock' || $stock === 'in-stock'){
            $entries = $entries->join('catalog_items',function($j){
                $j->on('catalog_items.product_id','=','entries.id');
            });

           if($stock == 'out-of-stock'){
                 $entries = $entries->where(function($query){
                    $query->where('catalog_items.stock_status','=',0);
                    $query->orWhere('catalog_items.stock_available','<=',0);
                 });
           }

           if($stock == 'in-stock'){
                 $entries = $entries->where(function($query){
                    $query->where('catalog_items.stock_status','=',1);
                    $query->orWhere('catalog_items.stock_available','>=',0);
                 });
           }
           $entries = $entries->groupBy('catalog_items.product_id');
        }

        $entries = $entries->paginate(30);
        
        $info = getEntryConfig($type);
        $last = \Entry::getLastUpdate($type);
        $theLastName = 'alpha';
        $theLastDate = date('d F Y H:i:s');
        if(!empty($last->modifiedBy)){
            $theLastName = $last->modifiedBy->username;
            $theLastDate = date('d F Y H:i:s',strtotime($last->updated_at));
        }

        $brands = \Entry::whereEntryType('brand')->whereStatus('published')->get();

        $this->layout->active = 'alpha_catalog_type_'.$type;
        $this->layout->title = setPageTitle($info['plural']);
        $this->layout->content = view('catalog::admin.pages.product.index',[
                'entries' => $entries,
                'type' => $type,
                'total' => $total,
                'info'  => $info,
                'theLastName'=> $theLastName,
                'theLastDate'=> $theLastDate,
                'catalogType'=> $catalogType,
                'brands' => $brands,
                'sort' => $sort,
                'stock' => $stock,
                'brand' => $brand,
            ]);
	}
	public function create($type = '')
	{
		$entryType = 'catalog_variant_'.$type;
		$config = getEntryConfig($entryType);
		$info = $this->getAdditionalInfo($entryType);

        $this->layout->active = 'alpha_catalog_type_'.$entryType;
		$this->layout->title = setPageTitle("Create ".$config['single']);
		$this->layout->content = view('catalog::admin.pages.product.create',[
			'config'	=> $config,
			'catalogType' => $type,
			'type'		=> $entryType,
			'brands' => $info->brands,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
			]);
	}
    public function edit($type = '',$id = 0)
    {
        $entry = \Entry::with(['taxonomies','metas'])->find($id);
        if(empty($entry)) abort(404);

        $entryType = 'catalog_variant_'.$type;
        $config = getEntryConfig($entryType);
        $info = $this->getAdditionalInfo($entryType);

        $selectedTaxo = [];
        if(!empty($entry->taxonomies)){
            foreach ($entry->taxonomies as $key => $value) {
                $selectedTaxo[] = $value->id;
            }
        }


        $this->layout->active = 'alpha_catalog_type_'.$entryType;
        $this->layout->title = setPageTitle("Edit ".$config['single']);
        $this->layout->content = view('catalog::admin.pages.product.edit',[
            'config'    => $config,
            'entry' => $entry,
            'catalogType' => $type,
            'type'      => $entryType,
            'brands' => $info->brands,
            'metas' => $info->metas,
            'taxo' => $info->taxo,
            'taxonomies' => $info->taxonomies,
            'selectedTaxo' => $selectedTaxo,
            ]);
        
    }  
	public function save($catalogType = '',$id = 0,Request $requests){

		$type = 'catalog_variant_'.$catalogType;
		$defaultLang = \Config::get('alpha.application.default_locale');
        
        $this->validate($requests,[
            'title.'.$defaultLang => 'required',
            'status' => 'required'
            ]);

        $input = $requests->input();
       
        $vars = ['title','content','excerpt'];

        foreach ($vars as $key => $value) {
            
            ${'default'.ucfirst($value)} = @$input[$value][$defaultLang];
            ${$value} = '';

            foreach ($input[$value] as $k => $v) {

                $tmp = '';
                $tmp = ${'default'.ucfirst($value)};
                if(!empty($v)) $tmp = $v;
                ${$value} .= '[:'.$k.']'.$tmp;

            }

        }
        $entry = \Entry::find($id);
        if(empty($entry)){
            $entry = new \Entry();
            $entry->author = app('AdminUser')->user->id;
        }

        $slug = \Request::input('slug',str_slug($defaultTitle));
        if(empty($slug)) $slug = str_slug($defaultTitle);
        $check = \Entry::where('id','!=',$id)->where('slug','like',$slug.'%')->count();
        if($check > 0){
            $slug .= '-'.$check;
        }
        $metas = array();
        $tmpMetas = app('AlphaSetting')->entries;
        foreach ($tmpMetas as $key => $value) {
            if($value['slug'] == $type){
                $metas = $value['metas'];
            }
        }
        $entry->title = $title;
        $entry->slug = $slug;
        $entry->content = $content;
        $entry->excerpt = $excerpt;
        $entry->entry_type = $type;
        $entry->brand_id = \Request::input('brand',0);
        $entry->catalog_type = 'variant';
        $entry->modified_by = app('AdminUser')->user->id;
        $entry->status = $input['status'];
        $entry->published_at = date('Y-m-d H:i:s',strtotime($input['published_at']));
        $entry->touch();
        $entry->save();
        $entry->saveSEO(@$input['seo']);
        $entry->saveEntryMetaFromInput($metas,$input); 

        $entry->taxonomies()->sync([]);  
        $entry->taxonomies()->sync(\Request::input('taxonomies',[]));


        $year = (int)getEntryMetaFromArray($entry->metas,'year_released');
        
        $categoryId = 0;
        $seriesId = 0;
        $partsShape = 0;
        $partsColor = 0;
        $conditionId = 0;

        if(!empty($input['taxonomies'])){
            foreach ($input['taxonomies'] as $key => $value) {
                $tmpTaxo = \Taxonomy::find($value);
                $tmpTaxo->count = $tmpTaxo->count + 1;
                $tmpTaxo->save();

                if($tmpTaxo->taxonomy_type == 'category'){
                    $categoryId = $tmpTaxo->id;
                }

                if($tmpTaxo->taxonomy_type == 'condition'){
                    $conditionId = $tmpTaxo->id;
                }

                if($tmpTaxo->taxonomy_type == 'series'){
                    $seriesId = $tmpTaxo->id;
                }

                if($tmpTaxo->taxonomy_type == 'parts-category'){
                    $partsShape = $tmpTaxo->id;
                }

                if($tmpTaxo->taxonomy_type == 'parts-color'){
                    $partsColor = $tmpTaxo->id;
                }

            }
        }

        $entry->category_id = $categoryId;
        $entry->condition_id = $conditionId;
        $entry->series_id = $seriesId;
        $entry->parts_shape_id = $partsShape;
        $entry->parts_color_id = $partsColor;
        $entry->year = $year;
        $entry->save();

        $entry->medias()->sync([]);
        $medias = \Request::input('media',[]);
        if($id != 0){
            $medias = array_reverse(\Request::input('media',[]));            
        }

        $entry->medias()->sync($medias);
        \CatalogSearch::rebuildProduct($entry);
        
        return redirect()->route('catalog_product_variant_edit',[$catalogType,$entry->id])->with('msg','Data Saved!');
	}
	public function status($id = 0,$status = '')
	{
		$entry = \Entry::find($id);
        if(empty($entry)) return redirect()->back()->with('err','Product not found!');

        $entry->status = $status;
        $entry->save();

        if($status == 'published'){
            \CatalogSearch::rebuildProduct($entry);
        }else{
            deleteProductFromSolr($entry->id);
        }
        return redirect()->back()->with('msg','Data Saved!');
	}
}