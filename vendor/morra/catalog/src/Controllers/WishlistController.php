<?php

namespace Morra\Catalog\Controllers;

use Illuminate\Http\Request;
use Alpha\Core\AlphaController;

class WishlistController extends AlphaController
{
	public function index(){
		$this->layout->active = 'alpha_catalog_wishlists';
		$this->layout->title = setPageTitle("Wishlists");

		$data = \CatalogWishlist::with(['user','item','item.entry'])->where('status','!=','deleted')->paginate(30);

		$this->layout->content = view('catalog::admin.pages.wishlists',[
			'data' => $data
			]);

	}
	public function status($id,$status){

		$data = \CatalogWishlist::find($id);
		if(empty($data)) return redirect()->back()->with('err','Data not found');

		$data->status = $status;
		$data->save();
		return redirect()->back()->with('msg','Data Saved!');
	}
}