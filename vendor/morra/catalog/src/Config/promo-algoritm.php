<?php 

return [
	'fixed_amount' => (object)['title' => 'Fixed Amount','class'=>'Morra\Catalog\Core\Promo\FixedAmount'],
	'percentage_discount' => (object)['title' => 'Percentage Discount','class'=>'Morra\Catalog\Core\Promo\PercentageDicount'],
	'free_shipping' => (object)['title' => 'Free Shipping','class'=>'Morra\Catalog\Core\Promo\FreeShipping'],
];