<?php 
Route::group(['middleware' => ['AlphaAdminGroup','AuthAdmin'],'namespace'=>'Morra\Catalog\Controllers','prefix'=>'admin/catalog'], function () {

	//main
	Route::get('/',['as'=>'catalog_stat','uses'=>'CatalogController@index']);

	//migration	
	Route::any('/migration',['as'=>'catalog_stat','uses'=>'CatalogController@primitiveMigration']);

	//setting

	Route::get('setting',['as'=>'catalog_setting','uses'=>'SettingController@getSetting']);
	Route::post('setting',['as'=>'catalog_setting_post','uses'=>'SettingController@postSetting']);

	//product
	Route::get('variant/{type}',['as'=>'catalog_product_variant_index','uses'=>'ProductController@index']);
	Route::get('variant/{type}/create',['as'=>'catalog_product_variant_create','uses'=>'ProductController@create']);
	Route::get('variant/edit/{type}/{id}',['as'=>'catalog_product_variant_edit','uses'=>'ProductController@edit']);
	Route::post('variant/save/{type}/{id}',['as'=>'catalog_product_variant_save','uses'=>'ProductController@save']);
	Route::get('variant/status/{id}/{status}',['as'=>'catalog_product_variant_status','uses'=>'ProductController@status']);

	//item normal
	Route::get('item/{product_id}',['as'=>'catalog_item_variant_index','uses'=>'ItemVariantController@index']);
	Route::get('item/{product_id}/create',['as'=>'catalog_item_variant_create','uses'=>'ItemVariantController@create']);
	Route::get('item/edit/{product_id}/{id}',['as'=>'catalog_item_variant_edit','uses'=>'ItemVariantController@edit']);
	Route::post('item/save/{product_id}/{id}',['as'=>'catalog_item_variant_save','uses'=>'ItemVariantController@save']);
	Route::get('item/status/{id}/{status}',['as'=>'catalog_item_variant_status','uses'=>'ItemVariantController@status']);

	//item group
	Route::get('item-group/{product_id}/create',['as'=>'catalog_item_group_create','uses'=>'ItemGroupController@create']);
	Route::get('item-group/edit/{product_id}/{id}',['as'=>'catalog_item_group_edit','uses'=>'ItemGroupController@edit']);
	Route::post('item-group/save/{product_id}/{id}',['as'=>'catalog_item_group_save','uses'=>'ItemGroupController@save']);

	//search item normal
	Route::get('item-search',['as'=>'catalog_item_variant_search','uses'=>'ItemVariantController@search']);

	//wishlist
	Route::get('wishlists',['as'=>'catalog_wishlists_index','uses'=>'WishlistController@index']);
	Route::get('status-wishlist/{id}/{status}',['as'=>'catalog_wishlists_status','uses'=>'WishlistController@status']);

	Route::get('promo',['as'=>'catalog_promo_index','uses'=>'PromoController@index']);
	Route::get('promo-create',['as'=>'catalog_promo_create','uses'=>'PromoController@create']);
	Route::get('promo-edit/{id}',['as'=>'catalog_promo_edit','uses'=>'PromoController@edit']);
	Route::post('promo-save',['as'=>'catalog_promo_save','uses'=>'PromoController@save']);
	Route::get('promo-delete/{id}',['as'=>'catalog_promo_delete','uses'=>'PromoController@delete']);

	Route::get('orders',['as'=>'catalog_orders_index','uses'=>'OrderController@index']);
	Route::get('order-detail/{id}',['as'=>'catalog_orders_detail','uses'=>'OrderController@detail']);
	Route::post('order-save',['as'=>'catalog_orders_save','uses'=>'OrderController@save']);
	Route::get('invoice',['as'=>'catalog_invoice','uses'=>'InvoiceController@view']);
	Route::get('invoice/get-user',['as'=>'catalog_get_user','uses'=>'InvoiceController@getUser']);
	Route::post('invoice/save-user',['as'=>'catalog_save_user','uses'=>'InvoiceController@saveUser']);
	Route::post('invoice/save-address',['as'=>'catalog_save_address','uses'=>'InvoiceController@saveAddress']);
	Route::get('invoice/get-city',['as'=>'catalog_get_city','uses'=>'InvoiceController@getCity']);
	Route::get('invoice/get-district',['as'=>'catalog_get_district','uses'=>'InvoiceController@getDistrict']);
	Route::get('invoice/get-shipping-methods',['as'=>'catalog_get_shipping_methods','uses'=>'InvoiceController@getShippingMethods']);

});

//clean up cart
Route::get('clean-up-cart',['as'=>'catalog_cart_clean_up','uses'=>'Morra\Catalog\Controllers\CartController@clean']);