<?php //file : vendor/morra/catalog/src/core/paymentmethod/CatalogVeritrans.php --------------------------------------------- ?>

<?php

namespace Morra\Catalog\Core\PaymentMethod;
use Morra\Catalog\Core\PaymentMethod as AbsPayment;
use App\Veritrans\Veritrans;
class CatalogVeritrans extends AbsPayment
{
	public function capture($order,$data = array())
	{
		Veritrans::$serverKey = 'VT-server-KmcpmHcF7CqVHCJjq2rsaWh6';
        Veritrans::$isProduction = false;
        $vt = new Veritrans;
        $items = [];

        $items[] = array(
		 	'id' => 'shippingfee',
		 	'price' => $order->shipping_amount,
		 	'quantity' => 1,
		 	'name' => 'Shipping Fee',
		);
        $items[] = array(
			'id' => 'totaldiscount',
			'price' => $order->total_discount * -1,
			'quantity' => 1,
			'name' => 'Total Discount'
       	);

        foreach ($order->details as $key => $value) {
        	$items[] = array(
				'id' => $value->id,
				'price' =>$value->final_amount,
				'quantity' => 1,
				'name' => $value->item_title.' '.$value->product_title,
       		);
        }
        $item_details = $items;
        $transaction_details = array(
			'order_id' => $order->order_number,
			'gross_amount' => $order->grand_total,
		);

		$billing_address = array(
					    'first_name'    => $order->payment_firstname,
					    'last_name'     => $order->payment_lastname,
					    'address'       => $order->payment_address_1,
					    'city'          => $order->payment_city,
					    'phone'         => $order->payment_phone,
					    'country_code'  => 'IDN'
					    );

					// Optional
		$shipping_address = array(
					   	'first_name'    => $order->shipping_firstname,
					    'last_name'     => $order->shipping_lastname,
					    'address'       => $order->shipping_address_1,
					    'city'          => $order->shipping_city,
					    'phone'         => $order->shipping_phone,
					    'country_code'  => 'IDN'
					    );

		$customer_details = array(
					    'first_name'    => $order->payment_firstname,
					    'last_name'     => $order->payment_lastname,
					    'email'         => $order->payment_email,
					    'phone'         => $order->payment_phone,,
					    'billing_address'  => $billing_address,
					    'shipping_address' => $shipping_address
					    );
		$transaction = array(
						'payment_type' => 'vtweb', 
					    'transaction_details' => $transaction_details,
					    'customer_details' => $customer_details,
					    'item_details' => $item_details,
					    'vtweb' => array(
					    	  "finish_redirect_url" => URL::to('callback'),
					    	  "unfinish_redirect_url" => URL::to('callback-unfinish'),
					    	  "error_redirect_url" => URL::to('callback-error'),
					    	)
					    );
		$vtweb_url = $vt->vtweb_charge($transaction);
		return array('redirect'=>true,'url'=>$vtweb_url);

	}
	public function getNicePaymentMethodName(){
		return 'Veritrans';
	}
	public function getPaymentMethodName(){
		return 'veritrans';
	}
	public function render($orderId = 0){
		echo "string";
	}
}

?>
<?php //file : vendor/morra/catalog/src/core/paymentmethod/CatalogVeritrans.php --------------------------------------------- ?>


Cara pakai


1. Buat file CatalogVeritrans.php di folder /vendor/morra/catalog/src/core/paymentmethod,isinya yg diatas tuh
2. Sebelum CatalogCheckout masukin code ini 

app()->bind('Alpha_Catalog_PaymentMethod','Morra\Catalog\Core\PaymentMethod\CatalogVeritrans');

3. Wes mari ahahaha





